#ifndef MODELS_STEPSLISTMODEL_H
#define MODELS_STEPSLISTMODEL_H

#include "abstract_list_model.h"
#include "widgets/abstract_step_widget.h"
#include <studio-lib/models/steps/abstract_step.h>

namespace models {

class StepsListModel : public models::AbstractListModel
{
  Q_OBJECT
public:
  explicit StepsListModel(QObject* parent = nullptr);
  ~StepsListModel() override;

protected:
  class ItemStepFactory;
  class ItemStepMixFactory;
  class ItemStepDryFactory;
  class ItemStepBeadsCollectFactory;
  class ItemStepBeadsReleaseFactory;
  class ItemStepSleeveCollectFactory;
  class ItemStepSleeveReleaseFactory;
  class ItemStepHeaterFactory;
  class ItemStepManualMoveFactory;
  class ItemStepPauseFactory;
  class ItemStepSleepFactory;

  // QAbstractItemModel interface
public:
  int rowCount(const QModelIndex& parent) const override;
  QVariant data(const QModelIndex& index, int role) const override;
  QVariant headerData(int section,
                      Qt::Orientation orientation,
                      int role) const override;
  bool insertRows(int row, int count, const QModelIndex& parent) override;
  bool removeRows(int row, int count, const QModelIndex& parent) override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;

  // AbstractListModel interface
public:
  void createItem() override {}
  void clear() override;

  struct step_item : public AbstractListModel::list_item
  {
    step_item(models::steps::AbstractStep* _item,
              widgets::AbstractStepWidget* _widget)
      : list_item{ _item, _widget }
    {}

    ~step_item() override
    {
      delete item;
      delete widget;
    }
  };

private:
  QList<step_item*> items;
  QScopedPointer<ItemStepFactory> factory;

public:
  QList<step_item*> getItems() { return items; }

public slots:
  void createStep(const StepType type);
  void loadData(const QList<models::steps::AbstractStep*>& data);

private:
  void deduceStepType(const StepType type);
};

} // namespace models

#endif // MODELS_STEPSLISTMODEL_H
