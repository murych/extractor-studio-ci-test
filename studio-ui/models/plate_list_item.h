#ifndef PLATELISTITEM_H
#define PLATELISTITEM_H

#include "abstract_list_item.h"
#include <QJsonObject>

class PlateListItem : public AbstractListItem
{
public:
  enum class plate_type
  {
    unknown = 0,
    standard_96,
    user
  };

  PlateListItem(const QJsonObject& json = {});
  ~PlateListItem();

  plate_type plateType() const { return m_plate_type; }

private:
  plate_type m_plate_type{ plate_type::standard_96 };
};

#endif // PLATELISTITEM_H
