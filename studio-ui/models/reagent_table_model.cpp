#include "reagent_table_model.h"

#include <QColor>
#include <QIcon>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <studio-lib/models/plates/reagent.h>

#include <QLoggingCategory>
#include <studio-lib/logger.h>

Q_LOGGING_CATEGORY(RTM, "REAGENT TABLE")

namespace models {

ReagentTableModel::ReagentTableModel(QObject* parent)
  : QAbstractTableModel{ parent }
{}

ReagentTableModel::~ReagentTableModel() {}

int
ReagentTableModel::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid())
    return 0;

  return items.count();
}

int
ReagentTableModel::columnCount(const QModelIndex& parent) const
{
  if (parent.isValid())
    return 0;

  return cols::color + 1;
}

QVariant
ReagentTableModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
    return QVariant{};

  const plates::Reagent* reagent = items.at(index.row());

  switch (static_cast<Qt::ItemDataRole>(role)) {
    case Qt::DisplayRole:
    case Qt::EditRole: {
      switch (static_cast<cols>(index.column())) {
        case name:
          return reagent->name();
        case volume:
          return reagent->volume();
        case color:
          return QColor{ reagent->color() };
        case type:
          return reagent->type();
        default:
          break;
      }
      break;
    }
    case Qt::DecorationRole: {
      switch (static_cast<cols>(index.column())) {
        case type: {
          return QIcon{ reagent->icon() };
        }
        case color: {
          return QColor{ reagent->color() };
        }
        default:
          break;
      }
      break;
    }
    default:
      break;
  }

  return QVariant{};
}

bool
ReagentTableModel::setData(const QModelIndex& index,
                           const QVariant& value,
                           int role)
{
  if (!index.isValid() || static_cast<Qt::ItemDataRole>(role) != Qt::EditRole)
    return false;

  plates::Reagent* reagent = items.at(index.row());

  switch (static_cast<cols>(index.column())) {
    case name:
      reagent->setName(value.toString());
      break;
    case volume:
      reagent->setVolume(value.toInt());
      break;
    case color:
      reagent->setColor(value.toString());
      break;
    case type:
      reagent->setType(value.toInt());
      break;
  }

  emit dataChanged(index, index);
  return true;
}

QVariant
ReagentTableModel::headerData(int section,
                              Qt::Orientation orientation,
                              int role) const
{
  if (role != Qt::DisplayRole)
    return QVariant{};

  const plates::Reagent reagent;

  switch (orientation) {
    case Qt::Horizontal: {
      switch (static_cast<cols>(section)) {
        case name:
          return reagent.reagent_name->label();
        case volume:
          return reagent.reagent_volume->label();
        case color:
          return reagent.reagent_color->label();
        case type:
          return reagent.reagent_type->label();
      }
    }
    case Qt::Vertical:
      return QString::number(section + 1);
  }

  return QVariant{};
}

bool
ReagentTableModel::insertRows(int row, int count, const QModelIndex& parent)
{
  if (parent.isValid())
    return false;

  beginInsertRows(QModelIndex{}, row, row + count - 1);

  for (int idx = 0; idx < count; ++idx) {
    auto newItem = new plates::Reagent;

    if (items.isEmpty())
      items.append(newItem);
    else
      items.insert(row, newItem);
  }

  endInsertRows();

  return true;
}

bool
ReagentTableModel::removeRows(int row, int count, const QModelIndex& parent)
{
  if (parent.isValid())
    return false;

  if (items.isEmpty())
    return false;

  if (row < 0 || row > items.count() || (row + count) > items.count())
    return false;

  beginRemoveRows(QModelIndex{}, row, row + count - 1);

  for (int idx = 0; idx < count; ++idx)
    items.removeAt(row);

  endRemoveRows();

  return true;
}

Qt::ItemFlags
ReagentTableModel::flags(const QModelIndex& index) const
{
  return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

void
ReagentTableModel::clear()
{
  beginResetModel();
  qDeleteAll(items);
  items.clear();
  endResetModel();
}

/*
void
ReagentTableModel::loadData(const QJsonArray& json)
{
  clear();

  if (json.isEmpty())
    return;

  beginInsertRows(QModelIndex{}, items.count(), items.count());

  for (int idx = 0; idx < json.count(); ++idx) {
    const QJsonObject json_object = json.at(idx).toObject();
    plates::Reagent* newReagent = new plates::Reagent{ this, json_object };
    items.append(newReagent);
  }

  endInsertRows();
}
*/

void
ReagentTableModel::loadData(const QList<plates::Reagent*>& reagents)
{
  clear();

  beginInsertRows(QModelIndex{}, items.count(), items.count());
  items << reagents;
  endInsertRows();
}

} // namespace models
