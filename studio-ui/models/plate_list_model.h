#ifndef TREEMODEL_H
#define TREEMODEL_H

#include "abstract_list_model.h"
#include "widgets/plate_info.h"
#include <QAbstractListModel>
#include <QJsonArray>
#include <QJsonObject>
#include <QObject>
#include <studio-lib/models/plates/plate.h>

namespace models {

class PlateListModel : public AbstractListModel
{
  Q_OBJECT
public:
  explicit PlateListModel(QObject* parent = nullptr);
  ~PlateListModel() override;

  // AbstractListModel interface
public:
  void createItem() override;
  void clear() override;

  struct plate_item : public AbstractListModel::list_item
  {
    plate_item(plates::Plate* _item, widgets::PlateInfo* _widget)
      : list_item{ _item, _widget }
    {}

    ~plate_item() override;
  };

protected:
  class ItemPlateFactory;

private:
  QList<plate_item*> items;
  QScopedPointer<ItemPlateFactory> factory;

public:
  QList<plate_item*> getItems() { return items; }

  // QAbstractItemModel interface
public:
  int rowCount(const QModelIndex& parent) const override;
  QVariant data(const QModelIndex& index, int role) const override;
  QVariant headerData(int section,
                      Qt::Orientation orientation,
                      int role) const override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;
  bool insertRows(int row, int count, const QModelIndex& parent) override;
  bool removeRows(int row, int count, const QModelIndex& parent) override;
  bool moveRows(const QModelIndex& sourceParent,
                int sourceRow,
                int count,
                const QModelIndex& destinationParent,
                int destinationChild) override;

public:
  void loadData(QList<plates::Plate*>& plates);
};

} // namespace models

#endif // TREEMODEL_H
