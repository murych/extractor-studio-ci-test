#ifndef TREEITEM_H
#define TREEITEM_H

#include "abstract_list_item.h"
#include <QJsonObject>
#include <studio-lib/models/abstract_step.h>

class StepListItem : public AbstractListItem
{
public:
  //  enum class step_type
  //  {
  //    pickup,
  //    leave,
  //    mix,
  //    dry,
  //    pause,
  //    move,
  //    collect,
  //    release,
  //    sleep,
  //    user
  //  };

  explicit StepListItem(const QJsonObject& json = {});
  ~StepListItem();

  StepType stepType() const { return m_step_type; }

private:
  StepType m_step_type{ models::AbstractStep::user };

  QIcon chooseDefaultIcon();
  QString chooseDefaultName();
};

#endif // TREEITEM_H
