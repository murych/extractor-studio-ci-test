#ifndef MODELS_ABSTRACTLISTMODEL_H
#define MODELS_ABSTRACTLISTMODEL_H

#include <QAbstractListModel>
#include <QWidget>
#include <studio-lib/data/entity.h>

namespace models {

class AbstractListModel : public QAbstractListModel
{
  Q_OBJECT
public:
  explicit AbstractListModel(QObject* parent = nullptr) {}
  virtual ~AbstractListModel() {}

  virtual void createItem() = 0;
  virtual void clear() = 0;

public slots:
  void onItemSelected(const QModelIndex& index)
  {
    if (!index.isValid())
      return;

    emit openRelatedWidget(index.row());
  }

signals:
  void openRelatedWidget(const int index);

protected:
  struct list_item
  {
    data::Entity* item{ nullptr };
    QWidget* widget{ nullptr };

    list_item(data::Entity* _item, QWidget* _widget)
      : item{ _item }
      , widget{ _widget }
    {}

    virtual ~list_item() = default;
  };

  class ItemFactory
  {
  public:
    ItemFactory() = default;
    virtual ~ItemFactory() = default;
    virtual list_item* FactoryMethod() const = 0;
  };
};

} // namespace models

#endif // MODELS_ABSTRACTLISTMODEL_H
