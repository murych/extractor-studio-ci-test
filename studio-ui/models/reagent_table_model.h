#ifndef REAGENTTABLEMODEL_H
#define REAGENTTABLEMODEL_H

#include <QAbstractTableModel>

namespace models {

namespace plates {
class Reagent;
}

class ReagentTableModel : public QAbstractTableModel
{
  Q_OBJECT
public:
  explicit ReagentTableModel(QObject* parent = nullptr);
  ~ReagentTableModel() override;

  // QAbstractItemModel interface
public:
  int rowCount(const QModelIndex& parent) const override;
  int columnCount(const QModelIndex& parent) const override;
  QVariant data(const QModelIndex& index, int role) const override;
  bool setData(const QModelIndex& index,
               const QVariant& value,
               int role) override;
  QVariant headerData(int section,
                      Qt::Orientation orientation,
                      int role) const override;
  bool insertRows(int row, int count, const QModelIndex& parent) override;
  bool removeRows(int row, int count, const QModelIndex& parent) override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;

  void clear();

public slots:
  //  void loadData(const QJsonArray& json);
  void loadData(const QList<plates::Reagent*>& reagents);

public:
  QList<plates::Reagent*> items;

public:
  enum cols
  {
    //    idx = 0,
    name = 0,
    volume,
    type,
    color,
  };
};

} // namespace models

#endif // REAGENTTABLEMODEL_H
