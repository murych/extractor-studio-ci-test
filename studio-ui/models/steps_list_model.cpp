#include "steps_list_model.h"

#include "widgets/step_info_beads_collect.h"
#include "widgets/step_info_beads_release.h"
#include "widgets/step_info_dry.h"
#include "widgets/step_info_heater.h"
#include "widgets/step_info_manual_move.h"
#include "widgets/step_info_mix.h"
#include "widgets/step_info_pause.h"
#include "widgets/step_info_sleep.h"
#include "widgets/step_info_sleeves_collect.h"
#include "widgets/step_info_sleeves_release.h"

#include <studio-lib/models/steps/step_beads_collect.h>
#include <studio-lib/models/steps/step_beads_release.h>
#include <studio-lib/models/steps/step_dry.h>
#include <studio-lib/models/steps/step_heater.h>
#include <studio-lib/models/steps/step_manual_move.h>
#include <studio-lib/models/steps/step_mix.h>
#include <studio-lib/models/steps/step_pause.h>
#include <studio-lib/models/steps/step_sleep.h>
#include <studio-lib/models/steps/step_sleeves_leave.h>
#include <studio-lib/models/steps/step_sleeves_pickup.h>

#include <QIcon>
#include <QLoggingCategory>
#include <studio-lib/logger.h>

Q_LOGGING_CATEGORY(SLM, "STEP LIST")

namespace models {

class StepsListModel::ItemStepFactory : public AbstractListModel::ItemFactory
{
public:
  ItemStepFactory() = default;
  virtual ~ItemStepFactory() override = default;

  virtual AbstractListModel::list_item* FactoryFromItem(
    models::steps::AbstractStep* step) const = 0;

protected:
  void connectAdvancedParams(models::steps::AbstractStep* step,
                             widgets::AbstractStepWidget* widget) const
  {
    connect(widget,
            &widgets::AbstractStepWidget::endPositionChanged,
            step,
            &steps::AbstractStep::setSleevesPosAtEnd);
    connect(widget,
            &widgets::AbstractStepWidget::verticalSpeedChanged,
            step,
            &steps::AbstractStep::setVerticalSpeed);
    connect(widget,
            &widgets::AbstractStepWidget::horizontalSpeedChanged,
            step,
            &steps::AbstractStep::setHorizontalSpeed);
  }

  void connectRelatedPlate(models::steps::AbstractStep* step,
                           widgets::AbstractStepWidget* widget) const
  {
    connect(widget,
            &widgets::AbstractStepWidget::relatedPlateChanged,
            step,
            &steps::AbstractStep::setRelatedPlate);
  }

  //  virtual void connectSpecificParams(
  //    models::steps::AbstractStep* step,
  //    widgets::AbstractStepWidget* widget) const = 0;
};

class StepsListModel::ItemStepMixFactory : public ItemStepFactory
{
public:
  ItemStepMixFactory() = default;
  ~ItemStepMixFactory() override = default;

private:
  void connectSpecificParams(steps::StepMix* step,
                             widgets::StepInfoMix* widget) const
  {
    qDebug() << "StepsListModel::ItemStepMixFactory::connectSpecificParams"
             << step;

    connect(widget,
            &widgets::StepInfoMix::patternRepeatCountChanged,
            step,
            &steps::StepMix::setPatternRepeat);
    connect(widget,
            &widgets::StepInfoMix::enterLiquidSpeedChanged,
            step,
            &steps::StepMix::setEnterSpeed);
    connect(widget,
            &widgets::StepInfoMix::mixStartingPositionChanged,
            step,
            &steps::StepMix::setStartPosition);
    connect(widget,
            &widgets::StepInfoMix::mixAmplitudeChanged,
            step,
            &steps::StepMix::setMixAmplitude);
  }

public:
  AbstractListModel::list_item* FactoryMethod() const override
  {
    models::steps::StepMix* newStep = new models::steps::StepMix;
    widgets::StepInfoMix* newWidget = new widgets::StepInfoMix;

    connectRelatedPlate(newStep, newWidget);
    connectAdvancedParams(newStep, newWidget);
    connectSpecificParams(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }

  AbstractListModel::list_item* FactoryFromItem(
    steps::AbstractStep* step) const override
  {
    qDebug() << "\t\tStepsListModel::ItemStepMixFactory::FactoryFromItem"
             << step;

    steps::StepMix* newStep = static_cast<steps::StepMix*>(step);
    widgets::StepInfoMix* newWidget = new widgets::StepInfoMix;

    qDebug() << newStep;

    newWidget->loadStepToUi(newStep);

    connectRelatedPlate(newStep, newWidget);
    connectAdvancedParams(newStep, newWidget);
    connectSpecificParams(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }
};

class StepsListModel::ItemStepDryFactory : public ItemStepFactory
{
public:
  ItemStepDryFactory() = default;
  ~ItemStepDryFactory() override = default;

private:
  void connectSpecificParameters(steps::StepDry* step,
                                 widgets::StepInfoDry* widget) const
  {
    connect(widget,
            &widgets::StepInfoDry::dryDurationChanged,
            step,
            &steps::StepDry::setDuration);
    connect(widget,
            &widgets::StepInfoDry::dryPositionChanged,
            step,
            &steps::StepDry::setPosition);
  }

  AbstractListModel::list_item* FactoryMethod() const override
  {
    models::steps::StepDry* newStep = new models::steps::StepDry;
    widgets::StepInfoDry* newWidget = new widgets::StepInfoDry;

    connectRelatedPlate(newStep, newWidget);
    connectAdvancedParams(newStep, newWidget);
    connectSpecificParameters(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }

  AbstractListModel::list_item* FactoryFromItem(
    steps::AbstractStep* step) const override
  {
    qDebug() << "\t\tStepsListModel::ItemStepDryFactory::FactoryFromItem"
             << step->getStepName();

    steps::StepDry* newStep = static_cast<steps::StepDry*>(step);
    widgets::StepInfoDry* newWidget = new widgets::StepInfoDry;
    newWidget->loadStepToUi(newStep);

    connectRelatedPlate(newStep, newWidget);
    connectAdvancedParams(newStep, newWidget);
    connectSpecificParameters(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }
};

class StepsListModel::ItemStepBeadsCollectFactory : public ItemStepFactory
{
public:
  ItemStepBeadsCollectFactory() = default;
  ~ItemStepBeadsCollectFactory() override = default;

private:
  void connectSpecificParameters(steps::StepBeadsCollect* step,
                                 widgets::StepInfoBeadsCollect* widget) const
  {
    connect(widget,
            &widgets::AbstractStepWidget::endPositionChanged,
            step,
            &steps::AbstractStep::setSleevesPosAtEnd);
    connect(widget,
            &widgets::AbstractStepWidget::horizontalSpeedChanged,
            step,
            &steps::AbstractStep::setHorizontalSpeed);
    connect(widget,
            &widgets::AbstractStepWidget::verticalSpeedChanged,
            step,
            &steps::AbstractStep::setVerticalSpeed);
    connect(widget,
            &widgets::AbstractStepWidget::relatedPlateChanged,
            step,
            &steps::AbstractStep::setRelatedPlate);
  }

public:
  AbstractListModel::list_item* FactoryMethod() const override
  {
    auto newStep = new models::steps::StepBeadsCollect;
    auto newWidget = new widgets::StepInfoBeadsCollect;

    connectRelatedPlate(newStep, newWidget);
    connectAdvancedParams(newStep, newWidget);
    connectSpecificParameters(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }

  AbstractListModel::list_item* FactoryFromItem(
    steps::AbstractStep* step) const override
  {
    qDebug()
      << "\t\tStepsListModel::ItemStepBeadsCollectFactory::FactoryFromItem"
      << step->getStepName();

    auto newStep = static_cast<steps::StepBeadsCollect*>(step);
    auto newWidget = new widgets::StepInfoBeadsCollect;

    newWidget->loadStepToUi(newStep);

    connectRelatedPlate(newStep, newWidget);
    connectAdvancedParams(newStep, newWidget);
    connectSpecificParameters(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }
};

class StepsListModel::ItemStepBeadsReleaseFactory : public ItemStepFactory
{
public:
  ItemStepBeadsReleaseFactory() = default;
  ~ItemStepBeadsReleaseFactory() override = default;

private:
  void connectSpecificParameters(steps::StepBeadsRelease* step,
                                 widgets::StepInfoBeadsRelease* widget) const
  {
    connect(widget,
            &widgets::StepInfoBeadsRelease::releaseSpeedChanged,
            step,
            &steps::StepBeadsRelease::setReleaseSpeed);
    connect(widget,
            &widgets::StepInfoBeadsRelease::releaseCountChanged,
            step,
            &steps::StepBeadsRelease::setReleaseCount);
    connect(widget,
            &widgets::StepInfoBeadsRelease::releaseDurationChanged,
            step,
            &steps::StepBeadsRelease::setReleaseDuration);
  }

public:
  AbstractListModel::list_item* FactoryMethod() const override
  {
    auto newStep = new models::steps::StepBeadsRelease;
    auto newWidget = new widgets::StepInfoBeadsRelease;

    connectRelatedPlate(newStep, newWidget);
    connectAdvancedParams(newStep, newWidget);
    connectSpecificParameters(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }

  AbstractListModel::list_item* FactoryFromItem(
    steps::AbstractStep* step) const override
  {
    qDebug()
      << "\t\tStepsListModel::ItemStepBeadsReleaseFactory::FactoryFromItem"
      << step->getStepName();

    auto newStep = static_cast<steps::StepBeadsRelease*>(step);
    auto newWidget = new widgets::StepInfoBeadsRelease;

    newWidget->loadStepToUi(newStep);

    connectRelatedPlate(newStep, newWidget);
    connectAdvancedParams(newStep, newWidget);
    connectSpecificParameters(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }
};

class StepsListModel::ItemStepSleeveCollectFactory : public ItemStepFactory
{
public:
  ItemStepSleeveCollectFactory() = default;
  ~ItemStepSleeveCollectFactory() override = default;

  AbstractListModel::list_item* FactoryMethod() const override
  {
    auto newStep{ new models::steps::StepSleevesPickup };
    auto newWidget{ new widgets::StepInfoSleevesCollect };

    connectRelatedPlate(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }

  AbstractListModel::list_item* FactoryFromItem(
    steps::AbstractStep* step) const override
  {
    qDebug()
      << "\t\tStepsListModel::ItemStepSleevesCollectFactory::FactoryFromItem"
      << step->getStepName();

    auto newStep{ static_cast<steps::StepSleevesPickup*>(step) };
    auto newWidget{ new widgets::StepInfoSleevesCollect };

    newWidget->loadStepToUi(newStep);

    connectRelatedPlate(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }
};

class StepsListModel::ItemStepSleeveReleaseFactory : public ItemStepFactory
{
public:
  ItemStepSleeveReleaseFactory() = default;
  ~ItemStepSleeveReleaseFactory() override = default;

  AbstractListModel::list_item* FactoryMethod() const override
  {
    auto newStep{ new models::steps::StepSleevesLeave };
    auto newWidget{ new widgets::StepInfoSleevesRelease };

    connectRelatedPlate(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }

  AbstractListModel::list_item* FactoryFromItem(
    steps::AbstractStep* step) const override
  {
    qDebug()
      << "\t\tStepsListModel::ItemStepSleevesReleaseFactory::FactoryFromItem"
      << step->getStepName();

    auto newStep{ static_cast<steps::StepSleevesLeave*>(step) };
    auto newWidget{ new widgets::StepInfoSleevesRelease };

    newWidget->loadStepToUi(newStep);
    connectRelatedPlate(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }
};

class StepsListModel::ItemStepHeaterFactory : public ItemStepFactory
{
public:
  ItemStepHeaterFactory() = default;
  ~ItemStepHeaterFactory() override = default;

private:
  void connectSpecificParameters(steps::StepHeater* step,
                                 widgets::StepInfoHeater* widget) const
  {
    connect(widget,
            &widgets::StepInfoHeater::blockExecutionChanged,
            step,
            &steps::StepHeater::setBlockExecution);
    connect(widget,
            &widgets::StepInfoHeater::targetTemperatureChanged,
            step,
            &steps::StepHeater::setTargetTemp);
  }

public:
  AbstractListModel::list_item* FactoryMethod() const override
  {
    models::steps::StepHeater* newStep{ new models::steps::StepHeater };
    widgets::StepInfoHeater* newWidget{ new widgets::StepInfoHeater };

    connectRelatedPlate(newStep, newWidget);
    connectAdvancedParams(newStep, newWidget);
    connectSpecificParameters(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }

  AbstractListModel::list_item* FactoryFromItem(
    steps::AbstractStep* step) const override
  {
    qDebug() << "\t\tStepsListModel::ItemStepHeaterFactory::FactoryFromItem"
             << step->getStepName();

    models::steps::StepHeater* newStep{ static_cast<steps::StepHeater*>(step) };
    widgets::StepInfoHeater* newWidget{ new widgets::StepInfoHeater };

    newWidget->loadStepToUi(newStep);

    connectRelatedPlate(newStep, newWidget);
    connectAdvancedParams(newStep, newWidget);
    connectSpecificParameters(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }
};

class StepsListModel::ItemStepManualMoveFactory : public ItemStepFactory
{
public:
  ItemStepManualMoveFactory() = default;
  ~ItemStepManualMoveFactory() override = default;

  AbstractListModel::list_item* FactoryMethod() const override
  {
    models::steps::StepManualMove* newStep{ new models::steps::StepManualMove };
    widgets::StepInfoManualMove* newWidget{ new widgets::StepInfoManualMove };

    connectRelatedPlate(newStep, newWidget);
    connectAdvancedParams(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }

  AbstractListModel::list_item* FactoryFromItem(
    steps::AbstractStep* step) const override
  {
    qDebug() << "\t\tStepsListModel::ItemStepManualMoveFactory::FactoryFromItem"
             << step->getStepName();

    steps::StepManualMove* newStep{ static_cast<steps::StepManualMove*>(step) };
    widgets::StepInfoManualMove* newWidget{ new widgets::StepInfoManualMove };

    newWidget->loadStepToUi(newStep);
    connectRelatedPlate(newStep, newWidget);
    connectAdvancedParams(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }
};

class StepsListModel::ItemStepPauseFactory : public ItemStepFactory
{
public:
  ItemStepPauseFactory() = default;
  ~ItemStepPauseFactory() override = default;

private:
  void connectSpecificParameters(steps::StepPause* step,
                                 widgets::StepInfoPause* widget) const
  {

    connect(widget,
            &widgets::StepInfoPause::messageChanged,
            step,
            &steps::StepPause::setMessage);
  }

public:
  AbstractListModel::list_item* FactoryMethod() const override
  {
    models::steps::StepPause* newStep{ new models::steps::StepPause };
    widgets::StepInfoPause* newWidget{ new widgets::StepInfoPause };

    connectRelatedPlate(newStep, newWidget);
    connectSpecificParameters(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }

  AbstractListModel::list_item* FactoryFromItem(
    steps::AbstractStep* step) const override
  {
    qDebug() << "\t\tStepsListModel::ItemStepPauseFactory::FactoryFromItem"
             << step->getStepName();

    models::steps::StepPause* newStep{ static_cast<steps::StepPause*>(step) };
    widgets::StepInfoPause* newWidget{ new widgets::StepInfoPause };

    newWidget->loadStepToUi(newStep);
    connectRelatedPlate(newStep, newWidget);
    connectSpecificParameters(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }
};

class StepsListModel::ItemStepSleepFactory : public ItemStepFactory
{
public:
  ItemStepSleepFactory() = default;
  ~ItemStepSleepFactory() override = default;

private:
  void connectSpecificParameters(steps::StepSleep* step,
                                 widgets::StepInfoSleep* widget) const
  {

    connect(widget,
            &widgets::StepInfoSleep::sleepDurationChanged,
            step,
            &steps::StepSleep::setSleepDuration);
  }

public:
  AbstractListModel::list_item* FactoryMethod() const override
  {
    models::steps::StepSleep* newStep{ new models::steps::StepSleep };
    widgets::StepInfoSleep* newWidget{ new widgets::StepInfoSleep };

    connectRelatedPlate(newStep, newWidget);
    connectSpecificParameters(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }

  AbstractListModel::list_item* FactoryFromItem(
    steps::AbstractStep* step) const override
  {
    qDebug() << "\t\tStepsListModel::ItemStepSleepFactory::FactoryFromItem"
             << step->getStepName();

    models::steps::StepSleep* newStep{ static_cast<steps::StepSleep*>(step) };
    widgets::StepInfoSleep* newWidget{ new widgets::StepInfoSleep };

    newWidget->loadStepToUi(newStep);
    connectRelatedPlate(newStep, newWidget);
    connectSpecificParameters(newStep, newWidget);

    return new step_item{ newStep, newWidget };
  }
};

StepsListModel::StepsListModel(QObject* parent)
  : models::AbstractListModel{ parent }
{}

StepsListModel::~StepsListModel() {}

int
StepsListModel::rowCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent)
  return items.count();
}

QVariant
StepsListModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
    return QVariant{};

  const steps::AbstractStep* step =
    static_cast<steps::AbstractStep*>(items.at(index.row())->item);

  switch (static_cast<Qt::ItemDataRole>(role)) {
    case Qt::DisplayRole:
    case Qt::EditRole:
      return step->getStepName();
    case Qt::DecorationRole:
      return QIcon{ step->stepIcon() };
    default:
      break;
  }

  return QVariant{};
}

QVariant
StepsListModel::headerData(int section,
                           Qt::Orientation orientation,
                           int role) const
{
  if (role != Qt::DisplayRole)
    return QVariant{};

  return (orientation == Qt::Horizontal) ? QString("Step")
                                         : QString::number(section);
}

bool
StepsListModel::insertRows(int row, int count, const QModelIndex& parent)
{
  qCDebug(SLM) << "StepsListModel::insertRows"
               << "/row =" << row << "/count =" << count
               << "/parent is valid =" << parent.isValid();

  if (parent.isValid())
    return false;

  beginInsertRows(QModelIndex{}, row, row + count - 1);

  for (int idx = 0; idx < count; ++idx) {
    step_item* newItem = static_cast<step_item*>(factory->FactoryMethod());

    if (items.isEmpty())
      items.append(newItem);
    else
      items.insert(row, newItem);
  }

  endInsertRows();

  return true;
}

bool
StepsListModel::removeRows(int row, int count, const QModelIndex& parent)
{
  qCDebug(SLM) << "StepsListModel::removeRows"
               << "/row =" << row << "/count =" << count
               << "/parent is valid =" << parent.isValid();

  if (parent.isValid())
    return false;

  if (items.isEmpty())
    return false;

  if (row < 0 || row > items.count() || (row + count) > items.count())
    return false;

  beginRemoveRows(QModelIndex{}, row, row + count - 1);

  for (int idx = 0; idx < count; ++idx)
    items.removeAt(row);

  endRemoveRows();

  return true;
}

Qt::ItemFlags
StepsListModel::flags(const QModelIndex& index) const
{
  if (index.isValid())
    return QAbstractListModel::flags(index);
  else
    return Qt::NoItemFlags;
}

void
StepsListModel::clear()
{
  beginResetModel();
  items.clear();
  endResetModel();
}

void
StepsListModel::createStep(const StepType type)
{
  qCDebug(SLM) << "StepsListModel::createStep"
               << QVariant::fromValue(type).toString();

  deduceStepType(type);
  insertRow(items.count() + 1);
}

void
StepsListModel::loadData(const QList<steps::AbstractStep*>& data)
{
  qCDebug(SLM) << "StepsListModel::loadData" << data.count();

  QListIterator<steps::AbstractStep*> steps_iter{ data };
  while (steps_iter.hasNext()) {
    auto step{ steps_iter.next() };
    auto stepType = static_cast<StepType>(step->getStepType());

    qCDebug(SLM) << "\t" << step->getStepName() << stepType;

    deduceStepType(stepType);
    step_item* newItem{ static_cast<step_item*>(
      factory->FactoryFromItem(step)) };
    items.append(newItem);
  }
}

void
StepsListModel::deduceStepType(const StepType type)
{
  switch (type) {
    case StepType::heater:
      factory.reset(new ItemStepHeaterFactory);
      break;
    case StepType::block:
      factory.reset(new ItemStepPauseFactory);
      break;
    case StepType::pickup:
      factory.reset(new ItemStepSleeveCollectFactory);
      break;
    case StepType::leave:
      factory.reset(new ItemStepSleeveReleaseFactory);
      break;
    case StepType::mix:
      factory.reset(new ItemStepMixFactory);
      break;
    case StepType::dry:
      factory.reset(new ItemStepDryFactory);
      break;
    case StepType::pause:
      factory.reset(new ItemStepSleepFactory);
      break;
    case StepType::move:
      factory.reset(new ItemStepManualMoveFactory);
      break;
    case StepType::collect:
      factory.reset(new ItemStepBeadsCollectFactory);
      break;
    case StepType::release:
      factory.reset(new ItemStepBeadsReleaseFactory);
      break;
    default:
      return;
  }
}

} // namespace models
