#include "mix_pattern_tablemodel.h"

#include <studio-lib/models/steps/mixing_stage.h>

#include <QLoggingCategory>
#include <studio-lib/logger.h>

Q_LOGGING_CATEGORY(MTM, "MXI TABLE")

namespace models {

MixPatternTableModel::MixPatternTableModel(QObject* parent)
  : QAbstractTableModel{ parent }
{}

MixPatternTableModel::~MixPatternTableModel() {}

int
MixPatternTableModel::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid())
    return 0;

  return items.count();
}

int
MixPatternTableModel::columnCount(const QModelIndex& parent) const
{
  if (parent.isValid())
    return 0;

  return cols::speed + 1;
}

QVariant
MixPatternTableModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
    return QVariant{};

  steps::MixingStage* stage = items.at(index.row());

  switch (static_cast<Qt::ItemDataRole>(role)) {
    case Qt::DisplayRole: {
      switch (static_cast<cols>(index.column())) {
        case time: {
          QTime time{ 0, 0, 0 };
          time = time.addSecs(stage->getDuration());
          return time.toString("HH:mm:ss");
        }
        case speed:
          return stage->getSpeed();
        default:
          return QVariant{};
      }
      break;
    }
    case Qt::EditRole: {
      switch (static_cast<cols>(index.column())) {
        case time: {
          QTime time{ 0, 0, 0 };
          time = time.addSecs(stage->getDuration());
          return time;
        }
        case speed: {
          return stage->getSpeed();
        }
        default:
          return QVariant{};
      }

      break;
    }
    case Qt::DecorationRole: {
      break;
    }
    default:
      break;
  }

  return QVariant{};
}

bool
MixPatternTableModel::setData(const QModelIndex& index,
                              const QVariant& value,
                              int role)
{
  if (!index.isValid() || static_cast<Qt::ItemDataRole>(role) != Qt::EditRole)
    return false;

  auto stage{ items.at(index.row()) };

  switch (static_cast<cols>(index.column())) {
    case time: {
      //      qCDebug(MTM) << value << value.toString();
      const QTime start{ 0, 0, 0 };
      const QTime time{ QTime::fromString(value.toString(),
                                          QStringLiteral("HH:mm:ss")) };
      //      qCDebug(MTM) << "setData // duration" << start << time
      //                   << time.secsTo(start) << start.secsTo(time);
      stage->setDuration(start.secsTo(time));
      break;
    }
    case speed: {
      //      qCDebug(MTM) << "setData // speed" << value.toInt();
      stage->setSpeed(value.toUInt());
      break;
    }
  }

  emit dataChanged(index, index);
  return true;
}

QVariant
MixPatternTableModel::headerData(int section,
                                 Qt::Orientation orientation,
                                 int role) const
{
  if (role != Qt::DisplayRole)
    return QVariant{};

  steps::MixingStage stage;

  switch (orientation) {
    case Qt::Horizontal:
      switch (static_cast<cols>(section)) {
        case time:
          return stage.duration->label();
        case speed:
          return stage.speed->label();
        default:
          return QVariant{};
      }
      break;
    case Qt::Vertical:
      return section + 1;
  }

  return QVariant{};
}

bool
MixPatternTableModel::insertRows(int row, int count, const QModelIndex& parent)
{
  if (parent.isValid())
    return false;

  beginInsertRows(QModelIndex{}, row, row + count - 1);

  for (int idx = 0; idx < count; ++idx) {
    steps::MixingStage* newItem = new steps::MixingStage;

    if (items.isEmpty())
      items.append(newItem);
    else
      items.insert(row, newItem);
  }

  endInsertRows();

  return true;
}

bool
MixPatternTableModel::removeRows(int row, int count, const QModelIndex& parent)
{
  if (parent.isValid())
    return false;

  if (items.isEmpty())
    return false;

  if (row < 0 || row > items.count() || (row + count) > items.count())
    return false;

  beginRemoveRows(QModelIndex{}, row, row + count - 1);

  for (int idx = 0; idx < count; ++idx)
    items.removeAt(row);

  endRemoveRows();

  return true;
}

Qt::ItemFlags
MixPatternTableModel::flags(const QModelIndex& index) const
{
  return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

void
MixPatternTableModel::clear()
{
  beginResetModel();
  items.clear();
  endResetModel();
}

void
MixPatternTableModel::loadData(const QJsonArray& json)
{}

} // namespace models
