#include "step_list_item.h"

StepListItem::StepListItem(const QJsonObject& _json)
  : AbstractListItem{ item_type::step }
{
  if (_json.contains("step_type"))
    m_step_type = static_cast<StepType>(
      _json.value("step_type").toInt(static_cast<int>(StepType::user)));

  if (_json.contains("step_name"))
    m_name = _json.value("step_name").toString(chooseDefaultName());
  else
    m_name = chooseDefaultName();

  m_icon = chooseDefaultIcon();
}

StepListItem::~StepListItem() {}

QIcon
StepListItem::chooseDefaultIcon()
{
  switch (m_step_type) {
    case StepType::pickup:
      return QIcon{ ":/48px/collapse_arrow_48px.png" };
    case StepType::leave:
      return QIcon{ ":/48px/expand_arrow_48px.png" };
    case StepType::mix:
      return QIcon{ ":/48px/curly_arrow_48px.png" };
    case StepType::dry:
      return QIcon{ ":/48px/dry_48px.png" };
    case StepType::pause:
      return QIcon{ ":/48px/timer_48px.png" };
    case StepType::move:
      return QIcon{ ":/48px/stepper_motor_48px.png" };
    case StepType::collect:
      return QIcon{ ":/48px/arrow_up_48px.png" };
    case StepType::release:
      return QIcon{ ":/48px/turn_48px.png" };
    case StepType::heater:
      return QIcon{ ":/48px/heating_48px.png" };
    case StepType::block:
      return QIcon{ ":/48px/sleep_48px.png" };
    default:
      return QIcon{ ":/48px/stop_squared_48px.png" };
  }
}

QString
StepListItem::chooseDefaultName()
{
  switch (m_step_type) {
    case StepType::pickup:
      return QObject::tr("Pick-Up");
    case StepType::leave:
      return QObject::tr("Leave");
    case StepType::mix:
      return QObject::tr("Mix");
    case StepType::dry:
      return QObject::tr("Dry");
    case StepType::pause:
      return QObject::tr("Pause");
    case StepType::move:
      return QObject::tr("Move");
    case StepType::collect:
      return QObject::tr("Collect");
    case StepType::release:
      return QObject::tr("Release");
    case StepType::heater:
      return QObject::tr("Heater control");
    case StepType::block:
      return QObject::tr("Block");
    default:
      return QObject::tr("Error");
  }
}
