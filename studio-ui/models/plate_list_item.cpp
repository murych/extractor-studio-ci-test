#include "plate_list_item.h"

PlateListItem::PlateListItem(const QJsonObject& json)
  : AbstractListItem{ item_type::plate }
{
  if (json.contains("plate_type"))
    m_plate_type = static_cast<plate_type>(
      json.value("plate_type").toInt(static_cast<int>(plate_type::user)));

  if (json.contains("plate_name"))
    m_name = json.value("plate_name").toString(QStringLiteral("Plate"));
  else
    m_name = QStringLiteral("Plate");

  m_icon = QIcon{ ":/96px/grid_view_96px.png" };
}

PlateListItem::~PlateListItem() {}
