#include "plate_list_model.h"

#include "logger.h"
#include <QIcon>
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(PLM, "PLATE LIST")
Q_LOGGING_CATEGORY(PI, "PLATE ITEM")

namespace models {

/*!
 * @brief The PlateListModel::ItemPlateFactory class
 * @note с фабриками получилось не очень удобно загружать уже существующие
 * проекты - много перегрузок фабричного метода. ПЕРЕОРГАНИЗОВАТЬ?
 */
class PlateListModel::ItemPlateFactory : public AbstractListModel::ItemFactory
{
public:
  ItemPlateFactory() = default;
  ~ItemPlateFactory() override = default;

private:
  void connectActions(plates::Plate* plate, widgets::PlateInfo* widget) const
  {
    connect(widget,
            &widgets::PlateInfo::plate_renamed,
            plate,
            &plates::Plate::setPlateName);
    connect(widget,
            &widgets::PlateInfo::plate_idx_changed,
            plate,
            &plates::Plate::setPlateIdx);
    connect(widget,
            &widgets::PlateInfo::plate_size_changed,
            plate,
            &plates::Plate::setPlateSize);
    connect(widget,
            &widgets::PlateInfo::plate_type_changed,
            plate,
            &plates::Plate::setPlateContent);
  }

public:
  AbstractListModel::list_item* FactoryMethod() const override
  {
    qCDebug(PLM) << "PlateListModel::ItemPlateFactory::FactoryMethod";
    return new PlateListModel::plate_item{ new plates::Plate,
                                           new widgets::PlateInfo };
  }

  AbstractListModel::list_item* FactoryWIndex(const int idx) const
  {
    qCDebug(PLM) << "PlateListModel::ItemPlateFactory::FactoryWIndex";
    AbstractListModel::list_item* newItem = this->FactoryMethod();

    plates::Plate* newPlate{ static_cast<plates::Plate*>(newItem->item) };
    widgets::PlateInfo* newWidget{ static_cast<widgets::PlateInfo*>(
      newItem->widget) };

    newWidget->loadPlateToUi(newPlate);

    connectActions(newPlate, newWidget);

    return newItem;
  }

  AbstractListModel::list_item* FactoryFromItem(plates::Plate* newPlate) const
  {
    qCDebug(PLM) << "PlateListModel::ItemPlateFactory:FactoryFromItem";
    widgets::PlateInfo* newWidget{ new widgets::PlateInfo };
    newWidget->loadPlateToUi(newPlate);
    connectActions(newPlate, newWidget);

    return new PlateListModel::plate_item{ newPlate, newWidget };
  }
};

PlateListModel::PlateListModel(QObject* parent)
  : AbstractListModel{ parent }
{
  factory.reset(new ItemPlateFactory);
}

PlateListModel::~PlateListModel() {}

int
PlateListModel::rowCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent)
  return items.count();
}

QVariant
PlateListModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
    return QVariant{};

  const auto plate = static_cast<plates::Plate*>(items.at(index.row())->item);

  switch (static_cast<Qt::ItemDataRole>(role)) {
    case Qt::DisplayRole:
    case Qt::EditRole:
      return plate->name->value();
    case Qt::DecorationRole:
      return QIcon{ ":/96px/grid_view_96px.png" };
    default:
      break;
  }

  return QVariant{};
}

QVariant
PlateListModel::headerData(int section,
                           Qt::Orientation orientation,
                           int role) const
{
  if (role != Qt::DisplayRole)
    return QVariant{};

  return (orientation == Qt::Horizontal) ? QString("Plate")
                                         : QString::number(section);
}

Qt::ItemFlags
PlateListModel::flags(const QModelIndex& index) const
{
  if (index.isValid())
    return QAbstractListModel::flags(index) /*| Qt::ItemIsEditable*/;
  else
    return Qt::NoItemFlags;
}

void
PlateListModel::createItem()
{
  qCDebug(PLM) << "PlateListModel::createItem";

  //! @warning нужно связать с максимальным количеством плашек в устройстве
  if (items.count() + 1 > 5)
    return;

  insertRow(items.count() + 1);
}

void
PlateListModel::clear()
{
  beginResetModel();
  items.clear();
  endResetModel();
}

// если ничего не выбрано, то row передается -1, нужно учитывать либо тут, либо
// уровнем выше
bool
PlateListModel::insertRows(int row, int count, const QModelIndex& parent)
{
  qCDebug(PLM) << "PlateListModel::insertRows"
               << "/row =" << row << "/count =" << count
               << "/parent is valid =" << parent.isValid();

  if (parent.isValid())
    return false;

  beginInsertRows(QModelIndex{}, row, row + count - 1);

  for (int idx = 0; idx < count; ++idx) {
    plate_item* newItem =
      static_cast<plate_item*>(factory->FactoryWIndex(row + idx));

    if (items.isEmpty())
      items.append(newItem);
    else
      items.insert(row, newItem);
  }

  endInsertRows();

  return true;
}

bool
PlateListModel::removeRows(int row, int count, const QModelIndex& parent)
{
  qCDebug(PLM) << "PlateListModel::removeRows"
               << "/row =" << row << "/count =" << count
               << "/parent is valid =" << parent.isValid();

  if (parent.isValid())
    return false;

  if (items.isEmpty())
    return false;

  if (row < 0 || row > items.count() || (row + count) > items.count())
    return false;

  beginRemoveRows(QModelIndex{}, row, row + count - 1);

  for (int idx = 0; idx < count; ++idx)
    items.removeAt(row);

  endRemoveRows();

  return true;
}

bool
PlateListModel::moveRows(const QModelIndex& sourceParent,
                         int sourceRow,
                         int count,
                         const QModelIndex& destinationParent,
                         int destinationChild)
{
  return false;
}

void
PlateListModel::loadData(QList<plates::Plate*>& plates)
{
  QListIterator<plates::Plate*> plate_iter{ plates };

  while (plate_iter.hasNext()) {
    auto plate{ plate_iter.next() };
    plate_item* newItem{ static_cast<plate_item*>(
      factory->FactoryFromItem(plate)) };
    items.append(newItem);
  }
}

PlateListModel::plate_item::~plate_item()
{
  qCDebug(PI) << "PlateListModel::plate_item::~plate_item";
  delete item;
  delete widget;
}

} // namespace models
