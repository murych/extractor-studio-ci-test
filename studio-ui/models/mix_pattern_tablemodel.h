#ifndef MIXPATTERNTABLEMODEL_H
#define MIXPATTERNTABLEMODEL_H

#include <QAbstractTableModel>

namespace models {

namespace steps {
class MixingStage;
}

class MixPatternTableModel : public QAbstractTableModel
{
  Q_OBJECT
public:
  explicit MixPatternTableModel(QObject* parent = nullptr);
  ~MixPatternTableModel() override;

  // QAbstractItemModel interface
public:
  int rowCount(const QModelIndex& parent) const override;
  int columnCount(const QModelIndex& parent) const override;
  QVariant data(const QModelIndex& index, int role) const override;
  bool setData(const QModelIndex& index,
               const QVariant& value,
               int role) override;
  QVariant headerData(int section,
                      Qt::Orientation orientation,
                      int role) const override;
  bool insertRows(int row, int count, const QModelIndex& parent) override;
  bool removeRows(int row, int count, const QModelIndex& parent) override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;

public:
  enum cols
  {
    time,
    speed
  };

  void clear();

  QList<steps::MixingStage*> items;

public slots:
  void loadData(const QJsonArray& json);
};

} // namespace models

#endif // MIXPATTERNTABLEMODEL_H
