#include "about_dialog.h"
#include "ui_about_dialog.h"

#include <QApplication>

namespace dialogs {

AboutDialog::AboutDialog(QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::AboutDialog)
{
  ui->setupUi(this);
  ui->l_version->setText(
    tr("Version: %1").arg(QApplication::applicationVersion()));
  setWindowTitle(QApplication::applicationName());
}

AboutDialog::~AboutDialog()
{
  delete ui;
}

} // namespace dialogs
