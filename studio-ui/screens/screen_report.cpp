#include "screen_report.h"
#include "ui_screen_report.h"

namespace screens {

ScreenReport::ScreenReport(QWidget* parent)
  : AbstractScreen{ QStringLiteral("REPORT"),
                    QIcon(":/32px/ratings_32px.png"),
                    parent }
  , ui{ new Ui::ScreenReport }
{
  ui->setupUi(this);
}

ScreenReport::~ScreenReport()
{
  delete ui;
}

void
ScreenReport::onProjectOpened()
{}

void
ScreenReport::onProjectClosed()
{}

} // namespace screens


void screens::ScreenReport::addCreateActions(const QList<QAction *>)
{
}
