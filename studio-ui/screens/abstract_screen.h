#ifndef ABSTRACTSCREEN_H
#define ABSTRACTSCREEN_H

#include <QHash>
#include <QIcon>
#include <QList>
#include <QMap>
#include <QMetaType>
#include <QPair>
#include <QToolButton>
#include <QWidget>

namespace screens {

class AbstractScreen : public QWidget
{
  Q_OBJECT
public:
  explicit AbstractScreen(const QString& title = "",
                          const QIcon& icon = {},
                          QWidget* parent = nullptr)
    : QWidget{ parent }
    , m_title{ title }
    , m_icon{ icon }
  {}

  QString title() { return m_title; }
  QIcon icon() { return m_icon; }

public slots:
  virtual void onProjectOpened() = 0;
  virtual void onProjectClosed() = 0;
  virtual void onProjectSaving() = 0;

  //  void addCreateAction(const QList<QAction*> list) { m_create_actions =
  //  list; }
  virtual void addCreateActions(const QList<QAction*>) = 0;

private:
  QString m_title{};
  QIcon m_icon{};

  // protected:
  //  QList<QAction*> m_create_actions;
};

} // namespace screens

#endif // ABSTRACTSCREEN_H
