#ifndef SCREENHOME_H
#define SCREENHOME_H

#include "abstract_screen.h"
#include <QWidget>

QT_BEGIN_NAMESPACE

namespace Ui {
class ScreenHome;
}

QT_END_NAMESPACE

namespace models {
namespace project {
class ExtractorProject;
class Laboratory;
class Layout;
class Protocol;
class Project;
}
}

namespace screens {

class ScreenHome : public AbstractScreen
{
  Q_OBJECT
private:
public:
  explicit ScreenHome(QWidget* parent = nullptr);
  ~ScreenHome();

  void loadData(const models::project::ExtractorProject* project) const;

private:
  Ui::ScreenHome* ui{ nullptr };

  // AbstractScreen interface
public slots:
  void onProjectOpened() override;
  void onProjectClosed() override;
  void onProjectSaving() override {}

  void addCreateActions(const QList<QAction*>) override {}

private slots:
  QString fillProjectInfo(const models::project::Project* project) const;
  QString fillLayoutInfo(const models::project::Layout* layout) const;
  QString fillProtocolInfo(const models::project::Protocol* protocol) const;
};

} // namespace screens

#endif // SCREENHOME_H
