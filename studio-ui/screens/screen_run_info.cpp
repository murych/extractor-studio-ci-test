#include "screen_run_info.h"
#include "ui_screen_run_info.h"

namespace screens {

ScreenRunInfo::ScreenRunInfo(QWidget* parent)
  : AbstractScreen{ QStringLiteral("RUN INFO"),
                    QIcon(":/32px/info_32px.png"),
                    parent }
  , ui{ new Ui::ScreenRunInfo }
{
  ui->setupUi(this);
}

ScreenRunInfo::~ScreenRunInfo()
{
  delete ui;
}

void
ScreenRunInfo::onProjectOpened()
{}

void
ScreenRunInfo::onProjectClosed()
{}

}
