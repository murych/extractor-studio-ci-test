#include "screen_home.h"
#include "ui_screen_home.h"

#include <QFile>
#include <QLoggingCategory>
#include <QTextStream>
#include <QXmlStreamWriter>
#include <studio-lib/logger.h>
#include <studio-lib/models/project/extractor_project.h>
#include <studio-lib/models/project/layout.h>
#include <studio-lib/models/project/project_info.h>
#include <studio-lib/models/project/protocol.h>

Q_LOGGING_CATEGORY(S_HOME, "S_HOME")

namespace screens {

ScreenHome::ScreenHome(QWidget* parent)
  : AbstractScreen{ tr("HOME"), QIcon{ ":/32px/home_32px.png" }, parent }
  , ui{ new Ui::ScreenHome }
{
  ui->setupUi(this);

  ui->gb_projectSummary->setEnabled(false);
}

ScreenHome::~ScreenHome()
{
  delete ui;
}

void
ScreenHome::loadData(const models::project::ExtractorProject* project) const
{
  qCDebug(S_HOME) << project->info->title->value();

  ui->l_projectInfo->setText(fillProjectInfo(project->info));
  ui->l_projectLayout->setText(fillLayoutInfo(project->layout));
  ui->l_projectProtocol->setText(fillProtocolInfo(project->protocol));
}

void
ScreenHome::onProjectOpened()
{
  ui->gb_projectSummary->setEnabled(true);
}

void
ScreenHome::onProjectClosed()
{
  ui->gb_projectSummary->setEnabled(false);
}

QString
ScreenHome::fillProjectInfo(const models::project::Project* project) const
{
  QString qHtmlFile;

  QTextStream startDocStream(&qHtmlFile);
  startDocStream << QStringLiteral("<!DOCTYPE html>");

  QXmlStreamWriter htmlWriter(&qHtmlFile);
  htmlWriter.writeStartElement(QStringLiteral("html"));
  htmlWriter.writeAttribute(QStringLiteral("xmlns"),
                            QStringLiteral("http://www.w3.org/1999/xhtml"));
  htmlWriter.writeAttribute(QStringLiteral("lang"), QStringLiteral("en"));
  htmlWriter.writeAttribute(
    QStringLiteral("xml"), QStringLiteral("lang"), QStringLiteral("en"));
  htmlWriter.writeStartElement(QStringLiteral("head"));
  htmlWriter.writeStartElement(QStringLiteral("meta"));
  htmlWriter.writeAttribute(QStringLiteral("http-equiv"),
                            QStringLiteral("Content-Type"));
  htmlWriter.writeAttribute(QStringLiteral("content"),
                            QStringLiteral("text/html; charset=utf-8"));
  htmlWriter.writeEndElement(); // meta
  htmlWriter.writeStartElement(QStringLiteral("title"));
  htmlWriter.writeCharacters(project->title->value());
  htmlWriter.writeEndElement(); // title
  htmlWriter.writeStartElement(QStringLiteral("style"));
  htmlWriter.writeCharacters(QStringLiteral(
    "h1, h2, h3, h4 { color: rgb(83,129,53) } h1 { text-align: left; }"));
  htmlWriter.writeEndElement(); // style
  htmlWriter.writeEndElement(); // head
  htmlWriter.writeStartElement(QStringLiteral("body"));
  htmlWriter.writeStartElement(QStringLiteral("h2"));
  htmlWriter.writeCharacters(project->title->value());
  htmlWriter.writeEndElement(); // h1
  htmlWriter.writeStartElement(QStringLiteral("p"));
  htmlWriter.writeCharacters(project->author->label());
  htmlWriter.writeCharacters(QLatin1String(": "));
  htmlWriter.writeCharacters(project->author->value());
  htmlWriter.writeEmptyElement(QStringLiteral("br"));
  htmlWriter.writeCharacters(project->date_created->label());
  htmlWriter.writeCharacters(QLatin1String(": "));
  htmlWriter.writeCharacters(project->date_created->toPrettyString());
  htmlWriter.writeEmptyElement(QStringLiteral("br"));
  htmlWriter.writeCharacters(project->description->label());
  htmlWriter.writeCharacters(QLatin1String(": "));
  htmlWriter.writeCharacters(project->description->value());
  htmlWriter.writeEndElement(); // p
  htmlWriter.writeEndElement(); // body
  htmlWriter.writeEndElement(); // html

  return qHtmlFile;
}

QString
ScreenHome::fillLayoutInfo(const models::project::Layout* layout) const
{
  QString qHtmlFile;

  QTextStream startDocStream(&qHtmlFile);
  startDocStream << QStringLiteral("<!DOCTYPE html>");

  QXmlStreamWriter htmlWriter(&qHtmlFile);
  htmlWriter.writeStartElement(QStringLiteral("html"));
  htmlWriter.writeAttribute(QStringLiteral("xmlns"),
                            QStringLiteral("http://www.w3.org/1999/xhtml"));
  htmlWriter.writeAttribute(QStringLiteral("lang"), QStringLiteral("en"));
  htmlWriter.writeAttribute(
    QStringLiteral("xml"), QStringLiteral("lang"), QStringLiteral("en"));
  htmlWriter.writeStartElement(QStringLiteral("head"));
  htmlWriter.writeStartElement(QStringLiteral("meta"));
  htmlWriter.writeAttribute(QStringLiteral("http-equiv"),
                            QStringLiteral("Content-Type"));
  htmlWriter.writeAttribute(QStringLiteral("content"),
                            QStringLiteral("text/html; charset=utf-8"));
  htmlWriter.writeEndElement(); // meta
  htmlWriter.writeStartElement(QStringLiteral("title"));
  htmlWriter.writeCharacters(layout->key());
  htmlWriter.writeEndElement(); // title
  htmlWriter.writeStartElement(QStringLiteral("style"));
  htmlWriter.writeCharacters(QStringLiteral(
    "h1, h2, h3, h4 { color: rgb(83,129,53) } h1 { text-align: left; }"));
  htmlWriter.writeEndElement(); // style
  htmlWriter.writeEndElement(); // head
  htmlWriter.writeStartElement(QStringLiteral("body"));
  htmlWriter.writeStartElement(QStringLiteral("p"));
  htmlWriter.writeCharacters(tr("This protocol has: %1 plates")
                               .arg(layout->plates->derivedEntities().count()));
  htmlWriter.writeEndElement(); // p
  htmlWriter.writeEndElement(); // body
  htmlWriter.writeEndElement(); // html

  return qHtmlFile;
}

QString
ScreenHome::fillProtocolInfo(const models::project::Protocol* protocol) const
{
  QString qHtmlFile;

  QTextStream startDocStream(&qHtmlFile);
  startDocStream << QStringLiteral("<!DOCTYPE html>");

  QXmlStreamWriter htmlWriter(&qHtmlFile);
  htmlWriter.writeStartElement(QStringLiteral("html"));
  htmlWriter.writeAttribute(QStringLiteral("xmlns"),
                            QStringLiteral("http://www.w3.org/1999/xhtml"));
  htmlWriter.writeAttribute(QStringLiteral("lang"), QStringLiteral("en"));
  htmlWriter.writeAttribute(
    QStringLiteral("xml"), QStringLiteral("lang"), QStringLiteral("en"));
  htmlWriter.writeStartElement(QStringLiteral("head"));
  htmlWriter.writeStartElement(QStringLiteral("meta"));
  htmlWriter.writeAttribute(QStringLiteral("http-equiv"),
                            QStringLiteral("Content-Type"));
  htmlWriter.writeAttribute(QStringLiteral("content"),
                            QStringLiteral("text/html; charset=utf-8"));
  htmlWriter.writeEndElement(); // meta
  htmlWriter.writeStartElement(QStringLiteral("title"));
  htmlWriter.writeCharacters(protocol->key());
  htmlWriter.writeEndElement(); // title
  htmlWriter.writeStartElement(QStringLiteral("style"));
  htmlWriter.writeCharacters(QStringLiteral(
    "h1, h2, h3, h4 { color: rgb(83,129,53) } h1 { text-align: left; }"));
  htmlWriter.writeEndElement(); // style
  htmlWriter.writeEndElement(); // head
  htmlWriter.writeStartElement(QStringLiteral("body"));
  htmlWriter.writeStartElement(QStringLiteral("p"));
  htmlWriter.writeCharacters(
    tr("This protocol has: %1 steps")
      .arg(protocol->steps->derivedEntities().count()));
  htmlWriter.writeEndElement(); // p
  htmlWriter.writeEndElement(); // body
  htmlWriter.writeEndElement(); // html

  return qHtmlFile;
}

} // namespace screens
