#include "screen_layout.h"
#include "ui_screen_layout.h"

#include <QLoggingCategory>
#include <QMenu>
#include <studio-lib/logger.h>
#include <studio-lib/models/project/layout.h>

Q_LOGGING_CATEGORY(SL, "LAYOUT")

namespace screens {

ScreenLayout::ScreenLayout(QWidget* parent)
  : AbstractScreen{ tr("LAYOUT"), QIcon{ ":/32px/grid_view_32px.png" }, parent }
  , ui{ new Ui::ScreenLayout } //  , layout{ new models::project::Layout }
  , menu_add_plate{ new QMenu }
  , model{ new models::PlateListModel }
{
  layout.reset(new models::project::Layout);

  ui->setupUi(this);
  ui->list->setAddBtnAction(ui->actionAddPlate);

  ui->list->setModel(model);

  connect(model,
          &models::AbstractListModel::openRelatedWidget,
          ui->view,
          &widgets::PlateItemWidget::setCurrentIndex);

  connect(ui->list,
          &widgets::AbstractListView::item_deleted,
          this,
          &ScreenLayout::deletePlate);
}

ScreenLayout::~ScreenLayout()
{
  delete ui;
  //  delete layout;
  delete model;
  delete menu_add_plate;
}

void
ScreenLayout::loadData(models::project::Layout* _layout)
{
  layout.reset(_layout);

  model->clear();
  model->loadData(layout->plates->derivedEntities());

  using plate_item_t = models::PlateListModel::plate_item*;
  using plate_items = QList<plate_item_t>;
  const plate_items plates = model->getItems();
  QListIterator<plate_item_t> plate_iter{ plates };

  while (plate_iter.hasNext()) {
    plate_item_t item = plate_iter.next();
    ui->view->addWidget(item->widget);
  }
}

void
ScreenLayout::createPlate(const PlateSize size)
{
  //! @todo как-то после создания новой плашки отсылать сигнал в ui->view что
  //! тоже нужно бы выбрать соответствующий виджет

  Q_UNUSED(size)

  qCDebug(SL) << "ScreenLayout::createPlate";

  model->createItem();
  auto current_item = model->getItems().constLast();
  ui->view->addWidget(current_item->widget);

  qCDebug(SL) << "after adding new plate there are" << model->getItems().count()
              << "items in model";
  qCDebug(SL) << "after adding new plate there are" << ui->view->count()
              << "widgets in stacked view";
}

void
ScreenLayout::deletePlate(const QModelIndex& idx)
{
  //! @todo переделать удаление, ну
  qCDebug(SL) << "ScreenLayout::deletePlate"
              << idx; /*<< ui->view->currentIndex()
<< ui->view->currentWidget();*/

  qCDebug(SL) << "is model empty" << model->getItems().isEmpty()
              << "model items count:" << model->getItems().count()
              << "is requested index > items count:"
              << (idx.row() > model->getItems().count());
  if (model->getItems().isEmpty())
    return;

  if (idx.row() > model->getItems().count())
    return;

  models::PlateListModel::plate_item* item =
    model->getItems().at(idx.row()) ? model->getItems().at(idx.row())
                                    : model->getItems().at(idx.row() - 1);
  qCDebug(SL) << item->item->toJson() << item->widget->objectName();

  ui->view->removeWidget(item->widget);
  model->removeRow(idx.row());
}

void
ScreenLayout::onProjectOpened()
{}

void
ScreenLayout::onProjectClosed()
{
  layout->plates->derivedEntities().clear();
}

void
ScreenLayout::onProjectSaving()
{
  qCDebug(SL) << "ScreenLayout::onProjectSaving"
              << "adding plates to project's" << layout << "layout";
  using plate_item_t = models::PlateListModel::plate_item*;
  using plate_items = QList<plate_item_t>;
  const plate_items plates = model->getItems();
  QListIterator<plate_item_t> plate_iter{ plates };

  while (plate_iter.hasNext()) {
    auto item = plate_iter.next();

    qCDebug(SL) << "adding plate" << item->item->key();
    auto plate = static_cast<models::plates::Plate*>(item->item);

    const auto plateConent{ static_cast<PlateType>(plate->plateContent()) };
    if (plateConent == PlateType::liquids) {
      using reagent_t = models::plates::Reagent*;
      using reagents_list = QList<reagent_t>;

      const auto widget = static_cast<widgets::PlateInfo*>(item->widget);
      const reagents_list reagents{ widget->collectReagents() };
      QListIterator<reagent_t> iter{ reagents };
      while (iter.hasNext())
        plate->reagents->addEntity(iter.next());

    } else if (plateConent == PlateType::sleeves) {
      // collect sleeves info?
    }
    layout->plates->addEntity(plate);
  }
}

void
ScreenLayout::addCreateActions(const QList<QAction*> list)
{
  menu_add_plate->addActions(list);
  ui->list->setAddBtnMenu(menu_add_plate);
}

} // namespace screens
