#include "screen_protocol.h"
#include "ui_screen_protocol.h"

#include "widgets/step_info_mix.h"
#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QMenu>
#include <studio-lib/logger.h>
#include <studio-lib/models/project/protocol.h>
#include <studio-lib/models/steps/step_mix.h>

Q_LOGGING_CATEGORY(SP, "PROTOCOL")

namespace screens {

ScreenProtocol::ScreenProtocol(QWidget* parent)
  : AbstractScreen{ tr("PROTOCOL"), QIcon(":/32px/outline_32px.png"), parent }
  , ui{ new Ui::ScreenProtocol }
  , menu_add_step{ new QMenu } //  , protocol{ new models::project::Protocol }
  , model{ new models::StepsListModel }
{
  protocol.reset(new models::project::Protocol);

  ui->setupUi(this);
  ui->list->setAddBtnAction(ui->actionAddStep);
  ui->list->setModel(model);

  connect(model,
          &models::AbstractListModel::openRelatedWidget,
          ui->view,
          &widgets::ProtocolItemWidget::setCurrentIndex);

  connect(ui->list,
          &widgets::AbstractListView::item_deleted,
          this,
          &ScreenProtocol::deleteStep);

  /*
  QFile jsonFile{ ":/project-sample" };
  jsonFile.open(QIODevice::ReadOnly | QIODevice::Text);
  QByteArray json = jsonFile.readAll();
  jsonFile.close();

  QJsonParseError parseError;
  QJsonObject jsonObject = QJsonDocument::fromJson(json, &parseError).object();

  if (parseError.error == QJsonParseError::NoError &&
      jsonObject.contains(protocol->key())) {
    QJsonObject jsonProtocol = jsonObject.value(protocol->key()).toObject();

    ui->list->loadData(jsonProtocol);
    ui->view->loadData(jsonProtocol);
    if (ui->list->dataSize() != ui->view->dataSize()) {
      ui->list->clear();
      ui->view->clear();
    }
  }

  QList<models::AbstractStep*> steps = ui->view->getSteps();
  for (auto& step : steps)
    protocol->steps->addEntity(step);
  */
}

ScreenProtocol::~ScreenProtocol()
{
  delete ui;
  //  delete protocol;
}

void
ScreenProtocol::loadData(models::project::Protocol* _protocol)
{
  protocol.reset(_protocol);

  model->clear();
  model->loadData(protocol->steps->derivedEntities());

  using step_item_t = models::StepsListModel::step_item*;
  using step_items = QList<step_item_t>;
  const step_items steps = model->getItems();
  QListIterator<step_item_t> iter{ steps };

  while (iter.hasNext()) {
    step_item_t item = iter.next();
    ui->view->addWidget(item->widget);
  }
}

void
ScreenProtocol::createStep(const StepType type)
{
  qDebug() << "ScreenProtocol::createStep"
           << QVariant::fromValue(type).toString();

  model->createStep(type);

  auto current_item = model->getItems().constLast();

  qCDebug(SP) << current_item->item << current_item->widget;

  ui->view->addWidget(current_item->widget);

  qCDebug(SP) << "after adding new step there are" << model->getItems().count()
              << "items in model";
  qCDebug(SP) << "after adding new step there are" << ui->view->count()
              << "widgets in stacked view";
}

void
ScreenProtocol::deleteStep(const QModelIndex& idx)
{
  qCDebug(SP) << "ScreenProtocol::deleteStep" << idx;

  if (model->getItems().isEmpty())
    return;

  if (idx.row() > model->getItems().count())
    return;

  auto item = model->getItems().at(idx.row());

  ui->view->removeWidget(item->widget);
  model->removeRow(idx.row());
}

void
ScreenProtocol::onProjectOpened()
{}

void
ScreenProtocol::onProjectClosed()
{
  protocol->steps->derivedEntities().clear();
}

void
ScreenProtocol::onProjectSaving()
{
  qCDebug(SP) << "ScreenProtocol::onProjectSaving"
              << "adding steps to project's" << protocol << "protocol";

  using step_item_t = models::StepsListModel::step_item*;
  using step_items = QList<step_item_t>;
  const step_items steps = model->getItems();
  QListIterator<step_item_t> iter{ steps };

  while (iter.hasNext()) {
    auto item = iter.next();
    qCDebug(SP) << "adding step" << item->item->key();

    auto stepType =
      static_cast<models::steps::AbstractStep*>(item->item)->getStepType();

    if (stepType == StepType::mix) {
      // collect mix stages
      using mix_stage_t = models::steps::MixingStage*;
      using mix_stages = QList<mix_stage_t>;

      auto widget{ static_cast<widgets::StepInfoMix*>(item->widget) };
      auto step{ static_cast<models::steps::StepMix*>(item->item) };

      const mix_stages mixStages{ widget->collectMixingStages() };
      QListIterator<mix_stage_t> mix_iter{ mixStages };

      while (mix_iter.hasNext())
        step->mix_stages->addEntity(mix_iter.next());

      protocol->steps->addEntity(step);
    } else {
      auto step = static_cast<models::steps::AbstractStep*>(item->item);
      protocol->steps->addEntity(step);
    }
  }
}

void
ScreenProtocol::addCreateActions(const QList<QAction*> list)
{
  menu_add_step->addActions(list);
  ui->list->setAddBtnMenu(menu_add_step);
}

}
