#ifndef SCREEN_PROTOCOL_H
#define SCREEN_PROTOCOL_H

#include "abstract_screen.h"
#include "models/steps_list_model.h"

namespace Ui {
class ScreenProtocol;
}

namespace models {
namespace project {
class Protocol;
}
}

class QMenu;

namespace screens {

class ScreenProtocol : public AbstractScreen
{
  Q_OBJECT

public:
  explicit ScreenProtocol(QWidget* parent = nullptr);
  ~ScreenProtocol() override;

  void loadData(models::project::Protocol* _protocol);

private:
  Ui::ScreenProtocol* ui{ nullptr };
  QMenu* menu_add_step{ nullptr };
  models::StepsListModel* model{ nullptr };

  //  models::project::Protocol* protocol{ nullptr };
  QScopedPointer<models::project::Protocol> protocol;

private slots:
  void createStep(const StepType type);
  void deleteStep(const QModelIndex& idx);

  // AbstractScreen interface
public slots:
  void onProjectOpened() override;
  void onProjectClosed() override;
  void onProjectSaving() override;

  void addCreateActions(const QList<QAction*> list) override;

  void onCreateStepSleevesCollect() { createStep(StepType::pickup); }
  void onCreateStepSleevesRelease() { createStep(StepType::leave); }
  void onCreateStepBeadsCollect() { createStep(StepType::collect); }
  void onCreateStepBeadsRelease() { createStep(StepType::release); }
  void onCreateStepMix() { createStep(StepType::mix); }
  void onCreateStepDry() { createStep(StepType::dry); }
  void onCreateStepPause() { createStep(StepType::pause); }
  void onCreateStepBlock() { createStep(StepType::block); }
  void onCreateStepManualMove() { createStep(StepType::move); }
  void onCreateStepHeater() { createStep(StepType::heater); }
};

} // namespace screens

#endif // SCREEN_PROTOCOL_H
