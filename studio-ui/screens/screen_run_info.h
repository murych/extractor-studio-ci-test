#ifndef SCREEN_RUN_INFO_H
#define SCREEN_RUN_INFO_H

#include "abstract_screen.h"
#include <QWidget>

namespace Ui {
class ScreenRunInfo;
}

namespace screens {

class ScreenRunInfo : public AbstractScreen
{
  Q_OBJECT

public:
  explicit ScreenRunInfo(QWidget* parent = nullptr);
  ~ScreenRunInfo();

private:
  Ui::ScreenRunInfo* ui{ nullptr };

  // AbstractScreen interface
public slots:
  void onProjectOpened();
  void onProjectClosed();
};

} // namespace screens

#endif // SCREEN_RUN_INFO_H
