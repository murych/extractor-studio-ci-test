#ifndef SCREEN_LAYOUT_H
#define SCREEN_LAYOUT_H

#include "abstract_screen.h"

#include "models/plate_list_model.h"

namespace Ui {
class ScreenLayout;
}

class QMenu;

namespace models {
namespace project {
class Layout;
}
}

namespace screens {

class ScreenLayout : public AbstractScreen
{
  Q_OBJECT

public:
  explicit ScreenLayout(QWidget* parent = nullptr);
  ~ScreenLayout();

  void loadData(models::project::Layout* _layout);

private:
  Ui::ScreenLayout* ui{ nullptr };
  QMenu* menu_add_plate{ nullptr };
  models::PlateListModel* model{ nullptr };

private:
  QScopedPointer<models::project::Layout> layout;

private slots:
  void createPlate(const PlateSize size);
  void deletePlate(const QModelIndex& idx);

  // AbstractScreen interface
public slots:
  void onProjectOpened() override;
  void onProjectClosed() override;
  void onProjectSaving() override;

  void addCreateActions(const QList<QAction*> list) override;

  void onAddStandard96Plate() { createPlate(PlateSize::standard_96_plate); }
};

} // namespace screens

#endif // SCREEN_LAYOUT_H
