#ifndef SCREEN_REPORT_H
#define SCREEN_REPORT_H

#include "abstract_screen.h"

namespace Ui {
class ScreenReport;
}

namespace screens {

class ScreenReport : public AbstractScreen
{
  Q_OBJECT

public:
  explicit ScreenReport(QWidget* parent = nullptr);
  ~ScreenReport() override;

private:
  Ui::ScreenReport* ui{ nullptr };

  // AbstractScreen interface
public slots:
  void onProjectOpened() override;
  void onProjectClosed() override;
  void onProjectSaving() override {}

  // AbstractScreen interface
public slots:
  void addCreateActions(const QList<QAction *>) override;
};

} // namespace screens

#endif // SCREEN_REPORT_H
