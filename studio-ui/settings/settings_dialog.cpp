#include "settings_dialog.h"
#include "ui_settings_dialog.h"

#include <QSettings>

namespace settings {

SettingsDialog::SettingsDialog(QWidget* parent)
  : QDialog{ parent }
  , ui{ new Ui::SettingsDialog }
{
  ui->setupUi(this);

  /*
  QSettings settings(QSettings::IniFormat,
                     QSettings::UserScope,
                     QApplication::organizationName(),
                     QApplication::applicationName());
  this->move(settings.value("application/settings_pos").toPoint());
  this->resize(settings.value("application/settings_size").toSize());
  */

  connect(this, &QDialog::accepted, ui->pageLab, &PageLabInfo::saveSettings);
}

SettingsDialog::~SettingsDialog()
{
  /*
  QSettings settings(QSettings::IniFormat,
                     QSettings::UserScope,
                     QApplication::organizationName(),
                     QApplication::applicationName());
  settings.setValue("application/settings_pos", this->pos());
  settings.setValue("application/settings_size", this->size());
  */

  delete ui;
}

} // namespace settings
