#include "settings_device_info.h"
#include "ui_settings_device_info.h"

namespace settings {

PageDevice::PageDevice(QWidget* parent)
  : QWidget{ parent }
  , ui(new Ui::SettingsPageDeviceInfo)
{
  ui->setupUi(this);
}

PageDevice::~PageDevice()
{
  delete ui;
}

} // namespace settings
