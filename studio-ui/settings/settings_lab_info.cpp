#include "settings_lab_info.h"
#include "ui_settings_lab_info.h"

#include <studio-lib/config/config_lab.h>

namespace settings {

PageLabInfo::PageLabInfo(QWidget* parent)
  : QWidget{ parent }
  , ui{ new Ui::PageLabInfo }
  , settings{ new config::LaboratoryConfig }
{
  ui->setupUi(this);
  settings->readSettings();

  ui->laboratoryNameLineEdit->setText(settings->lab_name);
  ui->laboratoryAddressLineEdit->setText(settings->lab_address);
  ui->laboratoryEmailLineEdit->setText(settings->lab_email);
  ui->laboratoryPhoneLineEdit->setText(settings->lab_phone);
}

PageLabInfo::~PageLabInfo()
{
  delete ui;
  delete settings;
}

void
PageLabInfo::saveSettings()
{
  settings->lab_name = ui->laboratoryNameLineEdit->text();
  settings->lab_address = ui->laboratoryAddressLineEdit->text();
  settings->lab_email = ui->laboratoryEmailLineEdit->text();
  settings->lab_phone = ui->laboratoryPhoneLineEdit->text();

  settings->writeSettings();
}

} // namespace settings
