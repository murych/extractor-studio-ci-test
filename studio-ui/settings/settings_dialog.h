#ifndef SETTINGS_DIALOG_H
#define SETTINGS_DIALOG_H

#include <QDialog>

namespace Ui {
class SettingsDialog;
}

namespace settings {

class SettingsDialog : public QDialog
{
  Q_OBJECT

public:
  explicit SettingsDialog(QWidget* parent = nullptr);
  ~SettingsDialog();

private:
  Ui::SettingsDialog* ui{ nullptr };
};

} // namespace settings

#endif // SETTINGS_DIALOG_H
