#include "settings_general_settings.h"
#include "ui_settings_general_settings.h"

namespace settings {

PageGeneral::PageGeneral(QWidget* parent)
  : QWidget{ parent }
  , ui{ new Ui::PageGeneralSettings }
{
  ui->setupUi(this);
}

PageGeneral::~PageGeneral()
{
  delete ui;
}

} // namespace settings
