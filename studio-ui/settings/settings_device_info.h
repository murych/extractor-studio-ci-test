#ifndef PAGE_DEVICE_INFO_H
#define PAGE_DEVICE_INFO_H

#include <QWidget>

namespace Ui {
class SettingsPageDeviceInfo;
}

namespace settings {

class PageDevice : public QWidget
{
  Q_OBJECT

public:
  explicit PageDevice(QWidget* parent = nullptr);
  ~PageDevice();

private:
  Ui::SettingsPageDeviceInfo* ui{ nullptr };
};

} // namespace settings

#endif // PAGE_DEVICE_INFO_H
