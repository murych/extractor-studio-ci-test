#ifndef PAGE_LAB_INFO_H
#define PAGE_LAB_INFO_H

#include <QWidget>

namespace Ui {
class PageLabInfo;
}

namespace config {
class LaboratoryConfig;
}

namespace settings {

class PageLabInfo : public QWidget
{
  Q_OBJECT

public:
  explicit PageLabInfo(QWidget* parent = nullptr);
  ~PageLabInfo();

public slots:
  void saveSettings();

private:
  Ui::PageLabInfo* ui{ nullptr };
  config::LaboratoryConfig* settings{ nullptr };
};

} // namespace settings

#endif // PAGE_LAB_INFO_H
