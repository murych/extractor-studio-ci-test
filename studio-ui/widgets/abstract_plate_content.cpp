#include "abstract_plate_content.h"

namespace widgets {

AbstractPlateContent::AbstractPlateContent(QWidget* parent)
  : QGroupBox{ parent }
{
  setTitle("Abstract one");
}

AbstractPlateContent::~AbstractPlateContent() {}

} // namespace widgets
