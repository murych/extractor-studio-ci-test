#ifndef PROTOCOL_ITEM_WIDGET_H
#define PROTOCOL_ITEM_WIDGET_H

//#include <QJsonArray>
//#include <QJsonDocument>
//#include <QJsonObject>
//#include <studio-lib/models/abstract_step.h>
#include <QStackedWidget>

namespace Ui {
class ProtocolItemWidget;
}

// namespace models {
// class AbstractStep;
//}

namespace widgets {

class AbstractStepWidget;

/*!
 * \brief The ProtocolItemWidget class
 * \todo может также объединить с PlateItemWidget в один "абстрактный" класс
 * базирующийся на QStackedWidget - такто ведь ему все равно какой виджет внутри
 * себя размещать?
 */
class ProtocolItemWidget : public QStackedWidget
{
  Q_OBJECT

public:
  explicit ProtocolItemWidget(QWidget* parent = nullptr);
  ~ProtocolItemWidget();

private:
  Ui::ProtocolItemWidget* ui{ nullptr };
  /*
public slots:
  //  void loadData(const QJsonObject& json);
  void selectStep(const QModelIndex& idx);

  int dataSize() { return protocol_steps_widgets.size(); }
  void clear() { protocol_steps_widgets.clear(); }

  void addStep(models::AbstractStep* step);

  QList<models::AbstractStep*> getSteps();


  QList<AbstractStepWidget*> protocol_steps_widgets;

private:
  //  class FactoryStep;
  class FactoryWidget;
  class FactoryWidgetMix;
  class FactoryWidgetDry;
  class FactoryWidgetBeadsCollect;
  class FactoryWidgetBeadsRelease;
  class FactoryWidgetSleeveCollect;
  class FactoryWidgetSleeveRelease;
  class FactoryWidgetHeater;
  class FactoryWidgetManualMove;
  class FactoryWidgetPause;
  class FactoryWidgetSleep;

  QScopedPointer<FactoryWidget> factory_step;
  //  QScopedPointer<FactoryWidget> factory_widget;
  */
};

}

#endif // PROTOCOL_ITEM_WIDGET_H
