#ifndef PLATE_ITEM_WIDGET_H
#define PLATE_ITEM_WIDGET_H

//#include "widgets/plate_info.h"
//#include <QJsonArray>
//#include <QJsonDocument>
//#include <QJsonObject>
//#include <QPair>
#include <QStackedWidget>

namespace Ui {
class PlateItemWidget;
}

// namespace models {
// class Plate;
//}

namespace widgets {

class PlateItemWidget : public QStackedWidget
{
  Q_OBJECT

public:
  explicit PlateItemWidget(QWidget* parent = nullptr);
  ~PlateItemWidget();

  //  QList<models::Plate*> getPlates();

  // public slots:
  //  void loadData(const QJsonObject& json);
  //  void selectPlate(const QModelIndex& idx);
  //  void addPlate(widgets::PlateInfo* plate);

  //  void renameItem(const QModelIndex& idx, const QString& name);

private:
  Ui::PlateItemWidget* ui{ nullptr };
  //  QList<PlateInfo*> plate_widgets{};

  // private slots:
  //  void onItemRenamed(const QString name);

  // signals:
  //  void plate_renamed(const int idx, const QString& name);
};

} // namespace widgets

#endif // PLATE_ITEM_WIDGET_H
