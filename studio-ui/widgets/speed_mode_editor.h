#ifndef WIDGETS_SPEEDMODEEDITOR_H
#define WIDGETS_SPEEDMODEEDITOR_H

#include <QComboBox>
#include <QIcon>

namespace widgets {

class SpeedModeEditor : public QComboBox
{
  Q_OBJECT
public:
  SpeedModeEditor(QWidget* parent = nullptr);
  ~SpeedModeEditor() override;

private:
  void populateList();
};

} // namespace widgets

#endif // WIDGETS_SPEEDMODEEDITOR_H
