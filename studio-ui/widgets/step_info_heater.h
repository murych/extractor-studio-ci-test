#ifndef STEP_INFO_HEATER_H
#define STEP_INFO_HEATER_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoHeater;
}

namespace widgets {

class StepInfoHeater : public AbstractStepWidget
{
  Q_OBJECT

public:
  explicit StepInfoHeater(QWidget* parent = nullptr);
  ~StepInfoHeater() override;

private:
  Ui::StepInfoHeater* ui{ nullptr };

  // AbstractStepWidget interface
public:
  void loadStepToUi(models::steps::AbstractStep* _step) override;

signals:
  void blockExecutionChanged(const bool value);
  void targetTemperatureChanged(const double value);
};

} // namespace widgets

#endif // STEP_INFO_HEATER_H
