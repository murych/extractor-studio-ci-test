#ifndef PLATE_INFO_H
#define PLATE_INFO_H

#include <QWidget>
#include <studio-lib/models/plates/plate.h>

namespace Ui {
class PlateInfo;
}

namespace models {
namespace plates {
class Reagent;
}
}

namespace widgets {

class PlateInfo : public QWidget
{
  Q_OBJECT

public:
  PlateInfo(QWidget* parent = nullptr);
  ~PlateInfo();

private:
  Ui::PlateInfo* ui{ nullptr };

public slots:
  void loadPlateToUi(const models::plates::Plate* plate);
  void loadUiToPlate();

  QList<models::plates::Reagent*> collectReagents();

private slots:
  //  void on_platePositionComboBox_currentIndexChanged(int index);

signals:
  void plate_renamed(const QString& name);
  void plate_idx_changed(const int idx);
  void plate_type_changed(const int type);
  void plate_size_changed(const int size);

private:
  enum plate_info_pages
  {
    liquids = 0,
    sleeves
  };
};

} // namespace widgets

#endif // PLATE_INFO_H
