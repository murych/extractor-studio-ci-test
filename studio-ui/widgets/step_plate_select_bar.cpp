#include "step_plate_select_bar.h"
#include "ui_step_plate_select_bar.h"

#include <QPalette>

namespace widgets {

StepPlateSelectBar::StepPlateSelectBar(QWidget* parent)
  : QWidget{ parent }
  , ui{ new Ui::StepPlateSelectBar }
{
  ui->setupUi(this);

  QPalette palette = QPalette{};
  palette.setColor(QPalette::Window, QColor{ 173, 127, 168 });

  this->setAutoFillBackground(true);
  this->setPalette(palette);

  connect(ui->cb_plateIdx,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &StepPlateSelectBar::plateChanged);
}

StepPlateSelectBar::~StepPlateSelectBar()
{
  delete ui;
}

void
StepPlateSelectBar::setPlate(const int plateIdx)
{
  if (plateIdx < 0 || plateIdx > ui->cb_plateIdx->count())
    return;

  ui->cb_plateIdx->setCurrentIndex(plateIdx);
}

} // namespace widgets
