#ifndef STEP_INFO_LEAVE_H
#define STEP_INFO_LEAVE_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoSleevesRelease;
}

namespace widgets {

class StepInfoSleevesRelease : public AbstractStepWidget
{
  Q_OBJECT

public:
  explicit StepInfoSleevesRelease(QWidget* parent = nullptr);
  ~StepInfoSleevesRelease() override;

private:
  Ui::StepInfoSleevesRelease* ui{ nullptr };

  // AbstractStepWidget interface
public:
  void loadStepToUi(models::steps::AbstractStep* _step) override;
};

} // namespace widgets

#endif // STEP_INFO_LEAVE_H
