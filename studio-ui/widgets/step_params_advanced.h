#ifndef WIDGETS_STEP_PARAMS_ADVANCED_H
#define WIDGETS_STEP_PARAMS_ADVANCED_H

#include <QWidget>

namespace Ui {
class StepParamsAdvanced;
}

namespace models {
namespace steps {
class AbstractStep;
}
}

namespace widgets {

class StepParamsAdvanced : public QWidget
{
  Q_OBJECT

public:
  explicit StepParamsAdvanced(QWidget* parent = nullptr);
  ~StepParamsAdvanced();

private:
  Ui::StepParamsAdvanced* ui{ nullptr };

public slots:
  void loadStepToUi(const models::steps::AbstractStep* step);

signals:
  void endPositionChanged(const int value);
  void verticalSpeedChanged(const int value);
  void horizontalSpeedChanged(const int value);
};

} // namespace widgets
#endif // WIDGETS_STEP_PARAMS_ADVANCED_H
