#ifndef STEP_INFO_DRY_H
#define STEP_INFO_DRY_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoDry;
}

namespace widgets {

class StepInfoDry : public AbstractStepWidget
{
  Q_OBJECT

public:
  explicit StepInfoDry(QWidget* parent = nullptr);
  ~StepInfoDry() override;

private:
  Ui::StepInfoDry* ui{ nullptr };

  // AbstractStepWidget interface
public:
  void loadStepToUi(models::steps::AbstractStep* _step) override;

signals:
  void dryDurationChanged(const int value);
  void dryPositionChanged(const int value);
};

} // namespace widgets

#endif // STEP_INFO_DRY_H
