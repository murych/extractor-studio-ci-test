#ifndef ABSTRACT_LIST_VIEW_H
#define ABSTRACT_LIST_VIEW_H

#include "models/abstract_list_model.h"
#include <QModelIndex>
#include <QWidget>

namespace Ui {
class AbstractListView;
}

class QMenu;
class QStandardItemModel;

namespace widgets {

class AbstractListView : public QWidget
{
  Q_OBJECT

public:
  explicit AbstractListView(QWidget* parent = nullptr);
  virtual ~AbstractListView();

  void setAddBtnMenu(QMenu* menu);
  void setAddBtnAction(QAction* action);
  void loadData(const QJsonObject& json);

  void setModel(models::AbstractListModel* _model);

public slots:
  int dataSize();
  void clear();

  //! @deprecated
  bool addItem();

  void removeItem();
  void moveItem(const int src, const int dst);
  void copyItem(const int idx);
  void cutItem(const int idx);
  void pasteItem(const int idx);

private:
  Ui::AbstractListView* ui{ nullptr };
  QModelIndex current_selection{ QModelIndex{} };

  bool no_data{ true };

signals:
  void item_selected(const QModelIndex& idx);
  void item_renamed(const QModelIndex& idx, const QString& name);
  void item_deleted(const QModelIndex& idx);

private slots:
  void on_listView_rowsInserted(const QModelIndex& index, int first, int last);
};

} // namespace widgets

#endif // ABSTRACT_LIST_VIEW_H
