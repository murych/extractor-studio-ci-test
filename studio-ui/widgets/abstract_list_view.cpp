#include "abstract_list_view.h"
#include "ui_abstract_list_view.h"

#include <QAbstractItemModel>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QLoggingCategory>
#include <QMenu>
#include <QScopedPointer>
#include <QStandardItem>
#include <studio-lib/logger.h>
//#include <studio-lib/models/project/layout.h>
//#include <studio-lib/models/project/protocol.h>

Q_LOGGING_CATEGORY(ALW, "LIST VIEW")

namespace widgets {

AbstractListView::AbstractListView(QWidget* parent)
  : QWidget{ parent }
  , ui{ new Ui::AbstractListView }
{
  ui->setupUi(this);

  connect(ui->b_itemDelete,
          &QToolButton::clicked,
          this,
          &AbstractListView::removeItem);

  //! @todo включить назад после реализации всех действий
  ui->b_itemCopy->setVisible(false);
  ui->b_itemCut->setVisible(false);
  ui->b_itemMoveDown->setVisible(false);
  ui->b_itemMoveUp->setVisible(false);
  ui->b_itemRename->setVisible(false);
}

AbstractListView::~AbstractListView()
{
  delete ui;
}

void
AbstractListView::setAddBtnMenu(QMenu* menu)
{
  if (!menu)
    return;

  ui->b_itemAdd->setMenu(menu);
}

void
AbstractListView::setAddBtnAction(QAction* action)
{
  if (!action)
    return;

  ui->b_itemAdd->setDefaultAction(action);
}

void
AbstractListView::loadData(const QJsonObject& json)
{
  // commented out on 17/05/2022
  /*
  const models::Layout layout;
  const models::Protocol protocol;

  QString key{ json.contains(layout.plates->getKey())
                 ? layout.plates->getKey()
                 : protocol.steps->getKey() };

  if (json.contains(layout.plates->getKey()))
    factory.reset(new FactoryPlate);
  else if (json.contains(protocol.steps->getKey()))
    factory.reset(new FactoryStep);
  else
    return;

  QJsonArray array = json.value(key).toArray();

for (const auto& item : qAsConst(array)) {
QJsonObject json{ item.toObject() };
QStandardItem* model_item{ factory->FactoryFromJson(json) };
m_model->appendRow(model_item);
}

ui->listView->setModel(m_model);
ui->listView->setCurrentIndex(QModelIndex{});
  */
}

void
AbstractListView::setModel(models::AbstractListModel* _model)
{
  connect(ui->listView,
          &QListView::clicked,
          _model,
          &models::AbstractListModel::onItemSelected);

  connect(
    ui->listView, &QListView::clicked, this, [this](const QModelIndex& idx) {
      qCDebug(ALW) << "QlistView::clicked" << idx;
      current_selection = idx;
    });

  //  connect(_model,
  //          &models::AbstractListModel::rowsInserted,
  //          this,
  //          &widgets::AbstractListView::on_listView_rowsInserted);

  ui->listView->setModel(_model);
}

int
AbstractListView::dataSize()
{
  QAbstractItemModel* model = ui->listView->model();
  return model->rowCount(QModelIndex{});
}

void
AbstractListView::clear()
{
  auto model = static_cast<models::AbstractListModel*>(ui->listView->model());
  model->clear();
}

bool
AbstractListView::addItem()
{
  qCDebug(ALW) << "AbstractListView::addItem"
               << "/no data =" << no_data;

  models::AbstractListModel* model =
    static_cast<models::AbstractListModel*>(ui->listView->model());

  model->createItem();
  ui->listView->setCurrentIndex(model->index(dataSize(), 0));
  return true;
}

void
AbstractListView::removeItem()
{
  qCDebug(ALW) << "AbstractListView::removeItem" << current_selection;

  if (!current_selection.isValid())
    return;

  emit item_deleted(current_selection);

  //! @warning какое-то неправильное обновление индекса после удаления
  current_selection =
    ui->listView->model()->index(ui->listView->model()->rowCount(), 0);
}

void
AbstractListView::moveItem(const int src, const int dst)
{}

void
AbstractListView::copyItem(const int idx)
{}

void
AbstractListView::cutItem(const int idx)
{}

void
AbstractListView::pasteItem(const int idx)
{}

void
AbstractListView::on_listView_rowsInserted(const QModelIndex& index,
                                           int first,
                                           int last)
{
  qCDebug(ALW) << "AbstractListView::on_listView_rowsInserted"
               << "/index =" << index << "/first =" << first
               << "/last =" << last;
  ui->listView->setCurrentIndex(ui->listView->model()->index(last, 0));
}

} // namespace widgets
