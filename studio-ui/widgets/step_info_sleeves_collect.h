#ifndef STEP_INFO_PICKUP_H
#define STEP_INFO_PICKUP_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoSleevesCollect;
}

namespace widgets {

class StepInfoSleevesCollect : public AbstractStepWidget
{
  Q_OBJECT

public:
  explicit StepInfoSleevesCollect(QWidget* parent = nullptr);
  ~StepInfoSleevesCollect() override;

private:
  Ui::StepInfoSleevesCollect* ui{ nullptr };

  // AbstractStepWidget interface
public:
  void loadStepToUi(models::steps::AbstractStep* _step) override;
};

} // namespace widg

#endif // STEP_INFO_PICKUP_H
