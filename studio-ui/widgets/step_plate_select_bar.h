#ifndef STEP_PLATE_SELECT_BAR_H
#define STEP_PLATE_SELECT_BAR_H

#include <QWidget>

namespace Ui {
class StepPlateSelectBar;
}

namespace widgets {

class StepPlateSelectBar : public QWidget
{
  Q_OBJECT

public:
  explicit StepPlateSelectBar(QWidget* parent = nullptr);
  ~StepPlateSelectBar();

public slots:
  void setPlate(const int plateIdx);

private:
  Ui::StepPlateSelectBar* ui{ nullptr };

signals:
  void plateChanged(const int value);
};

} // namespace widgets

#endif // STEP_PLATE_SELECT_BAR_H
