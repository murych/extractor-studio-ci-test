#ifndef STEP_INFO_MIX_H
#define STEP_INFO_MIX_H

#include "abstract_step_widget.h"
#include <QPersistentModelIndex>

namespace Ui {
class StepInfoMix;
}

namespace models {
class MixPatternTableModel;

namespace steps {
class MixingStage;
}
}

namespace widgets {
class TimeDelegate;
class SpeedComboBoxDelegate;

class StepInfoMix : public AbstractStepWidget
{
  Q_OBJECT

public:
  explicit StepInfoMix(QWidget* parent = nullptr);
  ~StepInfoMix() override;

private:
  Ui::StepInfoMix* ui{ nullptr };
  //  models::MixPatternTableModel* model{ nullptr };
  QScopedPointer<models::MixPatternTableModel> model;
  widgets::TimeDelegate* time_delegate{ nullptr };
  widgets::SpeedComboBoxDelegate* speed_delegate{ nullptr };

  QPersistentModelIndex persistentIndex;

  // AbstractStepWidget interface
public:
  void loadStepToUi(models::steps::AbstractStep* _step) override;

  QList<models::steps::MixingStage*> collectMixingStages() const;

signals:
  void patternRepeatCountChanged(const int value);
  void enterLiquidSpeedChanged(const int value);
  void mixStartingPositionChanged(const int value);
  void mixAmplitudeChanged(const int value);
};

} // namespace widgets

#endif // STEP_INFO_MIX_H
