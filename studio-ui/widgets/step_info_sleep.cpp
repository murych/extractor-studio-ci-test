#include "step_info_sleep.h"
#include "ui_step_info_sleep.h"

#include <studio-lib/models/steps/step_sleep.h>

namespace widgets {

StepInfoSleep::StepInfoSleep(QWidget* parent)
  : AbstractStepWidget{ parent }
  , ui{ new Ui::StepInfoSleep }
{
  ui->setupUi(this);

  connect(ui->bar_plate_select,
          &widgets::StepPlateSelectBar::plateChanged,
          this,
          &AbstractStepWidget::relatedPlateChanged);

  connect(ui->sleepDurationTimeEdit,
          &QTimeEdit::timeChanged,
          this,
          [this](const QTime time) {
            QTime start{ 0, 0, 0 };
            emit sleepDurationChanged(time.secsTo(start));
          });
}

StepInfoSleep::~StepInfoSleep()
{
  delete ui;
}

void
StepInfoSleep::loadStepToUi(models::steps::AbstractStep* _step)
{
  setObjectName(_step->getStepName());
  ui->bar_plate_select->setPlate(_step->getRelatedPlate());

  models::steps::StepSleep* step{ static_cast<models::steps::StepSleep*>(
    _step) };

  QTime time{ 0, 0, 0 };
  time = time.addSecs(step->getSleepDuration());
  ui->sleepDurationTimeEdit->setTime(time);
}

} // namespace widgets
