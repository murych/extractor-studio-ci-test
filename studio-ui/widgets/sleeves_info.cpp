#include "sleeves_info.h"
#include "ui_sleeves_info.h"

namespace widgets {

SleevesInfo::SleevesInfo(QWidget* parent)
  : QWidget{ parent }
  , ui{ new Ui::SleevesInfo }
{
  ui->setupUi(this);
}

SleevesInfo::~SleevesInfo()
{
  delete ui;
}

} // namespace widgets
