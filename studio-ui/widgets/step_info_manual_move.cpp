#include "step_info_manual_move.h"
#include "ui_step_info_manual_move.h"

#include <studio-lib/models/steps/step_manual_move.h>

namespace widgets {

StepInfoManualMove::StepInfoManualMove(QWidget* parent)
  : AbstractStepWidget{ parent }
  , ui{ new Ui::StepInfoManualMove }
{
  ui->setupUi(this);
}

StepInfoManualMove::~StepInfoManualMove()
{
  delete ui;
  //  delete step;
}

void
StepInfoManualMove::loadStepToUi(models::steps::AbstractStep* _step)
{
  models::steps::StepManualMove* step =
    static_cast<models::steps::StepManualMove*>(_step);
  ui->bar_plate_select->setPlate(step->plate->value());
}

/*
StepInfoManualMove::StepInfoManualMove(const QJsonObject& json, QWidget* parent)
  : StepInfoManualMove{ parent }
{
  step = new models::StepManualMove{ this, json };
}

void
StepInfoManualMove::setStep(models::StepManualMove* newStep)
{
  if (step)
    delete step;
  step = nullptr;

  step = newStep;
  loadStepToUi();
}


models::AbstractStep*
StepInfoManualMove::getStep()
{
  if (step)
    return static_cast<models::AbstractStep*>(step);
  else
    return nullptr;
}
*/

} // namespace widgets
