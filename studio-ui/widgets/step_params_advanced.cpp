#include "step_params_advanced.h"
#include "ui_step_params_advanced.h"

#include <studio-lib/models/steps/abstract_step.h>

namespace widgets {

StepParamsAdvanced::StepParamsAdvanced(QWidget* parent)
  : QWidget{ parent }
  , ui{ new Ui::StepParamsAdvanced }
{
  ui->setupUi(this);

  connect(ui->sleevesPositionAtEndLineEdit,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &StepParamsAdvanced::endPositionChanged);
  connect(ui->verticalTravelSpeedComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &StepParamsAdvanced::verticalSpeedChanged);
  connect(ui->horizontalExitSpeedComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &StepParamsAdvanced::horizontalSpeedChanged);
}

StepParamsAdvanced::~StepParamsAdvanced()
{
  delete ui;
}

void
StepParamsAdvanced::loadStepToUi(const models::steps::AbstractStep* step)
{
  qDebug() << "\t\tStepParamsAdvanced::loadStepToUi" << step->getStepName();
  ui->sleevesPositionAtEndLineEdit->setCurrentIndex(step->getSleevesPosAtEnd());
  ui->horizontalExitSpeedComboBox->setCurrentIndex(step->getHorizontalSpeed());
  ui->verticalTravelSpeedComboBox->setCurrentIndex(step->getVerticalSpeed());
}

} // namespace widgets
