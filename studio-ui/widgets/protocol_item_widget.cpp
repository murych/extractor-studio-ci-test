#include "protocol_item_widget.h"
#include "ui_protocol_item_widget.h"

/*
#include "models/step_list_item.h"
#include <QModelIndex>

#include "protocol_item_widget.h"

#include "widgets/step_info_beads_collect.h"
#include "widgets/step_info_beads_release.h"
#include "widgets/step_info_dry.h"
#include "widgets/step_info_heater.h"
#include "widgets/step_info_leave.h"
#include "widgets/step_info_manual_move.h"
#include "widgets/step_info_mix.h"
#include "widgets/step_info_pause.h"
#include "widgets/step_info_pickup.h"
#include "widgets/step_info_sleep.h"
*/

/*
namespace widgets {

class ProtocolItemWidget::FactoryWidget
{
public:
  FactoryWidget() = default;
  virtual ~FactoryWidget() = default;
  // сделать для виджетов конструкторы с использованием сущностей, а не json'ов
  virtual widgets::AbstractStepWidget* FactoryEmpty() const = 0;
  virtual widgets::AbstractStepWidget* FactoryMethod(
    models::AbstractStep*) const = 0;
};

class ProtocolItemWidget::FactoryWidgetMix : public FactoryWidget
{
public:
  FactoryWidgetMix() = default;
  ~FactoryWidgetMix() override = default;
  widgets::AbstractStepWidget* FactoryEmpty() const override
  {
    return new widgets::StepInfoMix;
  }

  widgets::AbstractStepWidget* FactoryMethod(
    models::AbstractStep* _step) const override
  {
    widgets::StepInfoMix* newStep = new widgets::StepInfoMix;
    newStep->setStep(static_cast<models::StepMix*>(_step));
    return newStep;
  }
};

class ProtocolItemWidget::FactoryWidgetDry : public FactoryWidget
{
public:
  FactoryWidgetDry() = default;
  ~FactoryWidgetDry() override = default;
  widgets::AbstractStepWidget* FactoryEmpty() const override
  {
    return new widgets::StepInfoDry;
  }

  widgets::AbstractStepWidget* FactoryMethod(
    models::AbstractStep* _step) const override
  {
    widgets::StepInfoDry* newStep = new widgets::StepInfoDry;
    newStep->setStep(static_cast<models::StepDry*>(_step));
    return newStep;
  }
};

class ProtocolItemWidget::FactoryWidgetBeadsCollect : public FactoryWidget
{
public:
  FactoryWidgetBeadsCollect() = default;
  ~FactoryWidgetBeadsCollect() override = default;

  widgets::AbstractStepWidget* FactoryEmpty() const override
  {
    return new widgets::StepInfoBeadsCollect;
  }

  widgets::AbstractStepWidget* FactoryMethod(
    models::AbstractStep* _step) const override
  {
    widgets::StepInfoBeadsCollect* newStep = new widgets::StepInfoBeadsCollect;
    newStep->setStep(static_cast<models::StepBeadsCollect*>(_step));
    return newStep;
  }
};

class ProtocolItemWidget::FactoryWidgetBeadsRelease : public FactoryWidget
{
public:
  FactoryWidgetBeadsRelease() = default;
  ~FactoryWidgetBeadsRelease() override = default;

  widgets::AbstractStepWidget* FactoryEmpty() const override
  {
    return new widgets::StepInfoBeadsRelease;
  }

  widgets::AbstractStepWidget* FactoryMethod(
    models::AbstractStep* _step) const override
  {
    widgets::StepInfoBeadsRelease* newStep = new widgets::StepInfoBeadsRelease;
    newStep->setStep(static_cast<models::StepBeadsRelease*>(_step));
    return newStep;
  }
};

class ProtocolItemWidget::FactoryWidgetSleeveCollect : public FactoryWidget
{
public:
  FactoryWidgetSleeveCollect() = default;
  ~FactoryWidgetSleeveCollect() override = default;

  widgets::AbstractStepWidget* FactoryEmpty() const override
  {
    return new widgets::StepInfoSleevesPickup;
  }

  widgets::AbstractStepWidget* FactoryMethod(
    models::AbstractStep* _step) const override
  {
    qDebug() << "widgets::AbstractStepWidget* FactoryMethod";
    widgets::StepInfoSleevesPickup* newStep =
      new widgets::StepInfoSleevesPickup;
    newStep->setStep(static_cast<models::StepSleevesPickup*>(_step));
    return newStep;
  }
};

class ProtocolItemWidget::FactoryWidgetSleeveRelease : public FactoryWidget
{
public:
  FactoryWidgetSleeveRelease() = default;
  ~FactoryWidgetSleeveRelease() override = default;

  widgets::AbstractStepWidget* FactoryEmpty() const override
  {
    return new widgets::StepInfoSleevesRelease;
  }

  widgets::AbstractStepWidget* FactoryMethod(
    models::AbstractStep* _step) const override
  {
    widgets::StepInfoSleevesRelease* newStep =
      new widgets::StepInfoSleevesRelease;
    newStep->setStep(static_cast<models::StepSleevesLeave*>(_step));
    return newStep;
  }
};

class ProtocolItemWidget::FactoryWidgetHeater : public FactoryWidget
{
public:
  FactoryWidgetHeater() = default;
  ~FactoryWidgetHeater() override = default;

  widgets::AbstractStepWidget* FactoryEmpty() const override
  {
    return new widgets::StepInfoHeater;
  }

  widgets::AbstractStepWidget* FactoryMethod(
    models::AbstractStep* _step) const override
  {
    widgets::StepInfoHeater* newStep = new widgets::StepInfoHeater;
    newStep->setStep(static_cast<models::StepHeater*>(_step));
    return newStep;
  }
};

class ProtocolItemWidget::FactoryWidgetManualMove : public FactoryWidget
{
public:
  FactoryWidgetManualMove() = default;
  ~FactoryWidgetManualMove() override = default;

  widgets::AbstractStepWidget* FactoryEmpty() const override
  {
    return new widgets::StepInfoManualMove;
  }

  widgets::AbstractStepWidget* FactoryMethod(
    models::AbstractStep* _step) const override
  {
    widgets::StepInfoManualMove* newStep = new widgets::StepInfoManualMove;
    newStep->setStep(static_cast<models::StepManualMove*>(_step));
    return newStep;
  }
};

class ProtocolItemWidget::FactoryWidgetPause : public FactoryWidget
{
public:
  FactoryWidgetPause() = default;
  ~FactoryWidgetPause() override = default;

  widgets::AbstractStepWidget* FactoryEmpty() const override
  {
    return new widgets::StepInfoPause;
  }

  widgets::AbstractStepWidget* FactoryMethod(
    models::AbstractStep* _step) const override
  {
    widgets::StepInfoPause* newStep = new widgets::StepInfoPause;
    newStep->setStep(static_cast<models::StepPause*>(_step));
    return newStep;
  }
};

class ProtocolItemWidget::FactoryWidgetSleep : public FactoryWidget
{
public:
  FactoryWidgetSleep() = default;
  ~FactoryWidgetSleep() override = default;

  widgets::AbstractStepWidget* FactoryEmpty() const override
  {
    return new widgets::StepInfoSleep;
  }

  widgets::AbstractStepWidget* FactoryMethod(
    models::AbstractStep* _step) const override
  {
    widgets::StepInfoSleep* newStep = new widgets::StepInfoSleep;
    newStep->setStep(static_cast<models::StepSleep*>(_step));
    return newStep;
  }
};

} // namespace widgets
*/

namespace widgets {

ProtocolItemWidget::ProtocolItemWidget(QWidget* parent)
  : QStackedWidget{ parent }
  , ui{ new Ui::ProtocolItemWidget }
{
  ui->setupUi(this);
}

ProtocolItemWidget::~ProtocolItemWidget()
{
  delete ui;
}

/*
void
ProtocolItemWidget::loadData(const QJsonObject& json)
{
  // clear existing widgets
  for (int idx = this->count(); idx >= 0; --idx) {
    QWidget* widget = this->widget(idx);
    this->removeWidget(widget);
    widget->deleteLater();
  }

  qDeleteAll(protocol_steps_widgets);
  protocol_steps_widgets.clear();

  if (!json.contains("steps"))
    return;

  QJsonArray steps = json.value("steps").toArray();

  for (int idx = 0; idx < steps.count(); ++idx) {
    const QJsonObject json{ steps.at(idx).toObject() };
    const int step_type{ json.value("step_type").toInt(StepType::unknown) };

    switch (static_cast<StepType>(step_type)) {
      case StepType::heater: {
        StepInfoHeater* newStep{ new StepInfoHeater{ json, this } };
        this->addWidget(newStep);
        protocol_steps_widgets << newStep;
        break;
      }

      case StepType::block: {
        StepInfoPause* newStep{ new StepInfoPause{ json, this } };
        this->addWidget(newStep);
        protocol_steps_widgets << newStep;
        break;
      }

      case StepType::pickup: {
        StepInfoSleevesPickup* newStep{ new StepInfoSleevesPickup{ json,
                                                                   this } };
        this->addWidget(newStep);
        protocol_steps_widgets << newStep;
        break;
      }

      case StepType::leave: {
        StepInfoSleevesRelease* newStep{ new StepInfoSleevesRelease{ json,
                                                                     this } };
        this->addWidget(newStep);
        protocol_steps_widgets << newStep;
        break;
      }

      case StepType::mix: {
        StepInfoMix* newStep{ new StepInfoMix{ json, this } };
        addWidget(newStep);
        protocol_steps_widgets << newStep;
        break;
      }

      case StepType::dry: {
        StepInfoDry* newStep{ new StepInfoDry{ json, this } };
        this->addWidget(newStep);
        protocol_steps_widgets << newStep;
        break;
      }

      case StepType::pause: {
        StepInfoSleep* newStep{ new StepInfoSleep{ json, this } };
        this->addWidget(newStep);
        protocol_steps_widgets << newStep;
        break;
      }

      case StepType::move: {
        StepInfoManualMove* newStep{ new StepInfoManualMove{ json, this } };
        this->addWidget(newStep);
        protocol_steps_widgets << newStep;
        break;
      }

      case StepType::collect: {
        widgets::StepInfoBeadsCollect* newStep{
          new widgets::StepInfoBeadsCollect{ json, this }
        };
        this->addWidget(newStep);
        protocol_steps_widgets << newStep;
        break;
      }

      case StepType::release: {
        widgets::StepInfoBeadsRelease* newStep{
          new widgets::StepInfoBeadsRelease{ json, this }
        };
        this->addWidget(newStep);
        protocol_steps_widgets << newStep;
        break;
      }

      default: {
        AbstractStepWidget* newStep{ new AbstractStepWidget{ this } };
        addWidget(newStep);
        protocol_steps_widgets << newStep;
        break;
      }
    }
  }
}
*/

/*
void
ProtocolItemWidget::selectStep(const QModelIndex& idx)
{
  if (idx.row() < 0 || idx.row() > this->count())
    return;

  this->setCurrentIndex(idx.row());
}

void
ProtocolItemWidget::addStep(models::AbstractStep* step)
{
  // здесь сделать фабрики виджетов шагов
  // виджеты должно быть можно создавать без шага
  // в виджеты надо добавить два метода: установка шага и (чисто виртуальный)
  // заполнение полей из шага для каждого типа виджета - свое заполнение полей
  // ??????????
  // profit!

  qDebug() << "ProtocolItemWidget::addStep"
           << QVariant::fromValue(
                static_cast<StepType>(step->stepType->value()))
                .toString();

  switch (static_cast<StepType>(step->stepType->value())) {
    case StepType::heater:
      factory_step.reset(new FactoryWidgetHeater);
    case StepType::block:
      factory_step.reset(new FactoryWidgetPause);
    case StepType::pickup:
      factory_step.reset(new FactoryWidgetSleeveCollect);
    case StepType::leave:
      factory_step.reset(new FactoryWidgetSleeveRelease);
    case StepType::mix:
      factory_step.reset(new FactoryWidgetMix);
    case StepType::dry:
      factory_step.reset(new FactoryWidgetDry);
    case StepType::pause:
      factory_step.reset(new FactoryWidgetPause);
    case StepType::move:
      factory_step.reset(new FactoryWidgetManualMove);
    case StepType::collect:
      factory_step.reset(new FactoryWidgetBeadsCollect);
    case StepType::release:
      factory_step.reset(new FactoryWidgetBeadsRelease);
    default:
      break;
  }

  addWidget(factory_step->FactoryMethod(step));
}

QList<models::AbstractStep*>
ProtocolItemWidget::getSteps()
{
  QList<models::AbstractStep*> result{};

  for (auto& widget : protocol_steps_widgets)
    result.append(widget->getStep());

  return result;
}
*/

}
