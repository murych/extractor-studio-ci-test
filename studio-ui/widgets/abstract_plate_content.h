#ifndef ABSTRACTPLATECONTENT_H
#define ABSTRACTPLATECONTENT_H

#include <QGroupBox>

namespace widgets {

class AbstractPlateContent : public QGroupBox
{
  Q_OBJECT
public:
  explicit AbstractPlateContent(QWidget* parent = nullptr);
  virtual ~AbstractPlateContent();
};

} // namespace widgets

#endif // ABSTRACTPLATECONTENT_H
