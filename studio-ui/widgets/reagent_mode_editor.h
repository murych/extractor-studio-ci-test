#ifndef WIDGETS_REAGENTMODEEDITOR_H
#define WIDGETS_REAGENTMODEEDITOR_H

#include <QComboBox>

namespace widgets {

class ReagentTypeEditor : public QComboBox
{
  Q_OBJECT
public:
  ReagentTypeEditor(QWidget* parent = nullptr);
  ~ReagentTypeEditor() override;

private:
  void populateList();
};

} // namespace widgets

#endif // WIDGETS_REAGENTMODEEDITOR_H
