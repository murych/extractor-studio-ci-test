#include "step_info_mix.h"
#include "ui_step_info_mix.h"

#include "delegates/delegate_combobox_speed.h"
#include "delegates/delegate_time.h"
#include "models/mix_pattern_tablemodel.h"
#include <QItemEditorCreatorBase>
#include <QItemEditorFactory>
#include <studio-lib/models/steps/step_mix.h>

namespace widgets {

StepInfoMix::StepInfoMix(QWidget* parent)
  : AbstractStepWidget{ parent }
  , ui{ new Ui::StepInfoMix }
  , time_delegate{ new widgets::TimeDelegate{ this } }
  , speed_delegate{ new widgets::SpeedComboBoxDelegate{ this } }
{
  model.reset(new models::MixPatternTableModel{ this });

  ui->setupUi(this);
  ui->b_moveDownMix->setVisible(false);
  ui->b_moveUpMix->setVisible(false);

  ui->tableView->setModel(model.data());
  ui->tableView->horizontalHeader()->setSectionResizeMode(
    QHeaderView::ResizeToContents);
  ui->tableView->setItemDelegateForColumn(models::MixPatternTableModel::time,
                                          time_delegate);
  ui->tableView->setItemDelegateForColumn(models::MixPatternTableModel::speed,
                                          speed_delegate);

  persistentIndex = QPersistentModelIndex{ model->index(0, 0) };

  connect(
    ui->tableView, &QTableView::clicked, this, [this](const QModelIndex index) {
      persistentIndex = QPersistentModelIndex{ index };
    });

  connect(ui->b_addMix, &QToolButton::clicked, this, [this] {
    model->insertRow(model->items.count());
  });
  connect(ui->b_deleteMix, &QToolButton::clicked, this, [this] {
    if (!persistentIndex.isValid())
      return;
    model->removeRow(persistentIndex.row());
  });

  connect(ui->bar_plate_select,
          &widgets::StepPlateSelectBar::plateChanged,
          this,
          &AbstractStepWidget::relatedPlateChanged);
  connect(ui->mixParamAdvanced,
          &widgets::StepParamsAdvanced::endPositionChanged,
          this,
          &AbstractStepWidget::endPositionChanged);
  connect(ui->mixParamAdvanced,
          &widgets::StepParamsAdvanced::verticalSpeedChanged,
          this,
          &AbstractStepWidget::verticalSpeedChanged);
  connect(ui->mixParamAdvanced,
          &widgets::StepParamsAdvanced::horizontalSpeedChanged,
          this,
          &AbstractStepWidget::horizontalSpeedChanged);

  connect(ui->patternRepeatCountSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          this,
          &widgets::StepInfoMix::patternRepeatCountChanged);
  connect(ui->enteringLiquidSpeedComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &widgets::StepInfoMix::enterLiquidSpeedChanged);
  connect(ui->mixingStartPositionComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &widgets::StepInfoMix::mixStartingPositionChanged);
  connect(ui->mixingAmplitudeSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          this,
          &widgets::StepInfoMix::mixAmplitudeChanged);
}

StepInfoMix::~StepInfoMix()
{
  delete ui;
  delete time_delegate;
}

void
StepInfoMix::loadStepToUi(models::steps::AbstractStep* _step)
{
  qDebug() << "\t\tStepInfoMix::loadStepToUi";

  qDebug() << "\t\t---------------"
           << "loading base params";

  setObjectName(_step->getStepName());
  ui->bar_plate_select->setPlate(_step->plate->value());
  ui->mixParamAdvanced->loadStepToUi(_step);

  qDebug() << "\t\t---------------"
           << "loading specific mix params";
  models::steps::StepMix* step{ static_cast<models::steps::StepMix*>(_step) };
  qDebug() << step;

  ui->mixingAmplitudeSpinBox->setValue(step->getMixAmplitude());
  ui->patternRepeatCountSpinBox->setValue(step->getPatternRepeat());
  ui->enteringLiquidSpeedComboBox->setCurrentIndex(step->getEnterSpeed());
  ui->mixingStartPositionComboBox->setCurrentIndex(step->getStartPosition());

  qDebug() << "\t\t\t"
           << "loading mixing statges";
  qDeleteAll(model->items);
  model->items.clear();

  for (auto& stage : step->getMixStages())
    model->items.append(stage);

  ui->tableView->setModel(model.data());
  ui->tableView->horizontalHeader()->stretchLastSection();

  // код ниже работает, но как-то стремно использовать толком не разобравшись
  // а я не разобрался :С
  //  QItemEditorFactory* factory{ new QItemEditorFactory };
  //  QItemEditorCreatorBase* speedSelectCreator{
  //    new QStandardItemEditorCreator<SpeedModeEditor>()
  //  };
  //  factory->registerEditor(QVariant::Int, speedSelectCreator);
  //  QItemEditorFactory::setDefaultFactory(factory);
}

QList<models::steps::MixingStage*>
StepInfoMix::collectMixingStages() const
{
  return model->items;
}

} // namespace widgets
