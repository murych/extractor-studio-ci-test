#include "reagent_mode_editor.h"

#include <studio-lib/models/plates/reagent.h>

using namespace models;

namespace widgets {

ReagentTypeEditor::ReagentTypeEditor(QWidget* parent)
  : QComboBox{ parent }
{
  populateList();
  setCurrentIndex(0);
}

ReagentTypeEditor::~ReagentTypeEditor() {}

void
ReagentTypeEditor::populateList()
{
  const QStringList modes{
    plates::Reagent::reagentTypeMapper.value(plates::Reagent::reagent),
    plates::Reagent::reagentTypeMapper.value(plates::Reagent::sample)
  };
  addItems(modes);
}

} // namespace widgets
