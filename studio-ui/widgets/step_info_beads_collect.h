#ifndef STEP_INFO_BEADS_COLLECT_H
#define STEP_INFO_BEADS_COLLECT_H

#include "abstract_step_widget.h"

#include <studio-lib/models/steps/step_beads_collect.h>

namespace Ui {
class StepInfoBeadsCollect;
}

namespace widgets {

class StepInfoBeadsCollect : public AbstractStepWidget
{
  Q_OBJECT

public:
  explicit StepInfoBeadsCollect(QWidget* parent = nullptr);
  //  StepInfoBeadsCollect(const QJsonObject& json, QWidget* parent = nullptr);
  ~StepInfoBeadsCollect() override;

private:
  Ui::StepInfoBeadsCollect* ui{ nullptr };

  // AbstractStepWidget interface
public:
  void loadStepToUi(models::steps::AbstractStep* _step) override;

signals:
  void collectSpeedChanged(const int value);
  void collectCountChanged(const int value);
  void collectDurationChanged(const int value);
};

} // namespace widgets

#endif // STEP_INFO_BEADS_COLLECT_H
