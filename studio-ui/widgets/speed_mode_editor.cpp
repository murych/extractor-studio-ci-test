#include "speed_mode_editor.h"

#include <studio-lib/models/steps/moving_speed.h>

using namespace models;

namespace widgets {

SpeedModeEditor::SpeedModeEditor(QWidget* parent)
  : QComboBox{ parent }
{
  populateList();
  setCurrentIndex(0);
}

SpeedModeEditor::~SpeedModeEditor() {}

void
SpeedModeEditor::populateList()
{
  addItem(QIcon{ steps::speedIconMapper.value(steps::slow) },
          steps::travelSpeedMapper.value(steps::slow));
  addItem(QIcon{ steps::speedIconMapper.value(steps::medium) },
          steps::travelSpeedMapper.value(steps::medium));
  addItem(QIcon{ steps::speedIconMapper.value(steps::fast) },
          steps::travelSpeedMapper.value(steps::fast));
}

} // namespace widgets
