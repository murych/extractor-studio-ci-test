#include "step_info_sleeves_release.h"
#include "ui_step_info_sleeves_release.h"

#include <studio-lib/models/steps/step_sleeves_leave.h>

namespace widgets {

StepInfoSleevesRelease::StepInfoSleevesRelease(QWidget* parent)
  : AbstractStepWidget{ parent }
  , ui{ new Ui::StepInfoSleevesRelease }
{
  ui->setupUi(this);

  connect(ui->bar_plate_select,
          &widgets::StepPlateSelectBar::plateChanged,
          this,
          &widgets::AbstractStepWidget::relatedPlateChanged);
}

StepInfoSleevesRelease::~StepInfoSleevesRelease()
{
  delete ui;
}

void
StepInfoSleevesRelease::loadStepToUi(models::steps::AbstractStep* _step)
{
  setObjectName("Step" + Qt::Key_Space + _step->getStepName());
  ui->bar_plate_select->setPlate(_step->getRelatedPlate());
}

} // namespace widgets
