#include "step_info_sleeves_collect.h"
#include "ui_step_info_sleeves_collect.h"

#include <studio-lib/models/steps/step_sleeves_pickup.h>

namespace widgets {

StepInfoSleevesCollect::StepInfoSleevesCollect(QWidget* parent)
  : AbstractStepWidget{ parent }
  , ui{ new Ui::StepInfoSleevesCollect }
{
  ui->setupUi(this);

  connect(ui->bar_plate_select,
          &widgets::StepPlateSelectBar::plateChanged,
          this,
          &widgets::AbstractStepWidget::relatedPlateChanged);
}

StepInfoSleevesCollect::~StepInfoSleevesCollect()
{
  delete ui;
}

void
StepInfoSleevesCollect::loadStepToUi(models::steps::AbstractStep* _step)
{
  setObjectName("Step" + Qt::Key_Space + _step->getStepName());
  ui->bar_plate_select->setPlate(_step->getRelatedPlate());
}

} // namespace widgets
