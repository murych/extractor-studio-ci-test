#include "plate_info.h"
#include "ui_plate_info.h"

#include "logger.h"
#include <QLoggingCategory>
#include <QMenu>

Q_LOGGING_CATEGORY(W_PL_INFO, "PLATE INFO")

namespace widgets {

PlateInfo::PlateInfo(QWidget* parent)
  : QWidget{ parent }
  , ui{ new Ui::PlateInfo }
{
  ui->setupUi(this);

  connect(ui->plateNameLineEdit, &QLineEdit::editingFinished, this, [this] {
    emit plate_renamed(ui->plateNameLineEdit->text());
  });

  connect(ui->platePositionComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &PlateInfo::plate_idx_changed);

  connect(ui->plateTypeComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &PlateInfo::plate_size_changed);

  connect(ui->plateContentComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &PlateInfo::plate_type_changed);

  //  auto reagent_info =
  //    static_cast<widgets::ReagentInfo*>(ui->stackedWidget->widget(liquids));
  //  connect(reagent_info, &ReagentInfo::reagentAdded, this, [this] {});
}

PlateInfo::~PlateInfo()
{
  delete ui;
}

void
PlateInfo::loadPlateToUi(const models::plates::Plate* plate)
{
  setObjectName("Widget " + plate->name->value());

  ui->platePositionComboBox->setCurrentIndex(plate->idx->value());
  ui->plateNameLineEdit->setText(plate->name->value());
  ui->plateTypeComboBox->setCurrentIndex(plate->size->value());
  ui->plateContentComboBox->setCurrentIndex(plate->type->value());

  widgets::ReagentInfo* reagent_info =
    static_cast<widgets::ReagentInfo*>(ui->stackedWidget->widget(liquids));
  reagent_info->loadData(plate->reagents->derivedEntities());
}

void
PlateInfo::loadUiToPlate()
{}

QList<models::plates::Reagent*>
PlateInfo::collectReagents()
{
  const int plateContent{ ui->plateContentComboBox->currentIndex() };
  if (plateContent == models::plates::Plate::ePlateType::sleeves)
    return {};

  const auto reagent_info =
    static_cast<widgets::ReagentInfo*>(ui->stackedWidget->widget(liquids));
  return reagent_info->getData();
}

} // namespace widgets
