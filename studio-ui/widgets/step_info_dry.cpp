#include "step_info_dry.h"
#include "ui_step_info_dry.h"

#include <studio-lib/models/steps/step_dry.h>

namespace widgets {

StepInfoDry::StepInfoDry(QWidget* parent)
  : AbstractStepWidget{ parent }
  , ui{ new Ui::StepInfoDry }
{
  ui->setupUi(this);

  connect(ui->bar_plate_select,
          &widgets::StepPlateSelectBar::plateChanged,
          this,
          &widgets::AbstractStepWidget::relatedPlateChanged);
  connect(ui->dryParamAdvanced,
          &widgets::StepParamsAdvanced::endPositionChanged,
          this,
          &widgets::AbstractStepWidget::endPositionChanged);
  connect(ui->dryParamAdvanced,
          &widgets::StepParamsAdvanced::verticalSpeedChanged,
          this,
          &widgets::AbstractStepWidget::verticalSpeedChanged);
  connect(ui->dryParamAdvanced,
          &widgets::StepParamsAdvanced::horizontalSpeedChanged,
          this,
          &widgets::AbstractStepWidget::horizontalSpeedChanged);

  connect(ui->dryDurationTimeEdit,
          &QTimeEdit::timeChanged,
          this,
          [this](const QTime time) { emit dryDurationChanged(time.second()); });
  connect(ui->dryPositionComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &widgets::StepInfoDry::dryPositionChanged);
}

StepInfoDry::~StepInfoDry()
{
  delete ui;
}

void
StepInfoDry::loadStepToUi(models::steps::AbstractStep* _step)
{
  setObjectName("Step" + Qt::Key_Space + _step->getStepName());
  ui->bar_plate_select->setPlate(_step->getRelatedPlate());
  ui->dryParamAdvanced->loadStepToUi(_step);

  models::steps::StepDry* step = static_cast<models::steps::StepDry*>(_step);

  QTime t{ 0, 0, 0 };
  t = t.addSecs(step->getDuration());
  ui->dryDurationTimeEdit->setTime(t);

  ui->dryPositionComboBox->setCurrentIndex(step->getPosition());
}

} // namespace widgets
