#include "reagent_info.h"
#include "ui_reagent_info.h"

#include "delegates/delegate_color.h"
#include "delegates/delegate_combobox_reagent_type.h"
#include "delegates/delegate_spinbox.h"
#include "models/reagent_table_model.h"
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <studio-lib/models/plates/reagent.h>

namespace widgets {

ReagentInfo::ReagentInfo(QWidget* parent)
  : QWidget{ parent }
  , ui{ new Ui::ReagentInfo }
  , model{ new models::ReagentTableModel }
  , spinbox_delegate{ new widgets::SpinboxDelegate }
  , combobox_delegate{ new widgets::ReagentTypeDelegate }
  , color_delegate{ new widgets::ColorDelegate }
{
  ui->setupUi(this);

  //! @todo включить назад после реализации всех действий в модели
  ui->b_copyRow->setVisible(false);
  ui->b_cutRow->setVisible(false);
  ui->b_pasteRow->setVisible(false);
  ui->b_moveDownRow->setVisible(false);
  ui->b_moveUpRow->setVisible(false);

  ui->tableView->verticalHeader()->setDefaultSectionSize(30);
  ui->tableView->setModel(model);

  ui->tableView->setItemDelegateForColumn(models::ReagentTableModel::volume,
                                          spinbox_delegate);
  ui->tableView->setItemDelegateForColumn(models::ReagentTableModel::type,
                                          combobox_delegate);
  ui->tableView->setItemDelegateForColumn(models::ReagentTableModel::color,
                                          color_delegate);
  ui->tableView->horizontalHeader()->setSectionResizeMode(
    QHeaderView::ResizeToContents);

  m_persistentIndex = QPersistentModelIndex{ model->index(0, 0) };

  connect(ui->b_calculateVolume,
          &QToolButton::clicked,
          this,
          &ReagentInfo::calculateTotalVolume);

  connect(ui->b_addRow, &QToolButton::clicked, this, [this] {
    model->insertRow(model->items.count());
    emit reagentAdded(m_persistentIndex.row());
  });

  connect(ui->b_deleteRow, &QToolButton::clicked, this, [this] {
    if (!m_persistentIndex.isValid())
      return;
    model->removeRow(m_persistentIndex.row());
    emit reagentDeleted(m_persistentIndex.row());
  });

  connect(
    ui->tableView, &QTableView::clicked, this, [this](const QModelIndex index) {
      m_persistentIndex = QPersistentModelIndex{ index };
    });
}

ReagentInfo::~ReagentInfo()
{
  delete ui;

  delete model;
  delete spinbox_delegate;
  delete color_delegate;
  delete combobox_delegate;
}

void
ReagentInfo::loadData(const QList<models::plates::Reagent*>& reagents)
{
  model->loadData(reagents);
  ui->tableView->setModel(model);

  m_persistentIndex = QPersistentModelIndex{ model->index(0, 0) };

  ui->tableView->setCurrentIndex(m_persistentIndex);
}

QList<models::plates::Reagent*>
ReagentInfo::getData() const
{
  return model->items;
}

void
ReagentInfo::calculateTotalVolume() const
{
  QList<models::plates::Reagent*> m_reagents = model->items;

  if (m_reagents.isEmpty())
    return;

  int total_volume = 0;
  for (const models::plates::Reagent* reagent : qAsConst(m_reagents))
    total_volume += reagent->reagent_volume->value();

  ui->sb_totalVolume->setValue(total_volume);
}

} // namespace widgets
