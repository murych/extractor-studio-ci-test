#ifndef STEP_BEADS_RELEASE_H
#define STEP_BEADS_RELEASE_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoBeadsRelease;
}

namespace widgets {

class StepInfoBeadsRelease : public AbstractStepWidget
{
  Q_OBJECT

public:
  explicit StepInfoBeadsRelease(QWidget* parent = nullptr);
  ~StepInfoBeadsRelease() override;

private:
  Ui::StepInfoBeadsRelease* ui{ nullptr };

  // AbstractStepWidget interface
public:
  void loadStepToUi(models::steps::AbstractStep* _step) override;

signals:
  void releaseSpeedChanged(const int value);
  void releaseCountChanged(const int value);
  void releaseDurationChanged(const int value);
};

} // namespace widgets

#endif // STEP_BEADS_RELEASE_H
