#ifndef STEP_INFO_MANUAL_MOVE_H
#define STEP_INFO_MANUAL_MOVE_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoManualMove;
}

namespace widgets {

class StepInfoManualMove : public AbstractStepWidget
{
  Q_OBJECT

public:
  explicit StepInfoManualMove(QWidget* parent = nullptr);
  //  StepInfoManualMove(const QJsonObject& json, QWidget* parent = nullptr);
  ~StepInfoManualMove() override;

private:
  Ui::StepInfoManualMove* ui{ nullptr };
  //  models::StepManualMove* step{ nullptr };

  // AbstractStepWidget interface
public:
  //  models::AbstractStep* getStep() override;
  //  void setStep(models::StepManualMove* newStep);

  void loadStepToUi(models::steps::AbstractStep* _step) override;
};

} // namespace widgets

#endif // STEP_INFO_MANUAL_MOVE_H
