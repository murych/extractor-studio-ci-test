#ifndef STEP_INFO_PAUSE_H
#define STEP_INFO_PAUSE_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoPause;
}

namespace widgets {

class StepInfoPause : public AbstractStepWidget
{
  Q_OBJECT

public:
  explicit StepInfoPause(QWidget* parent = nullptr);
  ~StepInfoPause() override;

private:
  Ui::StepInfoPause* ui{ nullptr };

  // AbstractStepWidget interface
public:
  void loadStepToUi(models::steps::AbstractStep* _step) override;

signals:
  void messageChanged(const QString& message);
};

} // namespace widgets

#endif // STEP_INFO_PAUSE_H
