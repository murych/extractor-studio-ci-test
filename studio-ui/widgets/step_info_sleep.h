#ifndef STEP_INFO_SLEEP_H
#define STEP_INFO_SLEEP_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoSleep;
}

namespace widgets {

class StepInfoSleep : public AbstractStepWidget
{
  Q_OBJECT

public:
  explicit StepInfoSleep(QWidget* parent = nullptr);
  ~StepInfoSleep() override;

private:
  Ui::StepInfoSleep* ui{ nullptr };

  // AbstractStepWidget interface
public:
  void loadStepToUi(models::steps::AbstractStep* _step) override;

signals:
  void sleepDurationChanged(const int value);
};

} // widgets

#endif // STEP_INFO_SLEEP_H
