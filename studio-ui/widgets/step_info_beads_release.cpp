#include "step_info_beads_release.h"
#include "ui_step_info_beads_release.h"

#include <studio-lib/models/steps/step_beads_release.h>

namespace widgets {

StepInfoBeadsRelease::StepInfoBeadsRelease(QWidget* parent)
  : AbstractStepWidget{ parent }
  , ui{ new Ui::StepInfoBeadsRelease }
{
  ui->setupUi(this);

  connect(ui->releaseParamAdvanced,
          &widgets::StepParamsAdvanced::endPositionChanged,
          this,
          &AbstractStepWidget::endPositionChanged);
  connect(ui->releaseParamAdvanced,
          &widgets::StepParamsAdvanced::verticalSpeedChanged,
          this,
          &AbstractStepWidget::verticalSpeedChanged);
  connect(ui->releaseParamAdvanced,
          &widgets::StepParamsAdvanced::horizontalSpeedChanged,
          this,
          &AbstractStepWidget::horizontalSpeedChanged);
  connect(ui->bar_plateSelect,
          &widgets::StepPlateSelectBar::plateChanged,
          this,
          &AbstractStepWidget::relatedPlateChanged);

  connect(ui->releaseSpeedComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &StepInfoBeadsRelease::releaseSpeedChanged);
  connect(ui->releaseCountSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          this,
          &StepInfoBeadsRelease::releaseCountChanged);
  connect(ui->releaseDurationSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          this,
          &StepInfoBeadsRelease::releaseDurationChanged);
}

StepInfoBeadsRelease::~StepInfoBeadsRelease()
{
  delete ui;
}

void
StepInfoBeadsRelease::loadStepToUi(models::steps::AbstractStep* _step)
{
  setObjectName("Step " + _step->name->value());

  ui->releaseParamAdvanced->loadStepToUi(_step);
  ui->bar_plateSelect->setPlate(_step->plate->value());

  auto step = static_cast<models::steps::StepBeadsRelease*>(_step);

  ui->releaseSpeedComboBox->setCurrentIndex(step->getReleaseSpeed());
  ui->releaseDurationSpinBox->setValue(step->getReleaseDuration());
  ui->releaseCountSpinBox->setValue(step->getReleaseCount());
}

} // namespace widgets
