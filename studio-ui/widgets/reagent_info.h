#ifndef REAGENT_INFO_H
#define REAGENT_INFO_H

#include "abstract_plate_content.h"
#include <QGroupBox>
#include <QPersistentModelIndex>

namespace Ui {
class ReagentInfo;
}

namespace models {
class ReagentTableModel;

namespace plates {
class Reagent;
}
}

namespace widgets {
class ColorDelegate;
class SpinboxDelegate;
class ReagentTypeDelegate;

class ReagentInfo : public QWidget
{
  Q_OBJECT

public:
  explicit ReagentInfo(QWidget* parent = nullptr);
  ~ReagentInfo() override;

  void loadData(const QList<models::plates::Reagent*>& reagents);
  QList<models::plates::Reagent*> getData() const;

private:
  Ui::ReagentInfo* ui;

  models::ReagentTableModel* model = nullptr;
  widgets::SpinboxDelegate* spinbox_delegate = nullptr;
  widgets::ColorDelegate* color_delegate = nullptr;
  widgets::ReagentTypeDelegate* combobox_delegate = nullptr;

  QPersistentModelIndex m_persistentIndex;

private slots:
  void calculateTotalVolume() const;

signals:
  void reagentAdded(const int idx);
  void reagentDeleted(const int idx);
  void reagentChanged(const int idx);
};

} // namespace widgets

#endif // REAGENT_INFO_H
