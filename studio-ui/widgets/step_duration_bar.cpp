#include "step_duration_bar.h"
#include "ui_step_duration_bar.h"

#include <QPalette>

namespace widgets {

StepDurationBar::StepDurationBar(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::StepDurationBar)
{
  ui->setupUi(this);

  QPalette palette = QPalette{};
  palette.setColor(QPalette::Window, QColor{ 233, 185, 110 });

  this->setAutoFillBackground(true);
  this->setPalette(palette);
}

StepDurationBar::~StepDurationBar()
{
  delete ui;
}

} // namespace widgets
