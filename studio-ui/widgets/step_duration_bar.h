#ifndef STEP_DURATION_BAR_H
#define STEP_DURATION_BAR_H

#include <QWidget>

namespace Ui {
class StepDurationBar;
}

namespace widgets {

class StepDurationBar : public QWidget
{
  Q_OBJECT

public:
  explicit StepDurationBar(QWidget* parent = nullptr);
  ~StepDurationBar();

private:
  Ui::StepDurationBar* ui;
};

} // namespace widgets

#endif // STEP_DURATION_BAR_H
