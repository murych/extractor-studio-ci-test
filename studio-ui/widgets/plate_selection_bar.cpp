#include "plate_selection_bar.h"
#include "ui_plate_selection_bar.h"

namespace widgets {

PlateSelectionBar::PlateSelectionBar(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::PlateSelectionBar)
{
  ui->setupUi(this);
}

PlateSelectionBar::~PlateSelectionBar()
{
  delete ui;
}

} // namespace widgets
