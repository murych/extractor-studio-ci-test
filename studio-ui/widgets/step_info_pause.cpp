#include "step_info_pause.h"
#include "ui_step_info_pause.h"

#include <studio-lib/models/steps/step_pause.h>

namespace widgets {

StepInfoPause::StepInfoPause(QWidget* parent)
  : AbstractStepWidget{ parent }
  , ui{ new Ui::StepInfoPause }
{
  ui->setupUi(this);

  connect(ui->tb_messageText, &QPlainTextEdit::textChanged, this, [this] {
    emit messageChanged(ui->tb_messageText->toPlainText());
  });
}

StepInfoPause::~StepInfoPause()
{
  delete ui;
}

void
StepInfoPause::loadStepToUi(models::steps::AbstractStep* _step)
{
  models::steps::StepPause* step =
    static_cast<models::steps::StepPause*>(_step);
  ui->bar_plate_select->setPlate(step->plate->value());
}

} // namespace widgets
