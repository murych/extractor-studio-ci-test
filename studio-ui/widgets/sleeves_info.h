#ifndef SLEEVES_INFO_H
#define SLEEVES_INFO_H

#include "abstract_plate_content.h"
#include <QGroupBox>

namespace Ui {
class SleevesInfo;
}

namespace widgets {

class SleevesInfo : public QWidget
{
  Q_OBJECT

public:
  explicit SleevesInfo(QWidget* parent = nullptr);
  ~SleevesInfo();

private:
  Ui::SleevesInfo* ui{ nullptr };
};

} // namespace widgets

#endif // SLEEVES_INFO_H
