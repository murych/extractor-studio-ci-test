#include "step_info_heater.h"
#include "ui_step_info_heater.h"

#include <studio-lib/models/steps/step_heater.h>

namespace widgets {

StepInfoHeater::StepInfoHeater(QWidget* parent)
  : AbstractStepWidget{ parent }
  , ui{ new Ui::StepInfoHeater }
{
  ui->setupUi(this);

  connect(ui->bar_plate_select,
          &widgets::StepPlateSelectBar::plateChanged,
          this,
          &AbstractStepWidget::relatedPlateChanged);

  connect(ui->pauseExecutionUntilHeatedCheckBox,
          &QCheckBox::toggled,
          this,
          &StepInfoHeater::blockExecutionChanged);
  connect(ui->targetTemperatureDoubleSpinBox,
          QOverload<double>::of(&QDoubleSpinBox::valueChanged),
          this,
          &StepInfoHeater::targetTemperatureChanged);
}

StepInfoHeater::~StepInfoHeater()
{
  delete ui;
}

void
StepInfoHeater::loadStepToUi(models::steps::AbstractStep* _step)
{
  setObjectName(_step->getStepName());
  ui->bar_plate_select->setPlate(_step->getRelatedPlate());

  models::steps::StepHeater* step =
    static_cast<models::steps::StepHeater*>(_step);
  ui->pauseExecutionUntilHeatedCheckBox->setChecked(step->getBlockExecution());
  ui->targetTemperatureDoubleSpinBox->setValue(step->getTargetTemp());
}

} // namespace widgets
