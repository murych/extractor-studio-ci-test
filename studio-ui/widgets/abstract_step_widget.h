#ifndef ABSTRACTSTEPWIDGET_H
#define ABSTRACTSTEPWIDGET_H

#include <QWidget>

namespace models {
namespace steps {
class AbstractStep;
}
}

namespace widgets {

class AbstractStepWidget : public QWidget
{
  Q_OBJECT
public:
  explicit AbstractStepWidget(QWidget* parent = nullptr)
    : QWidget{ parent }
  {}
  virtual ~AbstractStepWidget() {}

protected:
  virtual void loadStepToUi(models::steps::AbstractStep*) = 0;

signals:
  void endPositionChanged(const int value);
  void verticalSpeedChanged(const int value);
  void horizontalSpeedChanged(const int value);
  void relatedPlateChanged(const int value);
};

} // namespace widgets

#endif // ABSTRACTSTEPWIDGET_H
