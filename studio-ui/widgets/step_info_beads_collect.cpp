#include "step_info_beads_collect.h"
#include "ui_step_info_beads_collect.h"

namespace widgets {

StepInfoBeadsCollect::StepInfoBeadsCollect(QWidget* parent)
  : AbstractStepWidget{ parent }
  , ui{ new Ui::StepInfoBeadsCollect }
{
  ui->setupUi(this);

  connect(ui->collectSpeedComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &StepInfoBeadsCollect::collectSpeedChanged);
  connect(ui->collectCountSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          this,
          &StepInfoBeadsCollect::collectCountChanged);
  connect(ui->collectDurationSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          this,
          &StepInfoBeadsCollect::collectDurationChanged);
  connect(ui->collectParamAdvanced,
          &widgets::StepParamsAdvanced::endPositionChanged,
          this,
          &AbstractStepWidget::endPositionChanged);
  connect(ui->collectParamAdvanced,
          &widgets::StepParamsAdvanced::verticalSpeedChanged,
          this,
          &AbstractStepWidget::verticalSpeedChanged);
  connect(ui->collectParamAdvanced,
          &widgets::StepParamsAdvanced::horizontalSpeedChanged,
          this,
          &AbstractStepWidget::horizontalSpeedChanged);
  connect(ui->bar_plate_select,
          &widgets::StepPlateSelectBar::plateChanged,
          this,
          &AbstractStepWidget::relatedPlateChanged);
}

StepInfoBeadsCollect::~StepInfoBeadsCollect()
{
  delete ui;
}

void
StepInfoBeadsCollect::loadStepToUi(models::steps::AbstractStep* _step)
{
  ui->collectParamAdvanced->loadStepToUi(_step);

  models::steps::StepBeadsCollect* step =
    static_cast<models::steps::StepBeadsCollect*>(_step);

  ui->bar_plate_select->setPlate(step->plate->value());
  ui->collectSpeedComboBox->setCurrentIndex(step->getCollectSpeed());
  ui->collectCountSpinBox->setValue(step->getCollectCount());
  ui->collectDurationSpinBox->setValue(step->getCollectDuration());
}

} // namespace widgets
