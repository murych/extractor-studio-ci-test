#include "plate_item_widget.h"
#include "ui_plate_item_widget.h"

//#include "logger.h"
//#include "models/plate_list_item.h"
//#include "widgets/plate_info.h"
//#include <QLoggingCategory>
//#include <QModelIndex>
//#include <studio-lib/models/plate.h>

// Q_LOGGING_CATEGORY(PIW, "PALTE VIEW")

namespace widgets {

PlateItemWidget::PlateItemWidget(QWidget* parent)
  : QStackedWidget{ parent }
  , ui{ new Ui::PlateItemWidget }
{
  ui->setupUi(this);

  //  qDeleteAll(plate_widgets);
  //  plate_widgets.clear();
}

PlateItemWidget::~PlateItemWidget()
{
  delete ui;
}

// QList<models::Plate*>
// PlateItemWidget::getPlates()
//{
//  QList<models::Plate*> result{};

//  for (auto& widget : plate_widgets)
//    result.append(widget->getPlate());

//  return result;
//}

// void
// PlateItemWidget::loadData(const QJsonObject& json)
//{
//  for (int idx = this->count(); idx >= 0; --idx) {
//    QWidget* widget = this->widget(idx);
//    this->removeWidget(widget);
//    widget->deleteLater();
//  }

//  //  qDeleteAll(plate_widgets);
//  //  plate_widgets.clear();

//  if (!json.contains("plates"))
//    return;

//  QJsonArray plates = json.value("plates").toArray();

//  for (int idx = 0; idx < plates.count(); ++idx) {
//    const QJsonObject jsonPlate = plates.at(idx).toObject();
//    const models::Plate::ePlateSize plate_type =
//      static_cast<models::Plate::ePlateSize>(
//        jsonPlate.value("plate_size").toInt());

//    switch (plate_type) {
//      case models::Plate::ePlateSize::standard_96_plate: {
//        //        PlateInfo* newPlate = new PlateInfo(jsonPlate, this);
//        //        this->addWidget(newPlate);
//        //        plate_widgets << newPlate;
//        break;
//      }
//      default:
//        break;
//    }
//  }
//}

// void
// PlateItemWidget::selectPlate(const QModelIndex& idx)
//{
//  if (idx.row() < 0 || idx.row() > this->count())
//    return;

//  this->setCurrentIndex(idx.row());
//}

// void
// PlateItemWidget::addPlate(widgets::PlateInfo* plate)
//{
//  addWidget(plate);
//  plate_widgets.append(plate);
//  connect(
//    plate, &PlateInfo::plate_renamed, this, &PlateItemWidget::onItemRenamed);
//}

// void
// PlateItemWidget::renameItem(const QModelIndex& idx, const QString& name)
//{
//  if (idx.row() < 0 || idx.row() > this->count())
//    return;

//  //  widgets::PlateInfo* plate_info = plate_widgets.at(idx.row());
//  //  qCDebug(PIW) << idx << "old name"
//  //                   << plate_info->getPlate()->plate_name->value() << "new
//  //                   name"
//  //                   << name;

//  //  plate_info->getPlate()->plate_name->setValue(name);
//  //  plate_info->changePlateName(name);
//}

// void
// PlateItemWidget::onItemRenamed(const QString name)
//{}

} // namespace widge
