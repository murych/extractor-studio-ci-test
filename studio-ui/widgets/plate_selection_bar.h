#ifndef PLATE_SELECTION_BAR_H
#define PLATE_SELECTION_BAR_H

#include <QWidget>

namespace Ui {
class PlateSelectionBar;
}

namespace widgets {

class PlateSelectionBar : public QWidget
{
  Q_OBJECT

public:
  explicit PlateSelectionBar(QWidget* parent = nullptr);
  ~PlateSelectionBar();

private:
  Ui::PlateSelectionBar* ui;
};

} // namespace widgets

#endif // PLATE_SELECTION_BAR_H
