#ifndef COLORDELEGATE_H
#define COLORDELEGATE_H

#include <QStyledItemDelegate>

namespace widgets {

class ColorDelegate : public QStyledItemDelegate
{
  Q_OBJECT
public:
  explicit ColorDelegate(QObject* parent = nullptr);
  ~ColorDelegate();

  // QAbstractItemDelegate interface
public:
  QWidget* createEditor(QWidget* parent,
                        const QStyleOptionViewItem& option,
                        const QModelIndex& index) const override;
  void setEditorData(QWidget* editor, const QModelIndex& index) const override;
  void setModelData(QWidget* editor,
                    QAbstractItemModel* model,
                    const QModelIndex& index) const override;
  void paint(QPainter* painter,
             const QStyleOptionViewItem& option,
             const QModelIndex& index) const override;
  void updateEditorGeometry(QWidget* editor,
                            const QStyleOptionViewItem& option,
                            const QModelIndex& index) const override;
};

} // namespace widgets

#endif // COLORDELEGATE_H
