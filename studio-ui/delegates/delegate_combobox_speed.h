#ifndef COMBOBOXDELEGATE_H
#define COMBOBOXDELEGATE_H

#include <QItemDelegate>

namespace widgets {

class SpeedComboBoxDelegate : public QItemDelegate
{
  Q_OBJECT
public:
  explicit SpeedComboBoxDelegate(QObject* parent = nullptr);
  ~SpeedComboBoxDelegate();

  // QAbstractItemDelegate interface
public:
  void paint(QPainter* painter,
             const QStyleOptionViewItem& option,
             const QModelIndex& index) const override;
  QWidget* createEditor(QWidget* parent,
                        const QStyleOptionViewItem& option,
                        const QModelIndex& index) const override;
  void setEditorData(QWidget* editor, const QModelIndex& index) const override;
  void setModelData(QWidget* editor,
                    QAbstractItemModel* model,
                    const QModelIndex& index) const override;
  void updateEditorGeometry(QWidget* editor,
                            const QStyleOptionViewItem& option,
                            const QModelIndex& index) const override;
};

} // namespace models

#endif // COMBOBOXDELEGATE_H
