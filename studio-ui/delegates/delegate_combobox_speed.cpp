#include "delegate_combobox_speed.h"

#include "widgets/speed_mode_editor.h"
#include <QApplication>
#include <studio-lib/models/steps/moving_speed.h>

using namespace models;

namespace widgets {

SpeedComboBoxDelegate::SpeedComboBoxDelegate(QObject* parent)
  : QItemDelegate{ parent }
{}

SpeedComboBoxDelegate::~SpeedComboBoxDelegate() {}

void
SpeedComboBoxDelegate::paint(QPainter* painter,
                             const QStyleOptionViewItem& option,
                             const QModelIndex& index) const
{
  QStyleOptionComboBox comboBoxStyleOption;
  comboBoxStyleOption.rect = option.rect;

  const int value{ index.model()->data(index, Qt::EditRole).toInt() };
  const steps::eTravelSpeed speed{ static_cast<steps::eTravelSpeed>(value) };

  comboBoxStyleOption.currentIcon =
    QIcon{ steps::speedIconMapper.value(speed) };
  comboBoxStyleOption.currentText =
    QString{ steps::travelSpeedMapper.value(speed) };
  comboBoxStyleOption.editable = false;
  comboBoxStyleOption.iconSize = QSize{ 16, 16 };

  QApplication::style()->drawControl(
    QStyle::CE_ComboBoxLabel, &comboBoxStyleOption, painter);
}

QWidget*
SpeedComboBoxDelegate::createEditor(QWidget* parent,
                                    const QStyleOptionViewItem& option,
                                    const QModelIndex& index) const
{
  widgets::SpeedModeEditor* editor{ new widgets::SpeedModeEditor{ parent } };
  return editor;
}

void
SpeedComboBoxDelegate::setEditorData(QWidget* editor,
                                     const QModelIndex& index) const
{
  widgets::SpeedModeEditor* speed_edit{ static_cast<widgets::SpeedModeEditor*>(
    editor) };
  speed_edit->setCurrentIndex(index.model()->data(index, Qt::EditRole).toInt() -
                              1);
}

void
SpeedComboBoxDelegate::setModelData(QWidget* editor,
                                    QAbstractItemModel* model,
                                    const QModelIndex& index) const
{
  widgets::SpeedModeEditor* speed_edit{ static_cast<widgets::SpeedModeEditor*>(
    editor) };
  const int value{ speed_edit->currentIndex() + 1 };
  model->setData(index, value, Qt::EditRole);
}

void
SpeedComboBoxDelegate::updateEditorGeometry(QWidget* editor,
                                            const QStyleOptionViewItem& option,
                                            const QModelIndex& index) const
{
  editor->setGeometry(option.rect);
}

} // namespace models
