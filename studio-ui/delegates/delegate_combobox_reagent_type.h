#ifndef WIDGETS_DELEGATECOMBOBOXREAGENTTYPE_H
#define WIDGETS_DELEGATECOMBOBOXREAGENTTYPE_H

#include <QItemDelegate>

namespace widgets {

class ReagentTypeDelegate : public QItemDelegate
{
  Q_OBJECT
public:
  explicit ReagentTypeDelegate(QObject* parent = nullptr);
  ~ReagentTypeDelegate() override;

  // QAbstractItemDelegate interface
public:
  void paint(QPainter* painter,
             const QStyleOptionViewItem& option,
             const QModelIndex& index) const override;
  QWidget* createEditor(QWidget* parent,
                        const QStyleOptionViewItem& option,
                        const QModelIndex& index) const override;
  void setEditorData(QWidget* editor, const QModelIndex& index) const override;
  void setModelData(QWidget* editor,
                    QAbstractItemModel* model,
                    const QModelIndex& index) const override;
  void updateEditorGeometry(QWidget* editor,
                            const QStyleOptionViewItem& option,
                            const QModelIndex& index) const override;
};

} // namespace widgets

#endif // WIDGETS_DELEGATECOMBOBOXREAGENTTYPE_H
