#ifndef SPINBOXDELEGATE_H
#define SPINBOXDELEGATE_H

#include <QItemDelegate>

namespace widgets {

class SpinboxDelegate : public QItemDelegate
{
  Q_OBJECT
public:
  explicit SpinboxDelegate(QObject* parent = nullptr);
  ~SpinboxDelegate();

  // QAbstractItemDelegate interface
public:
  QWidget* createEditor(QWidget* parent,
                        const QStyleOptionViewItem& option,
                        const QModelIndex& index) const override;
  void setEditorData(QWidget* editor, const QModelIndex& index) const override;
  void setModelData(QWidget* editor,
                    QAbstractItemModel* model,
                    const QModelIndex& index) const override;
  void updateEditorGeometry(QWidget* editor,
                            const QStyleOptionViewItem& option,
                            const QModelIndex& index) const override;
};

} // namespace models

#endif // SPINBOXDELEGATE_H
