#include "delegate_combobox_reagent_type.h"

#include "widgets/reagent_mode_editor.h"
#include <QApplication>
#include <studio-lib/models/plates/reagent.h>

using namespace models;

namespace widgets {

ReagentTypeDelegate::ReagentTypeDelegate(QObject* parent)
  : QItemDelegate{ parent }
{}

ReagentTypeDelegate::~ReagentTypeDelegate() {}

void
ReagentTypeDelegate::paint(QPainter* painter,
                           const QStyleOptionViewItem& option,
                           const QModelIndex& index) const
{
  QStyleOptionComboBox comboBoxStyleOption;
  comboBoxStyleOption.rect = option.rect;

  const int value{ index.model()->data(index, Qt::EditRole).toInt() };
  const auto mode{ static_cast<plates::Reagent::eReagentType>(value) };

  comboBoxStyleOption.currentText =
    QString{ plates::Reagent::reagentTypeMapper.value(mode) };
  comboBoxStyleOption.editable = false;

  QApplication::style()->drawControl(
    QStyle::CE_ComboBoxLabel, &comboBoxStyleOption, painter);
}

QWidget*
ReagentTypeDelegate::createEditor(QWidget* parent,
                                  const QStyleOptionViewItem& option,
                                  const QModelIndex& index) const
{
  auto editor{ new widgets::ReagentTypeEditor{ parent } };
  return editor;
}

void
ReagentTypeDelegate::setEditorData(QWidget* editor,
                                   const QModelIndex& index) const
{
  auto mode_edit{ static_cast<widgets::ReagentTypeEditor*>(editor) };
  mode_edit->setCurrentIndex(index.model()->data(index, Qt::EditRole).toInt() -
                             1);
}

void
ReagentTypeDelegate::setModelData(QWidget* editor,
                                  QAbstractItemModel* model,
                                  const QModelIndex& index) const
{
  auto speed_edit{ static_cast<widgets::ReagentTypeEditor*>(editor) };
  const int value{ speed_edit->currentIndex() + 1 };
  model->setData(index, value, Qt::EditRole);
}

void
ReagentTypeDelegate::updateEditorGeometry(QWidget* editor,
                                          const QStyleOptionViewItem& option,
                                          const QModelIndex& index) const
{
  editor->setGeometry(option.rect);
}

} // namespace widgets
