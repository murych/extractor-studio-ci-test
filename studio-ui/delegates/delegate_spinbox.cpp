#include "delegate_spinbox.h"

#include <QSpinBox>

namespace widgets {

SpinboxDelegate::SpinboxDelegate(QObject* parent)
  : QItemDelegate{ parent }
{}

SpinboxDelegate::~SpinboxDelegate() {}

QWidget*
SpinboxDelegate::createEditor(QWidget* parent,
                              const QStyleOptionViewItem& option,
                              const QModelIndex& index) const
{
  QSpinBox* editor{ new QSpinBox{ parent } };
  editor->setMinimum(0);
  editor->setMaximum(1000);
  return editor;
}

void
SpinboxDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
  int value{ index.model()->data(index, Qt::EditRole).toInt() };
  QSpinBox* spinbox{ static_cast<QSpinBox*>(editor) };
  spinbox->setValue(value);
}

void
SpinboxDelegate::setModelData(QWidget* editor,
                              QAbstractItemModel* model,
                              const QModelIndex& index) const
{
  QSpinBox* spinbox{ static_cast<QSpinBox*>(editor) };
  spinbox->interpretText();
  int value = spinbox->value();
  model->setData(index, value, Qt::EditRole);
}

void
SpinboxDelegate::updateEditorGeometry(QWidget* editor,
                                      const QStyleOptionViewItem& option,
                                      const QModelIndex& index) const
{
  editor->setGeometry(option.rect);
}

} // namespace models
