#include "delegate_color.h"

#include "widgets/kcolorbutton.h"
#include <QApplication>
#include <QPainter>
#include <QPixmap>
#include <QPushButton>

namespace widgets {

ColorDelegate::ColorDelegate(QObject* parent)
  : QStyledItemDelegate{ parent }
{}

ColorDelegate::~ColorDelegate() {}

QWidget*
ColorDelegate::createEditor(QWidget* parent,
                            const QStyleOptionViewItem& option,
                            const QModelIndex& index) const
{
  //! @todo может проверять на конвертируемость в QString?
  if (index.data().canConvert<QColor>()) {
    KColorButton* editor{ new KColorButton{ parent } };
    //    connect(editor,
    //            &KColorButton::editingFinished,
    //            this,
    //            &ColorDelegate::commitAndCloseEditor);
    return editor;
  } else {
    return QStyledItemDelegate::createEditor(parent, option, index);
  }
}

void
ColorDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
  const QString value{ index.model()->data(index, Qt::EditRole).toString() };
  const QColor color{ value };

  KColorButton* button = static_cast<KColorButton*>(editor);
  button->setColor(color);
}

void
ColorDelegate::setModelData(QWidget* editor,
                            QAbstractItemModel* model,
                            const QModelIndex& index) const
{
  KColorButton* button = static_cast<KColorButton*>(editor);
  QString value = button->color().name();
  model->setData(index, value, Qt::EditRole);
}

void
ColorDelegate::paint(QPainter* painter,
                     const QStyleOptionViewItem& option,
                     const QModelIndex& index) const
{
  if (index.data().canConvert<QString>()) {
    painter->save();
    const QColor value{ index.model()->data(index).toString() };
    QRect rect{ option.rect };
    painter->fillRect(rect, value);
    painter->restore();
  } else {
    QStyledItemDelegate::paint(painter, option, index);
  }
}

void
ColorDelegate::updateEditorGeometry(QWidget* editor,
                                    const QStyleOptionViewItem& option,
                                    const QModelIndex& index) const
{
  editor->setGeometry(option.rect);
}

} // namespace widgets
