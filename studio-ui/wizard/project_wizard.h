#ifndef PROJECT_WIZARD_H
#define PROJECT_WIZARD_H

#include <QWizard>

namespace Ui {
class ProjectWizard;
}

namespace wizard {

class ProjectWizard : public QWizard
{
  Q_OBJECT

public:
  explicit ProjectWizard(QWidget* parent = nullptr);
  ~ProjectWizard();

private:
  Ui::ProjectWizard* ui;
};

} // namespace wizard

#endif // PROJECT_WIZARD_H
