#ifndef WIZARD_PAGE_PROJECT_INFO_H
#define WIZARD_PAGE_PROJECT_INFO_H

#include <QWizardPage>

namespace Ui {
class PageProjectInfo;
}

namespace wizard {

class PageProjectInfo : public QWizardPage
{
  Q_OBJECT

public:
  explicit PageProjectInfo(QWidget* parent = nullptr);
  ~PageProjectInfo();

private:
  Ui::PageProjectInfo* ui{ nullptr };
};

} // namespace wizard
#endif // WIZARD_PAGE_PROJECT_INFO_H
