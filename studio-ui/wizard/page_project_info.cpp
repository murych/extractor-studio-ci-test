#include "page_project_info.h"
#include "ui_page_project_info.h"

namespace wizard {

PageProjectInfo::PageProjectInfo(QWidget* parent)
  : QWizardPage{ parent }
  , ui{ new Ui::PageProjectInfo }
{
  ui->setupUi(this);

  registerField("projectTitle*", ui->projectTitleLineEdit);
  registerField("projectAuthor", ui->projectAuthorLineEdit);
  registerField("projectRevision", ui->projectRevisionSpinBox);
  registerField("projectDescription", ui->projectDescriptionTextEdit);
}

PageProjectInfo::~PageProjectInfo()
{
  delete ui;
}

} // namespace wizard
