#ifndef WIZARD_PAGE_DEVICE_INFO_H
#define WIZARD_PAGE_DEVICE_INFO_H

#include <QWizardPage>

namespace Ui {
class PageDeviceInfo;
}

namespace wizard {

class PageDeviceInfo : public QWizardPage
{
  Q_OBJECT

public:
  explicit PageDeviceInfo(QWidget* parent = nullptr);
  ~PageDeviceInfo();

private:
  Ui::PageDeviceInfo* ui{ nullptr };
};

} // namespace wizard
#endif // WIZARD_PAGE_DEVICE_INFO_H
