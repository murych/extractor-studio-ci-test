#include "page_device_info.h"
#include "ui_page_device_info.h"

namespace wizard {

PageDeviceInfo::PageDeviceInfo(QWidget* parent)
  : QWizardPage{ parent }
  , ui{ new Ui::PageDeviceInfo }
{
  ui->setupUi(this);

  registerField("deviceType", ui->deviceTypeComboBox);
  registerField("deviceModel", ui->deviceModelComboBox);
}

PageDeviceInfo::~PageDeviceInfo()
{
  delete ui;
}

} // namespace wizard
