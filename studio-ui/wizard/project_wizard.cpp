#include "project_wizard.h"
#include "ui_project_wizard.h"

#include "logger.h"
#include <QLoggingCategory>
#include <QPlainTextEdit>

Q_LOGGING_CATEGORY(WIZARD, "wizard")

namespace wizard {

ProjectWizard::ProjectWizard(QWidget* parent)
  : QWizard{ parent }
  , ui{ new Ui::ProjectWizard }
{
  ui->setupUi(this);
  setDefaultProperty("QPlainTextEdit", "plainText", "textChanged()");
}

ProjectWizard::~ProjectWizard()
{
  delete ui;
}

} // namespace wizard
