#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QHash>
#include <QMainWindow>
#include <QMap>

namespace Ui {
class MainWindow;
}

namespace models {
namespace project {
class ExtractorProject;
}
}

namespace settings {
class SettingsDialog;
}

class QToolButton;

class MainWindow : public QMainWindow
{
  Q_OBJECT
private:
  //! @todo перенести в класс models::project::Project?
  enum ProjectFormat
  {
    Binary = 0,
    Json
  };

  const QString file_filter_bin{ tr("Extractor Project File (*.extProj)") };
  const QString file_filter_json{ tr("JSON File (*.json)") };
  const QStringList project_filters{ { file_filter_bin, file_filter_json } };

  enum pages
  {
    home = 0,
    layout,
    protocol,
    run_info,
    reports
  };

  struct toolbutton;
  using toolbuttons_t = QHash<QString, toolbutton>;

  pages m_curPage{ pages::home };

public:
  explicit MainWindow(QWidget* parent = nullptr);
  ~MainWindow();

private:
  Ui::MainWindow* ui;
  settings::SettingsDialog* settings_dialog{ nullptr };
  models::project::ExtractorProject* current_project{ nullptr };

  //  bool save_as_binary{ true };
  bool is_project_opened{ false };
  //! директория с последним сохранением
  QString cur_work_dir{};
  //! путь к текущему проекту
  QString cur_proj_path{};
  //! название текущего проекта
  QString project_title{};

signals:
  void projectOpened();
  void projectClosed();

  void clearEditors();

  void proejctToSave();

private slots:
  void open();
  bool save();
  bool saveAs();
  void documentWasModified();
  void close();

private:
  bool maybeSave();
  void setCurrentFile(const QString& title);

  void readSettings();
  void writeSettings();

  void fillToolbar();

  void fillToolbarHome();
  void fillToolbarLayout();
  void fillToolbarProtocol();

  void addSectionOfButtons(const QString& category,
                           const QString& subCategory,
                           const QList<QAction*>& actions);
  void enableToolbutton(const QString& title);
  void disableToolbutton(const QString& title);
  toolbuttons_t toolbuttons;

  void readApplicationSettings();

  void projectLoad(const QString& filePath, const ProjectFormat format);
  bool projectSave(const QString& filePath, const ProjectFormat format);

private slots:
  //! @todo переделать так, чтобы это был не слот
  void projectCreate();

  void onProjectOpened();
  void onProjectClosed();

  void on_actionHelp_triggered();
  void on_actionAbout_triggered();

protected:
  void closeEvent(QCloseEvent* event) override;

private:
  QList<QAction*> add_step_actions;
  QList<QAction*> add_plates_actions;
};

#endif // MAINWINDOW_H
