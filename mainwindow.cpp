#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "dialogs/about_dialog.h"
#include "screens/abstract_screen.h"
#include "settings/settings_dialog.h"
#include "wizard/project_wizard.h"
#include <QApplication>
#include <QCborError>
#include <QCborMap>
#include <QCborValue>
#include <QCloseEvent>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QMenu>
#include <QMessageBox>
#include <QSaveFile>
#include <QSettings>
#include <studio-lib/config/config_app.h>
#include <studio-lib/config/config_lab.h>
#include <studio-lib/logger.h>
#include <studio-lib/models/project/extractor_project.h>

Q_LOGGING_CATEGORY(MAIN, "MW")

struct MainWindow::toolbutton
{
  QString subCategory{};
  QString title{};
  QToolButton* button{ nullptr };

  toolbutton() = default;
  toolbutton(const QString& _sub, QToolButton* _button)
    : subCategory{ _sub }
    , title{ _button->text() }
    , button{ _button }
  {}
};

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow{ parent }
  , ui{ new Ui::MainWindow }
  , settings_dialog{ new settings::SettingsDialog }
  , current_project{ new models::project::ExtractorProject }
  , cur_work_dir{ QDir::homePath() }
{
  ui->setupUi(this);

  add_step_actions.append({ ui->actionAddPickUpStep,
                            ui->actionAddLeaveStep,
                            ui->actionAddWaitStep,
                            ui->actionAddMixStep,
                            ui->actionAddDryStep,
                            ui->actionAddHeaterControlStep,
                            ui->actionAddCollectStep,
                            ui->actionAddReleaseStep,
                            ui->actionAddManualMoveStep,
                            ui->actionAddSleepStep });
  add_plates_actions.append({ ui->actionAddStandard96plate });
  ui->menuDevice->setEnabled(false);

  readSettings();

  QPalette palette = QPalette{};
  palette.setColor(QPalette::Window, QColor{ 0xff, 0xff, 0xff });
  this->setPalette(palette);

  // Hide ribbon dock title bar
  ui->ribbonDockWidget->setTitleBarWidget(new QWidget());

  for (int idx = 0; idx < ui->stackedWidget->count(); ++idx) {
    screens::AbstractScreen* screen =
      static_cast<screens::AbstractScreen*>(ui->stackedWidget->widget(idx));
    screen->setPalette(palette);

    connect(this,
            &MainWindow::projectOpened,
            screen,
            &screens::AbstractScreen::onProjectOpened);
    connect(this,
            &MainWindow::projectClosed,
            screen,
            &screens::AbstractScreen::onProjectClosed);

    ui->ribbonTabWidget->addTab(screen->icon(), screen->title());
  }

  fillToolbar();

  connect(ui->ribbonTabWidget,
          &Ribbon::currentChanged,
          ui->stackedWidget,
          &QStackedWidget::setCurrentIndex);
  ui->stackedWidget->setCurrentIndex(pages::home);

  onProjectClosed();

  connect(ui->actionNewProject,
          &QAction::triggered,
          this,
          &MainWindow::projectCreate);
  connect(ui->actionSaveProject, &QAction::triggered, this, &MainWindow::save);
  connect(
    ui->actionSaveProjectAs, &QAction::triggered, this, &MainWindow::saveAs);
  connect(ui->actionOpenProject, &QAction::triggered, this, &MainWindow::open);
  connect(
    ui->actionCloseProject, &QAction::triggered, this, &MainWindow::close);

  connect(ui->actionSettings,
          &QAction::triggered,
          settings_dialog,
          &settings::SettingsDialog::exec);

  connect(ui->actionAboutQt, &QAction::triggered, this, [this] {
    QApplication::aboutQt();
  });
}

MainWindow::~MainWindow()
{
  qCDebug(MAIN) << "~MainWindow";
  writeSettings();

  delete current_project;
  delete settings_dialog;
  delete ui;
}

void
MainWindow::projectCreate()
{
  qCDebug(MAIN) << "MainWindow::newFile";
  if (!maybeSave())
    return;

  emit clearEditors();

  wizard::ProjectWizard project_wizard{ this };
  int ret = project_wizard.exec();
  if (ret == QDialog::Rejected)
    return;

  QApplication::setOverrideCursor(Qt::WaitCursor);

  current_project = new models::project::ExtractorProject;
  connect(current_project,
          &models::project::ExtractorProject::modified,
          this,
          &MainWindow::documentWasModified);

  current_project->info->spec_major_version->setValue(0);
  current_project->info->spec_minor_version->setValue(1);

  current_project->info->title->setValue(
    project_wizard.field("projectTitle").toString());
  current_project->info->author->setValue(
    project_wizard.field("projectAuthor").toString());
  current_project->info->description->setValue(
    project_wizard.field("projectDescription").toString());

  const QDateTime cdt = QDateTime::currentDateTime().toLocalTime();
  current_project->info->date_created->setValue(cdt);
  current_project->info->date_modified->setValue(cdt);

  readApplicationSettings();

  onProjectOpened();

  qCDebug(MAIN) << "created new project" << current_project << "with layout"
                << current_project->layout << "with protocol"
                << current_project->protocol;

  ui->home->loadData(current_project);
  //  ui->layout->layout = current_project->layout;
  ui->layout->loadData(current_project->layout);
  //  ui->protocol->protocol = current_project->protocol;
  ui->protocol->loadData(current_project->protocol);

  QApplication::restoreOverrideCursor();
}

void
MainWindow::open()
{
  if (!maybeSave())
    return;

  QFileDialog dialog{ this };
  dialog.setNameFilters(project_filters);
  dialog.setDirectory(cur_work_dir);
  dialog.setWindowModality(Qt::WindowModal);
  dialog.setAcceptMode(QFileDialog::AcceptOpen);
  dialog.setFileMode(QFileDialog::ExistingFile);
  if (dialog.exec() != QDialog::Accepted)
    return;

  const QString fileName{ dialog.selectedFiles().constFirst() };
  const QFileInfo fileInfo{ fileName };

  if (!fileName.isEmpty())
    projectLoad(
      fileName,
      (dialog.selectedNameFilter() == file_filter_json ? Json : Binary));

  cur_work_dir = fileInfo.absolutePath();
  writeSettings();

  onProjectOpened();
  statusBar()->showMessage(tr("Project opened"), 2000);
}

void
MainWindow::documentWasModified()
{
  setWindowModified(current_project->is_modified);
}

void
MainWindow::close()
{
  if (!maybeSave())
    return;

  onProjectClosed();

  QApplication::exit(0);
}

void
MainWindow::setCurrentFile(const QString& title)
{
  project_title = title;
  current_project->is_modified = false;
  setWindowModified(false);

  QString shown_name = project_title;
  if (project_title.isEmpty())
    shown_name = QStringLiteral("Untitled.synproj");
  setWindowFilePath(shown_name);

  cur_proj_path = shown_name;
}

void
MainWindow::readSettings()
{
  config::ApplicationConfig config;
  config.readSettings();
  this->move(config.window_pos);
  this->resize(config.window_size);
  this->cur_work_dir = config.work_dir;
}

void
MainWindow::writeSettings()
{
  config::ApplicationConfig config;
  config.window_pos = this->pos();
  config.window_size = this->size();
  config.work_dir = this->cur_work_dir;
  config.writeSettings();
}

bool
MainWindow::maybeSave()
{
  qCDebug(MAIN) << "MainWindow::maybeSave"
                << "current_project->isModified"
                << current_project->is_modified;
  if (!current_project->is_modified)
    return true;

  const QMessageBox::StandardButton ret = QMessageBox::warning(
    this,
    QApplication::applicationName(),
    tr("The document has been modified.\n"
       "Do you want to save your changes?"),
    QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);

  switch (ret) {
    case QMessageBox::Save:
      return save();
    case QMessageBox::Cancel:
      return false;
    default:
      break;
  }
  return true;
}

bool
MainWindow::save()
{
  if (cur_proj_path.isEmpty())
    return saveAs();

  return projectSave(cur_proj_path, Binary);
}

bool
MainWindow::saveAs()
{
  QFileDialog dialog{ this };
  //  dialog.setNameFilters(project_filters);
  dialog.setNameFilter(file_filter_bin);
  dialog.setDirectory(cur_work_dir);
  dialog.setWindowModality(Qt::WindowModal);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  if (dialog.exec() != QDialog::Accepted)
    return false;

  const QString fileName{ dialog.selectedFiles().constFirst() };
  const QFileInfo info{ fileName };

  cur_work_dir = info.absolutePath();
  writeSettings();

  return projectSave(
    info.absoluteFilePath(),
    (dialog.selectedNameFilter() == file_filter_json ? Json : Binary));
}

bool
MainWindow::projectSave(const QString& filePath, const ProjectFormat format)
{
  QString errorMessage;
  QApplication::setOverrideCursor(Qt::WaitCursor);

  ui->layout->onProjectSaving();
  ui->protocol->onProjectSaving();

  QSaveFile file{ filePath };

  if (file.open(QFile::WriteOnly)) {
    file.write(format == Binary
                 ? QCborValue::fromJsonValue(current_project->toJson()).toCbor()
                 : QJsonDocument(current_project->toJson()).toJson());

    if (!file.commit()) {
      errorMessage =
        tr("Cannot write file %1:\n%2.")
          .arg(QDir::toNativeSeparators(filePath), file.errorString());
    }
  } else {
    errorMessage =
      tr("Cannot open file %1 for writing:\n%2.")
        .arg(QDir::toNativeSeparators(filePath), file.errorString());
  }
  QApplication::restoreOverrideCursor();

  if (!errorMessage.isEmpty()) {
    QMessageBox::warning(this, QApplication::applicationName(), errorMessage);
    return false;
  }

  current_project->is_modified = false;

  if (format == Binary)
    setCurrentFile(filePath);

  statusBar()->showMessage(tr("Project saved"), 2000);

  return true;
}

void
MainWindow::fillToolbar()
{
  fillToolbarHome();
  fillToolbarLayout();
  fillToolbarProtocol();
}

void
MainWindow::fillToolbarHome()
{
  QList<QAction*> projectActions = { ui->actionToolNewProject,
                                     ui->actionToolOpenProject,
                                     ui->actionToolSaveProject,
                                     ui->actionToolSaveProjectAs };
  addSectionOfButtons(ui->home->title(), tr("Project"), projectActions);

  toolbuttons.value(ui->actionToolNewProject->text()).button->setEnabled(true);
  toolbuttons.value(ui->actionToolOpenProject->text()).button->setEnabled(true);

  addSectionOfButtons(ui->home->title(),
                      tr("Settings"),
                      QList<QAction*>() << ui->actionToolSettings);
  toolbuttons.value(ui->actionToolSettings->text()).button->setEnabled(true);

  addSectionOfButtons(ui->home->title(),
                      tr("Device"),
                      QList<QAction*>()
                        << ui->actionConnect << ui->actionDisconnect
                        << ui->actionTransfer);
  //  toolbuttons.value(ui->actionConnect->text()).button->setEnabled(true);
}

void
MainWindow::fillToolbarLayout()
{
  addSectionOfButtons(ui->layout->title(), tr("Add Plate"), add_plates_actions);

  connect(ui->actionAddStandard96plate,
          &QAction::triggered,
          ui->layout,
          &screens::ScreenLayout::onAddStandard96Plate);

  ui->layout->addCreateActions(add_plates_actions);
}

void
MainWindow::fillToolbarProtocol()
{
  QList<QAction*> sleevesActions{ ui->actionAddPickUpStep,
                                  ui->actionAddLeaveStep,
                                  ui->actionAddWaitStep };
  addSectionOfButtons(
    ui->protocol->title(), tr("Sleeves actions"), sleevesActions);

  addSectionOfButtons(ui->protocol->title(),
                      tr("Regular actions"),
                      QList<QAction*>()
                        << ui->actionAddMixStep << ui->actionAddDryStep
                        << ui->actionAddHeaterControlStep);

  addSectionOfButtons(ui->protocol->title(),
                      tr("Beads actions"),
                      QList<QAction*>() << ui->actionAddCollectStep
                                        << ui->actionAddReleaseStep);

  QList<QAction*> manualActions{ ui->actionAddManualMoveStep,
                                 ui->actionAddSleepStep };
  addSectionOfButtons(
    ui->protocol->title(), tr("Manual actions"), manualActions);

  addSectionOfButtons(ui->protocol->title(),
                      tr("Protocol actions"),
                      QList<QAction*>() << ui->actionExportProtocol);

  connect(ui->actionAddPickUpStep,
          &QAction::triggered,
          ui->protocol,
          &screens::ScreenProtocol::onCreateStepSleevesCollect);
  connect(ui->actionAddLeaveStep,
          &QAction::triggered,
          ui->protocol,
          &screens::ScreenProtocol::onCreateStepSleevesRelease);
  connect(ui->actionAddWaitStep,
          &QAction::triggered,
          ui->protocol,
          &screens::ScreenProtocol::onCreateStepBlock);
  connect(ui->actionAddMixStep,
          &QAction::triggered,
          ui->protocol,
          &screens::ScreenProtocol::onCreateStepMix);
  connect(ui->actionAddDryStep,
          &QAction::triggered,
          ui->protocol,
          &screens::ScreenProtocol::onCreateStepDry);
  connect(ui->actionAddHeaterControlStep,
          &QAction::triggered,
          ui->protocol,
          &screens::ScreenProtocol::onCreateStepHeater);
  connect(ui->actionAddCollectStep,
          &QAction::triggered,
          ui->protocol,
          &screens::ScreenProtocol::onCreateStepBeadsCollect);
  connect(ui->actionAddReleaseStep,
          &QAction::triggered,
          ui->protocol,
          &screens::ScreenProtocol::onCreateStepBeadsRelease);
  connect(ui->actionAddManualMoveStep,
          &QAction::triggered,
          ui->protocol,
          &screens::ScreenProtocol::onCreateStepManualMove);
  connect(ui->actionAddSleepStep,
          &QAction::triggered,
          ui->protocol,
          &screens::ScreenProtocol::onCreateStepPause);

  ui->protocol->addCreateActions(add_step_actions);
}

void
MainWindow::addSectionOfButtons(const QString& category,
                                const QString& subCategory,
                                const QList<QAction*>& actions)
{
  for (auto& action : qAsConst(actions)) {
    QToolButton* newButton = new QToolButton;
    newButton->setDefaultAction(action);
    newButton->setEnabled(false);
    toolbutton button{ subCategory, newButton };
    toolbuttons.insert(button.title, button);
    ui->ribbonTabWidget->addButton(category, button.subCategory, button.button);
  }
}

void
MainWindow::enableToolbutton(const QString& title)
{
  QToolButton* button = toolbuttons.value(title).button;
  if (!button)
    return;

  button->setEnabled(true);
}

void
MainWindow::disableToolbutton(const QString& title)
{
  QToolButton* button = toolbuttons.value(title).button;
  if (!button)
    return;

  button->setEnabled(false);
}

void
MainWindow::readApplicationSettings()
{
  if (!current_project)
    return;

  config::LaboratoryConfig config;
  config.readSettings();
  current_project->laboratory->setName(config.lab_name);
  current_project->laboratory->setAddress(config.lab_address);
  current_project->laboratory->setEmail(config.lab_email);
  current_project->laboratory->setPhone(config.lab_phone);
}

void
MainWindow::projectLoad(const QString& fileName, const ProjectFormat format)
{
  qCDebug(MAIN) << "MainWindow::projectLoad" << fileName << format;

  QFile file{ fileName };
  if (!file.open(QFile::ReadOnly)) {
    QMessageBox::warning(
      this,
      QApplication::applicationName(),
      tr("Cannot open project %1:\n%2")
        .arg(QDir::toNativeSeparators(fileName), file.errorString()));
    return;
  }

  current_project = new models::project::ExtractorProject;
  connect(current_project,
          &models::project::ExtractorProject::modified,
          this,
          &MainWindow::documentWasModified);

  const QByteArray data{ file.readAll() };
  const QJsonDocument jsonDocument{
    format == Json
      ? QJsonDocument::fromJson(data)
      : QJsonDocument(QCborValue::fromCbor(data).toMap().toJsonObject())
  };
  const QJsonObject json{ jsonDocument.object() };

  if (json.contains(current_project->info->key()))
    current_project->info->update(
      json.value(current_project->info->key()).toObject());

  if (json.contains(current_project->laboratory->key()))
    current_project->laboratory->update(
      json.value(current_project->laboratory->key()).toObject());

  if (json.contains(current_project->layout->key()))
    current_project->layout->update(
      json.value(current_project->layout->key()).toObject());

  if (json.contains(current_project->protocol->key())) {
    const QJsonObject jsonProtocol{
      json.value(current_project->protocol->key()).toObject()
    };
    models::project::Protocol* protocol{ new models::project::Protocol{
      current_project, jsonProtocol } };
    current_project->protocol = protocol;
  }

  //  current_project->update(json);
  //  qCDebug(MAIN) << json << "\n\n" << current_project->info->title->value();
  if (current_project->protocol->steps->derivedEntities().count() > 0) {
    qCDebug(MAIN) << current_project->protocol->steps->baseEntities();
    qCDebug(MAIN) << current_project->protocol->steps->derivedEntities();
  }

  if (current_project->layout->plates->derivedEntities().count() > 0)
    qCDebug(MAIN) << current_project->layout->plates->derivedEntities();

  ui->home->loadData(current_project);
  //  ui->layout->layout = current_project->layout;
  ui->layout->loadData(current_project->layout);
  //  ui->protocol->protocol = current_project->protocol;
  ui->protocol->loadData(current_project->protocol);
}

void
MainWindow::onProjectOpened()
{
  ui->ribbonTabWidget->setTabEnabled(layout, true);
  ui->ribbonTabWidget->setTabEnabled(protocol, true);

  setCurrentFile(current_project->info->title->value());
  current_project->is_modified = true;

  ui->menuEdit->setEnabled(true);
  ui->menuLayout->setEnabled(true);
  ui->menuProtocol->setEnabled(true);

  ui->actionSaveProject->setEnabled(true);
  ui->actionSaveProjectAs->setEnabled(true);
  ui->actionCloseProject->setEnabled(true);

  toolbuttons.value(ui->actionToolSaveProject->text()).button->setEnabled(true);
  toolbuttons.value(ui->actionToolSaveProjectAs->text())
    .button->setEnabled(true);

  for (const QAction* action : qAsConst(add_step_actions))
    enableToolbutton(action->text());

  for (const QAction* action : qAsConst(add_plates_actions))
    enableToolbutton(action->text());

  is_project_opened = true;

  emit projectOpened();
}

void
MainWindow::onProjectClosed()
{
  ui->ribbonTabWidget->setTabEnabled(layout, false);
  ui->ribbonTabWidget->setTabEnabled(protocol, false);

  ui->menuEdit->setEnabled(false);
  ui->menuLayout->setEnabled(false);
  ui->menuProtocol->setEnabled(false);

  ui->actionSaveProject->setEnabled(false);
  ui->actionSaveProjectAs->setEnabled(false);
  ui->actionCloseProject->setEnabled(false);

  toolbuttons.value(ui->actionToolSaveProject->text())
    .button->setEnabled(false);
  toolbuttons.value(ui->actionToolSaveProjectAs->text())
    .button->setEnabled(false);

  for (const QAction* action : qAsConst(add_step_actions))
    disableToolbutton(action->text());

  for (const QAction* action : qAsConst(add_plates_actions))
    disableToolbutton(action->text());

  is_project_opened = false;

  emit projectClosed();
}

void
MainWindow::closeEvent(QCloseEvent* event)
{
  qCDebug(MAIN) << "MainWindow::closeEvent";
  if (maybeSave()) {
    writeSettings();
    event->accept();
  } else {
    event->ignore();
  }
}

void
MainWindow::on_actionHelp_triggered()
{
  QMessageBox::information(
    this, QApplication::applicationName(), tr("Application help will be here"));
}

void
MainWindow::on_actionAbout_triggered()
{
  dialogs::AboutDialog dialog{ this };
  dialog.exec();
}
