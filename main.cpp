#include "mainwindow.h"
#include <QApplication>
#include <QLoggingCategory>
#include <studio-lib/logger.h>

Q_LOGGING_CATEGORY(LOG_APP, "app")

/*
#include <studio-lib/models/extractor_project.h>
#include <studio-lib/models/step_heater.h>
static const quint32 magic_number = 0xA0B0C0D0;
static const quint8 spec_minor = 1;
static const quint8 spec_major = 0;
*/

/*
#include <QCborMap>
#include <QCborValue>
#include <QDateTime>
#include <QJsonArray>
#include <QJsonDocument>
#include <studio-lib/models/device/device_extractor.h>
#include <studio-lib/models/project/protocol.h>
*/

int
main(int argc, char* argv[])
{
  QApplication::setOrganizationName("Syntol");
  QApplication::setOrganizationDomain("syntol.ru");
  QApplication::setApplicationName("Extractor Designer");
  QApplication::setApplicationVersion("0.1");
  QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QApplication::setApplicationDisplayName(QApplication::applicationName());

  QApplication a(argc, argv);

  qInstallMessageHandler(Logger::messageOutput);
  qCInfo(LOG_APP) << "starting app" << QApplication::applicationName() << "on"
                  << QDateTime::currentDateTime().toString(Qt::LocalDate);

  /*
  QFile jsonFile{ ":/device-sample" };
  jsonFile.open(QIODevice::ReadOnly | QIODevice ::Text);
  QJsonObject jsonObject{
    QJsonDocument::fromJson(jsonFile.readAll()).object()
  };
  models::device::DeviceExtractor device{ nullptr, jsonObject };
  qCDebug(LOG_APP) << device.toJson()
                   << device.plates->derivedEntities().count()
                   << device.motors->derivedEntities().count();
  */

  MainWindow w;
  w.show();
  /*
  QFile file{ "/home/tmayzenberg/dist/work/PROJECTS/EXTRACTOR/script-creator/"
              "test-proj.extProj" };
  if (!file.open(QFile::ReadOnly))
    return -1;
  const QByteArray data{ file.readAll() };
  const QJsonDocument jsonDocument{ QJsonDocument(
    QCborValue::fromCbor(data).toMap().toJsonObject()) };
  const QJsonObject json{ jsonDocument.object() };

  const QJsonObject jsonProtocol{ json.value("protocol").toObject() };
  const QJsonArray jsonSteps{ jsonProtocol.value("steps").toArray() };
  qCDebug(LOG_APP) << jsonSteps;

  models::project::Protocol protocol{ nullptr, jsonProtocol };
  qCDebug(LOG_APP) << protocol.steps->derivedEntities();
*/

  /*
  models::steps::StepMix* cStep = new models::steps::StepMix;
  qDebug() << "concrete item" << cStep;

  models::steps::AbstractStep* aStep = new models::steps::AbstractStep;
  qDebug() << "abstract item" << aStep;

  qDebug() << qobject_cast<models::steps::AbstractStep*>(cStep) << Qt::endl
           << qobject_cast<models::steps::StepMix*>(aStep);
  */

  int ret_code = a.exec();

  qCInfo(LOG_APP) << "app exited with code" << ret_code;

  return ret_code;
}
