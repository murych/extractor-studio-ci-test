#include "logger.h"

#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QLoggingCategory>
#include <QStandardPaths>
#include <QTextStream>
#include <QTimer>

#define APP_ORG "Syntol"
#define APP_NAME "extractor-studio"
static const QString fmtDateTime("yyyy-MM-ddThh:mm:ss.zzz");

Q_LOGGING_CATEGORY(CATEGORY_LOGGER, "LOG")

Logger::Logger(QObject* parent)
  : QObject{ parent }
  , m_logDir{ QStandardPaths::writableLocation(
      QStandardPaths::GenericDataLocation) }
  , m_logFile{ new QFile{ this } }
  , m_updateTimer{ new QTimer{ this } }
  , m_stderr{ stderr, QIODevice::WriteOnly }
  , m_fileOut{ m_logFile }
  , m_startTime{ QDateTime::currentDateTime() }
  , m_logLevel{ LogLevel::Default }
  , m_maxLineCount{ 200 }
  , m_errorCount{ 0 }
{
  m_updateTimer->setSingleShot(true);
  m_updateTimer->setInterval(100);

  connect(m_updateTimer, &QTimer::timeout, this, &Logger::logTextChanged);

#ifndef QT_DEBUG
  m_logDir.mkdir(APP_ORG);

  if (!m_logDir.exists(APP_ORG)) {
    fallbackMessageOutput(QStringLiteral("Failed to create logs directory"));
    return;
  } else if (!m_logDir.cd(APP_ORG)) {
    fallbackMessageOutput(QStringLiteral("Failed to access logs directory"));
    return;
  } else if (!removeOldFiles()) {
    fallbackMessageOutput(QStringLiteral("Failed to remove old files"));
    return;
  }

  const auto fileName{
    QStringLiteral("%1-%2.log")
      .arg(APP_NAME, m_startTime.toString(QStringLiteral("yyyyMMdd-hhmmss")))
  };
  const auto filePath{ m_logDir.absoluteFilePath(fileName) };
  m_logFile->setFileName(filePath);

  if (!m_logFile->open(QIODevice::WriteOnly)) {
    fallbackMessageOutput(QStringLiteral("Failed to open log file: %1")
                            .arg(m_logFile->errorString()));
  }
#endif
}

Logger*
Logger::instance()
{
  static Logger* logger = new Logger();
  return logger;
}

void
Logger::messageOutput(QtMsgType type,
                      const QMessageLogContext& ctx,
                      const QString& msg)
{
  const QDateTime cdt{ QDateTime::currentDateTime() };
  const QString text{ QStringLiteral("[%1] [%2] %3")
                        .arg(ctx.category, cdt.toString(fmtDateTime), msg) };
  const QString criticalText{ QStringLiteral(
    "<font color=\"#ff1f00\">%1</font>") };

#ifndef QT_DEBUG
  // writing everything in the file regardless of the log level
  if (Logger::instance()->m_logFile->isOpen())
    Logger::instance()->m_fileOut << text << Qt::endl;
#endif

  const bool filterNonError{ Logger::instance()->m_logLevel ==
                               LogLevel::ErrorsOnly &&
                             type != QtCriticalMsg };
  const bool filterDebug{ Logger::instance()->m_logLevel == LogLevel::Terse &&
                          type == QtDebugMsg };

  if (filterNonError || filterDebug)
    return;

  Logger::instance()->m_stderr << text << Qt::endl;

  const bool filterWithoutCategory{ !strcmp(ctx.category, "default") };
  const bool filterPretty{ type == QtDebugMsg };

  if (filterWithoutCategory || filterPretty)
    return;

#ifndef QT_DEBUG
  Logger::instance()->append(type == QtCriticalMsg ? criticalText.arg(text)
                                                   : text);
#endif

  Logger::instance()->setErrorCount(Logger::instance()->errorCount() +
                                    (type == QtCriticalMsg ? 1 : 0));
}

const QUrl
Logger::logsPath() const
{
  return QUrl::fromLocalFile(m_logDir.absolutePath());
}

const QUrl
Logger::logsFile() const
{
  return QUrl::fromLocalFile(m_logFile->fileName());
}

int
Logger::errorCount() const
{
  return m_errorCount;
}

void
Logger::setErrorCount(const int count)
{
  if (m_errorCount == count)
    return;

  m_errorCount = count;
  emit errorCountChanged();
}

QString
Logger::logText() const
{
  return m_logText.join("<br/>");
}

void
Logger::setLogLevel(LogLevel level)
{
  if (m_logLevel == level)
    return;

  m_logLevel = level;
}

void
Logger::append(const QString& line)
{
  m_logText.append(line);

  if (m_logText.size() > m_maxLineCount)
    m_logText.removeFirst();

  if (!m_updateTimer->isActive())
    m_updateTimer->start();
}

void
Logger::fallbackMessageOutput(const QString& msg)
{
  m_stderr << "[" << CATEGORY_LOGGER().categoryName() << "] " << msg
           << Qt::endl;
}

bool
Logger::removeOldFiles()
{
  constexpr int maxFileCount{ 99 };
  const QFileInfoList files{ m_logDir.entryInfoList(
    QDir::Files, QDir::Time | QDir::Reversed) };
  const int excessFileCount{ files.size() - maxFileCount };

  for (qsizetype i = 0; i < excessFileCount; ++i) {
    const QFileInfo& fileInfo{ files.at(i) };
    if (m_logDir.remove(fileInfo.fileName())) {
      fallbackMessageOutput(
        QStringLiteral("Failed to remove file: %1").arg(fileInfo.fileName()));
      return false;
    }
  }

  return true;
}
