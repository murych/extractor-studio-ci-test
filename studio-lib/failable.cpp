#include "failable.h"
#include <QVariant>

Failable::Failable()
{
  clearError();
}

bool
Failable::isError() const
{
  return m_error != BackendError::NoError;
}

BackendError::ErrorType
Failable::error() const
{
  return m_error;
}

void
Failable::setError(BackendError::ErrorType error)
{
  if (m_error != error)
    m_error = error;
}

void
Failable::setError(BackendError::ErrorType error, const QString& errorString)
{
  if (m_error != error) {
    if (m_errorString != errorString) {
      m_errorString = errorString;
    }
    m_error = error;
  }
}

const QString&
Failable::errorString() const
{
  return m_errorString;
}

void
Failable::setErrorString(const QString& errorString)
{
  m_error = BackendError::UnknownError;
  m_errorString = errorString;
}

void
Failable::clearError()
{
  m_error = BackendError::NoError;
  m_errorString = QVariant::fromValue(BackendError::NoError).toString();
}
