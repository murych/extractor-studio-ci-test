#ifndef BACKEND_ERROR_H
#define BACKEND_ERROR_H

#include <QObject>

class BackendError
{
  Q_GADGET

public:
  enum ErrorType
  {
    // general errors
    NoError,
    UnknownError,

    // top-level errors
    InvalidDevice, //! cannot determine device type
    InternetError,
    DiskError,        //! cannot read/write to disk
    DataError,        //! necessary files are correupted
    SerialAcessError, //! cannot access device in serial mode
    OperationError,   //! current operation interrupted

    // low-level errors
    SerialError,   //! cannot open/read/write to/from serial port
    ProtocolError, //! recieved an error , unexpected ot not implemented rpc
                   //! responce
    TimeoutError   //! operation took to long to execute
  };
  Q_ENUM(ErrorType)
};

#endif // BACKEND_ERROR_H
