#include "color_decorator.h"

#include <QVariant>

namespace data {

class ColorDecorator::Implementation
{
public:
  Implementation(ColorDecorator* _colorDecorator, const QString& _value)
    : colorDecorator{ _colorDecorator }
    , value{ _value }
  {}

  ColorDecorator* colorDecorator{ nullptr };
  QString value{};
};

ColorDecorator::ColorDecorator(Entity* parentEntity,
                               const QString& key,
                               const QString& label,
                               const QString& value)
  : DataDecorator{ parentEntity, key, label }
{
  implementation.reset(new Implementation{ this, value });
}

ColorDecorator::~ColorDecorator() {}

ColorDecorator&
ColorDecorator::setValue(const QString& value)
{
  if (value != implementation->value) {
    implementation->value = value;
    emit valueChanged();
  }
  return *this;
}

const QString&
ColorDecorator::value() const
{
  return implementation->value;
}

QJsonValue
ColorDecorator::jsonValue() const
{
  return QJsonValue::fromVariant(QVariant{ implementation->value });
}

void
ColorDecorator::update(const QJsonObject& jsonObject)
{
  if (jsonObject.contains(key())) {
    setValue(jsonObject.value(key()).toString());
  } else {
    setValue("");
  }
}

}
