#ifndef DATA_DOUBLEDECORATOR_H
#define DATA_DOUBLEDECORATOR_H

#include "data_decorator.h"

namespace data {

class DoubleDecorator : public DataDecorator
{
  Q_OBJECT
  Q_PROPERTY(double ui_value READ value WRITE setValue NOTIFY valueChanged)
public:
  DoubleDecorator(Entity* parentEntity = nullptr,
                  const QString& key = "SomeItemKey",
                  const QString& label = "",
                  const double value = 0.0);
  ~DoubleDecorator() override;

  DoubleDecorator& setValue(const double value);
  double value() const;

  QJsonValue jsonValue() const override;
  void update(const QJsonObject& jsonObject) override;

signals:
  void valueChanged();

private:
  class Implementation;
  QScopedPointer<Implementation> implementation;
};

} // namespace data

#endif // DATA_DOUBLEDECORATOR_H
