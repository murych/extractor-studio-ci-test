#ifndef INTDECORATOR_H
#define INTDECORATOR_H

//#include "cm-lib_global.h"
#include "data_decorator.h"
#include <QJsonObject>
#include <QJsonValue>
#include <QObject>
#include <QScopedPointer>
#include <QVariant>

// namespace cm {
namespace data {

class IntDecorator : public DataDecorator
{
  Q_OBJECT
  Q_PROPERTY(int ui_value READ value WRITE setValue NOTIFY valueChanged)

public:
  IntDecorator(Entity* parentEntity = nullptr,
               const QString& key = "SomeItemKey",
               const QString& label = "",
               const int value = 0);
  ~IntDecorator();

  IntDecorator& setValue(const int value);
  int value() const;

  QJsonValue jsonValue() const override;
  void update(const QJsonObject& jsonObject) override;

signals:
  void valueChanged();

private:
  class Implementation;
  QScopedPointer<Implementation> implementation;
};

} // namespace data
//} // namespace cm

#endif // INTDECORATOR_H
