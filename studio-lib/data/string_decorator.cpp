#include "string_decorator.h"
#include <QVariant>

// namespace cm {
namespace data {

class StringDecorator::Implementation
{
public:
  Implementation(StringDecorator* _stringDecorator, const QString& _value)
    : stringDecorator(_stringDecorator)
    , value(_value)
  {}

  StringDecorator* stringDecorator{ nullptr };
  QString value{};
};

StringDecorator::StringDecorator(Entity* parentEntity,
                                 const QString& key,
                                 const QString& label,
                                 const QString& value)
  : DataDecorator(parentEntity, key, label)
{
  implementation.reset(new Implementation(this, value));
}

StringDecorator::~StringDecorator() {}

StringDecorator&
StringDecorator::setValue(const QString& value)
{
  if (value != implementation->value) {
    // validation here if required
    implementation->value = value;
    emit valueChanged();
  }
  return *this;
}

const QString&
StringDecorator::value() const
{
  return implementation->value;
}

QJsonValue
StringDecorator::jsonValue() const
{
  return QJsonValue::fromVariant(QVariant(implementation->value));
}

void
StringDecorator::update(const QJsonObject& jsonObject)
{
  if (jsonObject.contains(key())) {
    setValue(jsonObject.value(key()).toString());
  } else {
    setValue("");
  }
}

} // namespace data
//} // namespace cm
