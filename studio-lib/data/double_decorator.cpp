#include "double_decorator.h"

namespace data {

class DoubleDecorator::Implementation
{
public:
  Implementation(DoubleDecorator* _doubleDecorator, const double _value)
    : doubleDecorator{ _doubleDecorator }
    , value{ _value }
  {}

  DoubleDecorator* doubleDecorator{ nullptr };
  double value{ 0.0 };
};

DoubleDecorator::DoubleDecorator(Entity* parentEntity,
                                 const QString& key,
                                 const QString& label,
                                 const double value)
  : DataDecorator{ parentEntity, key, label }
{
  implementation.reset(new Implementation{ this, value });
}

DoubleDecorator::~DoubleDecorator() {}

DoubleDecorator&
DoubleDecorator::setValue(const double value)
{
  if (value != implementation->value) {
    implementation->value = value;
    emit valueChanged();
  }
  return *this;
}

double
DoubleDecorator::value() const
{
  return implementation->value;
}

QJsonValue
DoubleDecorator::jsonValue() const
{
  return QJsonValue::fromVariant(QVariant{ implementation->value });
}

void
DoubleDecorator::update(const QJsonObject& jsonObject)
{
  if (jsonObject.contains(key())) {
    setValue(jsonObject.value(key()).toDouble());
  } else {
    setValue(double(0.0));
  }
}

} // namespace data
