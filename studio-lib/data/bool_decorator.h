#ifndef DATA_BOOLDECORATOR_H
#define DATA_BOOLDECORATOR_H

#include "data_decorator.h"

namespace data {

class BoolDecorator : public data::DataDecorator
{
  Q_OBJECT
public:
  BoolDecorator(Entity* parentEntity = nullptr,
                const QString& key = "SomeItemKey",
                const QString& label = "",
                const bool value = false);
  ~BoolDecorator() override;

  BoolDecorator& setValue(const bool value);
  bool value() const;

  QJsonValue jsonValue() const override;
  void update(const QJsonObject& jsonObject) override;

signals:
  void valueChanged();

private:
  class Implementation;
  QScopedPointer<Implementation> implementation;
};

} // namespace data

#endif // DATA_BOOLDECORATOR_H
