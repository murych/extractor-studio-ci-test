#ifndef ENUMERATORDECORATOR_H
#define ENUMERATORDECORATOR_H

#include <QJsonObject>
#include <QJsonValue>
#include <QMap>
#include <QObject>
#include <QScopedPointer>
#include <map>

//#include "cm-lib_global.h"
#include "data_decorator.h"

// namespace cm {
namespace data {

class /*CMLIB_EXPORT*/ EnumeratorDecorator : public DataDecorator
{
  Q_OBJECT
  Q_PROPERTY(int ui_value READ value WRITE setValue NOTIFY valueChanged)
  Q_PROPERTY(
    QString ui_valueDescription READ valueDescription NOTIFY valueChanged)
public:
  EnumeratorDecorator(
    Entity* parentEntity = nullptr,
    const QString& key = "SomeItemKey",
    const QString& label = "",
    int value = 0,
    const QMap<int, QString>& descriptionMapper = QMap<int, QString>());
  ~EnumeratorDecorator();

  EnumeratorDecorator& setValue(int value);
  int value() const;
  QString valueDescription() const;

  QJsonValue jsonValue() const override;
  void update(const QJsonObject& jsonObject) override;

signals:
  void valueChanged();

private:
  class Implementation;
  QScopedPointer<Implementation> implementation;
};

} // namespace data
//} // namespace cm

#endif // ENUMERATORDECORATOR_H
