#include "datetime_decorator.h"
#include <QVariant>

// namespace cm {
namespace data {

class DateTimeDecorator::Implementation
{
public:
  Implementation(DateTimeDecorator* _dateTimeDecorator, const QDateTime& _value)
    : dateTimeDecorator{ _dateTimeDecorator }
    , value{ _value }
  {}

  DateTimeDecorator* dateTimeDecorator{ nullptr };
  QDateTime value{};
};

DateTimeDecorator::DateTimeDecorator(Entity* parentEntity,
                                     const QString& key,
                                     const QString& label,
                                     const QDateTime& value)
  : DataDecorator{ parentEntity, key, label }
{
  implementation.reset(new Implementation(this, value));
}

DateTimeDecorator::~DateTimeDecorator() {}

DateTimeDecorator&
DateTimeDecorator::setValue(const QDateTime& dateTime)
{
  if (dateTime != implementation->value) {
    implementation->value = dateTime;
    emit valueChanged();
  }
  return *this;
}

const QDateTime&
DateTimeDecorator::value() const
{
  return implementation->value;
}

QJsonValue
DateTimeDecorator::jsonValue() const
{
  return QJsonValue::fromVariant(
    QVariant{ implementation->value.toString(Qt::ISODate) });
}

void
DateTimeDecorator::update(const QJsonObject& jsonObject)
{
  if (jsonObject.contains(key())) {
    const auto valueAsString{ jsonObject.value(key()).toString() };
    const auto valueAsDate{ QDateTime::fromString(valueAsString, Qt::ISODate) };
    setValue(valueAsDate);
  } else {
    setValue(QDateTime{});
  }
}

const QString
DateTimeDecorator::toIso8601String() const
{
  if (implementation->value.isNull()) {
    return "";
  } else {
    return implementation->value.toString(Qt::ISODate);
  }
}

const QString
DateTimeDecorator::toPrettyDateString() const
{
  if (implementation->value.isNull()) {
    return "Not set";
  } else {
    return implementation->value.toString("ddd d MMM yyyy @ HH:mm:ss");
  }
}

const QString
DateTimeDecorator::toPrettyTimeString() const
{
  if (implementation->value.isNull()) {
    return "not set";
  } else {
    return implementation->value.toString("d MMM yyyy");
  }
}

const QString
DateTimeDecorator::toPrettyString() const
{
  if (implementation->value.isNull())
    return "not set";
  else
    return implementation->value.toString("hh:mm ap");
}

} // namespace data
//} // namespace cm
