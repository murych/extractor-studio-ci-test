#include "enumerator_decorator.h"
#include <QVariant>

// namespace cm {
namespace data {

class EnumeratorDecorator::Implementation
{
public:
  Implementation(EnumeratorDecorator* _enumeratorDecorator,
                 const int _value,
                 const QMap<int, QString> _descriptionMapper)
    : enumeratorDecorator{ _enumeratorDecorator }
    , value{ _value }
    , descriptionMapper{ _descriptionMapper }
  {}

  EnumeratorDecorator* enumeratorDecorator{ nullptr };
  int value{ 0 };
  QMap<int, QString> descriptionMapper;
};

EnumeratorDecorator::EnumeratorDecorator(
  Entity* parentEntity,
  const QString& key,
  const QString& label,
  int value,
  const QMap<int, QString>& descriptionMapper)
  : DataDecorator{ parentEntity, key, label }
{
  implementation.reset(new Implementation{ this, value, descriptionMapper });
}

EnumeratorDecorator::~EnumeratorDecorator() {}

EnumeratorDecorator&
EnumeratorDecorator::setValue(int value)
{
  if (value != implementation->value) {
    implementation->value = value;
    emit valueChanged();
  }
  return *this;
}

int
EnumeratorDecorator::value() const
{
  return implementation->value;
}

QString
EnumeratorDecorator::valueDescription() const
{
  if (implementation->descriptionMapper.find(implementation->value) !=
      implementation->descriptionMapper.end()) {
    return implementation->descriptionMapper.value(implementation->value);
  } else {
    return {};
  }
}

QJsonValue
EnumeratorDecorator::jsonValue() const
{
  return QJsonValue::fromVariant(QVariant{ implementation->value });
}

void
EnumeratorDecorator::update(const QJsonObject& jsonObject)
{
  if (jsonObject.contains(key())) {
    const auto valueFromJson{ jsonObject.value(key()).toInt() };
    setValue(valueFromJson);
  } else {
    setValue(0);
  }
}

} // namespace data
//} // namespace cm
