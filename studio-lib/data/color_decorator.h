#ifndef COLORDECORATOR_H
#define COLORDECORATOR_H

#include "data_decorator.h"
#include <QJsonObject>
#include <QJsonValue>
#include <QScopedPointer>
#include <QString>

namespace data {

//! @note так как в Qt::Core отсутсвует класс QColor, то храним тупо hex с
//! возможностью как-нибудь отдавать
//! @todo а может сделать еще один конструктор, который на вход будет принимать
//! RGB в int'ах для сбора hex'а :man_shrugging:
class ColorDecorator : public DataDecorator
{
  Q_OBJECT
public:
  explicit ColorDecorator(Entity* parent = nullptr,
                          const QString& key = "SomeItemKey",
                          const QString& label = "",
                          const QString& value = "");
  ~ColorDecorator();

  ColorDecorator& setValue(const QString& value);
  const QString& value() const;

  QJsonValue jsonValue() const override;
  void update(const QJsonObject& jsonObject) override;

signals:
  void valueChanged();

private:
  class Implementation;
  QScopedPointer<Implementation> implementation;
};

}

#endif // COLORDECORATOR_H
