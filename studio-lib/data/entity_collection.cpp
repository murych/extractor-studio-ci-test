#include "entity_collection.h"

namespace data {

template<class T>
QList<T*>&
EntityCollectionBase::derivedEntities()
{
  return dynamic_cast<const EntityCollection<T>&>(*this).derivedEntities();
}

template<class T>
T*
EntityCollectionBase::addEntity(T* entity)
{
  return dynamic_cast<const EntityCollection<T>&>(*this).addEntity(entity);
}

template<typename T>
void
EntityCollection<T>::clear()
{
  for (auto entity : collection)
    entity->deleteLater();

  collection.clear();
}

template<typename T>
void
EntityCollection<T>::update(const QJsonArray& jsonArray)
{
  clear();
  for (const QJsonValue& jsonValue : jsonArray)
    addEntity(new T(this, jsonValue.toObject()));
}

template<typename T>
std::vector<Entity*>
EntityCollection<T>::baseEntities()
{
  std::vector<Entity*> returnValue;
  for (T* entity : collection) {
    returnValue.push_back(entity);
  }
  return returnValue;
}

template<typename T>
T*
EntityCollection<T>::addEntity(T* entity)
{
  if (!collection.contains(entity)) {
    collection.append(entity);
    emit EntityCollectionObject::collectionChanged();
  }

  return entity;
}

} // namespace data
