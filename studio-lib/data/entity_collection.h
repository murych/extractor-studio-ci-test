#ifndef ENTITY_COLLECTION_H
#define ENTITY_COLLECTION_H

#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QObject>

namespace data {
class Entity;

class EntityCollectionObject : public QObject
{
  Q_OBJECT
public:
  EntityCollectionObject(QObject* _parent = nullptr)
    : QObject{ _parent }
  {
    qDebug() << "EntityCollectionObject" << _parent;
  }
  virtual ~EntityCollectionObject() = default;

signals:
  void collectionChanged();
};

class EntityCollectionBase : public EntityCollectionObject
{
public:
  EntityCollectionBase(QObject* parent = nullptr,
                       const QString& key = "somecollectionKey")
    : EntityCollectionObject{ parent }
    , key{ key }
  {
    qDebug() << "EntityCollectionBase" << parent << key;
  }
  virtual ~EntityCollectionBase() override = default;

  QString getKey() const { return key; }

  virtual void clear() = 0;
  virtual void update(const QJsonArray& json) = 0;
  virtual std::vector<Entity*> baseEntities() = 0;

  template<class T>
  QList<T*>& derivedEntities();

  template<class T>
  T* addEntity(T* entity);

private:
  QString key;
};

template<typename T>
class EntityCollection : public EntityCollectionBase
{
public:
  EntityCollection(QObject* parent = nullptr,
                   const QString& key = "SomeCollectionKey")
    : EntityCollectionBase{ parent, key }
  {
    qDebug() << "EntityCollection" << parent << key;
  }

  ~EntityCollection() override = default;

  void clear() override
  {
    for (auto entity : collection) {
      entity->deleteLater();
    }
    collection.clear();
  }

  void update(const QJsonArray& jsonArray) override
  {
    clear();
    for (const QJsonValue& jsonValue : jsonArray) {
      addEntity(new T(this, jsonValue.toObject()));
    }
  }

  std::vector<Entity*> baseEntities() override
  {
    std::vector<Entity*> returnValue;
    for (T* entity : collection) {
      returnValue.push_back(entity);
    }
    return returnValue;
  }

  QList<T*>& derivedEntities() { return collection; }

  T* addEntity(T* entity)
  {
    if (!collection.contains(entity)) {
      collection.append(entity);
      emit EntityCollectionObject::collectionChanged();
    }

    return entity;
  }

private:
  QList<T*> collection;
};

template<class T>
QList<T*>&
EntityCollectionBase::derivedEntities()
{
  return dynamic_cast<const EntityCollection<T>&>(*this).derivedEntities();
}

template<class T>
T*
EntityCollectionBase::addEntity(T* entity)
{
  return dynamic_cast<const EntityCollection<T>&>(*this).addEntity(entity);
}

}

#endif // ENTITY_COLLECTION_H
