#ifndef DATETIMEDECORATOR_H
#define DATETIMEDECORATOR_H

//#include "cm-lib_global.h"
#include "data_decorator.h"
#include <QDateTime>
#include <QJsonObject>
#include <QJsonValue>
#include <QObject>
#include <QScopedPointer>

// namespace cm {
namespace data {

class /*CMLIB_EXPORT*/ DateTimeDecorator : public DataDecorator
{
  Q_OBJECT
  Q_PROPERTY(QString ui_iso8601String READ toIso8601String NOTIFY valueChanged)
  Q_PROPERTY(
    QString ui_prettyDateString READ toPrettyDateString NOTIFY valueChanged)
  Q_PROPERTY(
    QString ui_prettyTimeString READ toPrettyTimeString NOTIFY valueChanged)
  Q_PROPERTY(QString ui_prettyString READ toPrettyString NOTIFY valueChanged)

public:
  DateTimeDecorator(Entity* parentEntity = nullptr,
                    const QString& key = "SomeItemKey",
                    const QString& label = {},
                    const QDateTime& dateTime = {});
  ~DateTimeDecorator();

  DateTimeDecorator& setValue(const QDateTime& dateTime);
  const QDateTime& value() const;

  QJsonValue jsonValue() const override;
  void update(const QJsonObject& jsonObject) override;

  const QString toIso8601String() const;
  const QString toPrettyDateString() const;
  const QString toPrettyTimeString() const;
  const QString toPrettyString() const;

signals:
  void valueChanged();

private:
  class Implementation;
  QScopedPointer<Implementation> implementation;
};

} // namespace data
//} // namespace cm

#endif // DATETIMEDECORATOR_H
