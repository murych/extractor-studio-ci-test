#ifndef GCODE_GCODEGENERATOR_H
#define GCODE_GCODEGENERATOR_H

#include "failable.h"
#include <QObject>

namespace gcode {

class AbstractGCodeGenerator
  : public QObject
  , public Failable
{
  Q_OBJECT
public:
  explicit AbstractGCodeGenerator(QObject* parent = nullptr);
  virtual ~AbstractGCodeGenerator();
};

} // namespace gcode

#endif // GCODE_GCODEGENERATOR_H
