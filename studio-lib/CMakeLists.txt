cmake_minimum_required(VERSION 3.5)

project(
  extractor-studio-core
  VERSION 0.1
  LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(
  Qt5
  COMPONENTS Core SerialPort Network
  REQUIRED)

set(PROJECT_SOURCES
    backend_error.h
    failable.h
    failable.cpp
    logger.h
    logger.cpp
    data/data_decorator.h
    data/data_decorator.cpp
    data/datetime_decorator.h
    data/datetime_decorator.cpp
    data/enumerator_decorator.h
    data/enumerator_decorator.cpp
    data/int_decorator.h
    data/int_decorator.cpp
    data/string_decorator.h
    data/string_decorator.cpp
    data/entity.h
    data/entity.cpp
    data/entity_collection.h
    data/color_decorator.h
    data/color_decorator.cpp
    data/double_decorator.h
    data/double_decorator.cpp
    data/bool_decorator.h
    data/bool_decorator.cpp
    models/project/extractor_project.h
    models/project/extractor_project.cpp
    models/project/project_info.h
    models/project/project_info.cpp
    models/project/laboratory.h
    models/project/laboratory.cpp
    models/project/layout.h
    models/project/layout.cpp
    models/project/protocol.h
    models/project/protocol.cpp
    models/plates/reagent.h
    models/plates/reagent.cpp
    models/plates/plate.h
    models/plates/plate.cpp
    models/steps/step_factories.h
    models/steps/abstract_step.h
    models/steps/abstract_step.cpp
    models/steps/mixing_stage.h
    models/steps/mixing_stage.cpp
    models/steps/step_mix.h
    models/steps/step_mix.cpp
    models/steps/step_sleeves_pickup.h
    models/steps/step_sleeves_pickup.cpp
    models/steps/step_sleeves_leave.h
    models/steps/step_sleeves_leave.cpp
    models/steps/step_dry.h
    models/steps/step_dry.cpp
    models/steps/step_pause.h
    models/steps/step_pause.cpp
    models/steps/step_sleep.h
    models/steps/step_sleep.cpp
    models/steps/step_heater.h
    models/steps/step_heater.cpp
    models/steps/step_beads_collect.h
    models/steps/step_beads_collect.cpp
    models/steps/step_beads_release.h
    models/steps/step_beads_release.cpp
    models/steps/step_manual_move.h
    models/steps/step_manual_move.cpp
    models/steps/moving_speed.h
    gcode/gcode_generator.h
    gcode/gcode_generator.cpp
    config/abstract_config.h
    config/abstract_config.cpp
    config/config_app.h
    config/config_app.cpp
    config/config_lab.h
    config/config_lab.cpp
    models/device/abstract_device.h
    models/device/abstract_device.cpp
    models/device/device_extractor.h
    models/device/device_extractor.cpp
    models/device/motor.h
    models/device/motor.cpp
    models/device/plate.h
    models/device/plate.cpp
    )

add_library(${PROJECT_NAME} STATIC ${PROJECT_SOURCES})
set_target_properties(${PROJECT_NAME} PROPERTIES VERSION ${PROJECT_VERSION}
                                                 SOVERSION 1)

target_link_libraries(${PROJECT_NAME} Qt5::Core Qt5::SerialPort Qt5::Network)
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_compile_options(${PROJECT_NAME} PRIVATE -Werror -Wall -Wextra)

include(GNUInstallDirs)
install(TARGETS ${PROJECT_NAME} LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
