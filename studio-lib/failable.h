#ifndef FAILABLE_H
#define FAILABLE_H

#include "backend_error.h"
#include <QString>

class Failable
{
public:
  Failable();

  bool isError() const;

  BackendError::ErrorType error() const;

  void setError(BackendError::ErrorType error);
  void setError(BackendError::ErrorType error, const QString& errorString);

  const QString& errorString() const;
  void setErrorString(const QString& errorString);

  void clearError();

private:
  BackendError::ErrorType m_error{ BackendError::NoError };
  QString m_errorString{};
};

#endif // FAILABLE_H
