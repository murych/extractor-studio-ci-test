#ifndef CONFIG_LABORATORYCONFIG_H
#define CONFIG_LABORATORYCONFIG_H

#include "abstract_config.h"
#include <QString>

namespace config {

class LaboratoryConfig : public AbstractConfig
{
public:
  LaboratoryConfig() = default;
  ~LaboratoryConfig() = default;

  QString lab_name{ QStringLiteral("Lab Name") };
  QString lab_phone{ QStringLiteral("88005553535") };
  QString lab_email{ QStringLiteral("research@lab.org") };
  QString lab_address{ QLatin1String("") };

  // AbstractConfig interface
public:
  void readSettings() override;
  void writeSettings() override;
};

} // namespace config

#endif // CONFIG_LABORATORYCONFIG_H
