#ifndef CONFIG_APP_H
#define CONFIG_APP_H

#include "abstract_config.h"
#include <QDir>
#include <QPoint>
#include <QSize>
#include <QString>

namespace config {

class ApplicationConfig : public AbstractConfig
{
public:
  ApplicationConfig() = default;
  ~ApplicationConfig() = default;

  bool first_run;
  QString work_dir{ QDir::homePath() };
  QSize window_size{ QSize{ 1000, 700 } };
  QPoint window_pos{ QPoint{ 400, 0 } };

  // AbstractConfig interface
public:
  void readSettings() override;
  void writeSettings() override;

  // as there's no model for application configuration (yet?) declaring settings
  // fields keys here
private:
  const QString k_header = QStringLiteral("application");
  const QString k_win_pos = QStringLiteral("window_pos");
  const QString k_win_size = QStringLiteral("window_size");
  const QString k_work_dir = QStringLiteral("cur_work_dir");
};

}

#endif // CONFIG_APP_H
