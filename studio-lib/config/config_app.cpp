#include "config_app.h"

#include <QCoreApplication>
#include <QSettings>

namespace config {

void
ApplicationConfig::readSettings()
{
  QSettings settings{ QSettings::IniFormat,
                      QSettings::UserScope,
                      QCoreApplication::organizationName(),
                      QCoreApplication::applicationName() };

  settings.beginGroup(k_header);
  window_pos = settings.value(k_win_pos, QPoint{ 400, 0 }).toPoint();
  window_size = settings.value(k_win_size, QSize{ 1000, 700 }).toSize();
  work_dir = settings.value(k_work_dir, QDir::homePath()).toString();
  settings.endGroup();
}

void
ApplicationConfig::writeSettings()
{
  QSettings settings{ QSettings::IniFormat,
                      QSettings::UserScope,
                      QCoreApplication::organizationName(),
                      QCoreApplication::applicationName() };

  settings.beginGroup(k_header);
  settings.setValue(k_win_pos, window_pos);
  settings.setValue(k_win_size, window_size);
  settings.setValue(k_work_dir, work_dir);
  settings.endGroup();
}

} // namespace config
