#ifndef CONFIG_ABSTRACTCONFIG_H
#define CONFIG_ABSTRACTCONFIG_H

#include <QString>

namespace config {

class AbstractConfig
{
public:
  AbstractConfig() = default;
  virtual ~AbstractConfig() = default;

  virtual void readSettings() = 0;
  virtual void writeSettings() = 0;
};

} // namespace config

#endif // CONFIG_ABSTRACTCONFIG_H
