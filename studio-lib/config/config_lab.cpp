#include "config_lab.h"

#include "models/project/laboratory.h"
#include <QCoreApplication>
#include <QSettings>

namespace config {

void
LaboratoryConfig::readSettings()
{
  models::project::Laboratory lab;
  QSettings settings{ QSettings::IniFormat,
                      QSettings::UserScope,
                      QCoreApplication::organizationName(),
                      QCoreApplication::applicationName() };

  settings.beginGroup(lab.key());
  lab_name = settings.value(lab.name->key(), lab.name->value()).toString();
  lab_phone = settings.value(lab.phone->key(), lab.phone->value()).toString();
  lab_email = settings.value(lab.email->key(), lab.email->value()).toString();
  lab_address =
    settings.value(lab.address->key(), lab.address->value()).toString();
  settings.endGroup(); // laboratory
}

void
LaboratoryConfig::writeSettings()
{
  models::project::Laboratory lab;
  QSettings settings{ QSettings::IniFormat,
                      QSettings::UserScope,
                      QCoreApplication::organizationName(),
                      QCoreApplication::applicationName() };

  settings.beginGroup(lab.key());
  settings.setValue(lab.name->key(), lab_name);
  settings.setValue(lab.phone->key(), lab_phone);
  settings.setValue(lab.email->key(), lab_email);
  settings.setValue(lab.address->key(), lab_address);
  settings.endGroup(); // laboratory
}

} // namespace config
