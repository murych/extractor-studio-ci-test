#ifndef MODELS_STEPMANUALMOVE_H
#define MODELS_STEPMANUALMOVE_H

#include "abstract_step.h"
#include "data/double_decorator.h"

namespace models {
namespace steps {

//! @todo добавить бы каким-то образом знание о количестве и именах осей, эхх
class StepManualMove : public AbstractStep
{
  Q_OBJECT
public:
  //! @todo проверить соответствие
  enum eAxises
  {
    unknown = 0,
    x1 = 2,
    x2 = 3,
    z1 = 4,
    z2 = 5,
    y1 = 6
  };
  Q_ENUM(eAxises)

  explicit StepManualMove(QObject* parent = nullptr);
  StepManualMove(QObject* parent, const QJsonObject& json);

  ~StepManualMove() override;

  data::EnumeratorDecorator* target_axis{ nullptr };
  data::DoubleDecorator* target_position{ nullptr };

private:
  static QMap<int, QString> axisMapper;
};

} // namespace steps
} // namespace models

#endif // MODELS_STEPMANUALMOVE_H
