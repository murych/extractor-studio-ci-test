#include "abstract_step.h"

#include "logger.h"
#include "moving_speed.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(MAS, "STEP ABSTR")

namespace models {
namespace steps {

QMap<int, QString> AbstractStep::stepTypeMapper{
  { StepType::unknown, "" },
  { StepType::pickup, "Pick-up" },
  { StepType::leave, "Leave" },
  { StepType::mix, "Mix" },
  { StepType::dry, "Dry" },
  { StepType::pause, "Pause" },
  { StepType::move, "Manual Move" },
  { StepType::collect, "Collect Beads" },
  { StepType::release, "Release Beads" },
};

QMap<int, QString> AbstractStep::stepEndTipPosMapper{
  { TipPos::undefined, "undefined" },
  { TipPos::outside, "Outside Well" },
  { TipPos::above, "On Well surface" },
  { TipPos::inside, "On Liquid surface" }
};

AbstractStep::AbstractStep(QObject* parent)
  : data::Entity{ parent, "step" }
  , idx{ static_cast<data::IntDecorator*>(
      addDataItem(new data::IntDecorator{ this, "step_idx", "Step Index" })) }
  , name{ static_cast<data::StringDecorator*>(
      addDataItem(new data::StringDecorator{ this, "name", "Step Name" })) }
  , duration{ static_cast<data::IntDecorator*>(addDataItem(
      new data::IntDecorator{ this, "step_duration", "Step Duration" })) }
  , type{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "step_type",
                                                 "Step Type",
                                                 0,
                                                 stepTypeMapper })) }
  , plate{ static_cast<data::IntDecorator*>(addDataItem(
      new data::IntDecorator{ this, "related_plate", "Related Plate" })) }
  , sleevesPosAtEnd{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "sleeves_pos_at_end",
                                                 "Sleeves Position At End" })) }
  , verticalTravelSpeed{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "vertical_travel_speed",
                                                 "Vertical Travel Speed",
                                                 0,
                                                 travelSpeedMapper })) }
  , horizontTravelSpeed{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "horizontal_travel_speed",
                                                 "Horizontal Travel Speed",
                                                 0,
                                                 travelSpeedMapper })) }
{
  qCDebug(MAS) << "AbstractStep::AbstractStep" << parent;
}

AbstractStep::AbstractStep(QObject* parent, const QJsonObject& json)
  : AbstractStep{ parent }
{
  qCDebug(MAS) << "AbstractStep::AbstractStep" << parent << json;
  update(json);
}

AbstractStep::~AbstractStep()
{
  delete idx;
  delete name;
  delete type;
  delete duration;
  delete plate;

  delete sleevesPosAtEnd;
  delete verticalTravelSpeed;
  delete horizontTravelSpeed;
}

void
AbstractStep::setStepIdx(const int newIdx)
{
  idx->setValue(newIdx);
  emit stepIdxChanged();
}

void
AbstractStep::setStepName(const QString& newName)
{
  name->setValue(newName);
  emit stepNameChanged();
}

void
AbstractStep::setRelatedPlate(const int newPlate)
{
  plate->setValue(newPlate);
  emit relatedPlateChanged();
}

void
AbstractStep::setStepType(const int newType)
{
  qCDebug(MAS) << "AbstractStep::setStepType"
               << QVariant::fromValue(newType).toString();
  type->setValue(static_cast<int>(newType));

  emit sleevesPosAtEndChanged();
}

void
AbstractStep::setSleevesPosAtEnd(const int newValue)
{
  qCDebug(MAS)
    << "AbstractStep::setSleevesPosAtEnd"
    << QVariant::fromValue(static_cast<eTipPos>(newValue)).toString();
  sleevesPosAtEnd->setValue(newValue);

  emit sleevesPosAtEndChanged();
}

void
AbstractStep::setVerticalSpeed(const int newValue)
{
  qCDebug(MAS) << "AbstractStep::setVerticalSpeed" << newValue;
  verticalTravelSpeed->setValue(newValue);

  emit verticalSpeedChanged();
}

void
AbstractStep::setHorizontalSpeed(const int newValue)
{
  qCDebug(MAS) << "AbstractStep::setHorizontalSpeed" << newValue;
  horizontTravelSpeed->setValue(newValue);

  emit horizontalSpeedChanged();
}

} // namespace step
} // namespace models
