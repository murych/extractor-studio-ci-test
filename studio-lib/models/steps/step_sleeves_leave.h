#ifndef MODELS_STEPLEAVE_H
#define MODELS_STEPLEAVE_H

#include "abstract_step.h"

namespace models {
namespace steps {

class StepSleevesLeave : public AbstractStep
{
  Q_OBJECT
public:
  explicit StepSleevesLeave(QObject* parent = nullptr);
  StepSleevesLeave(QObject* parent, const QJsonObject& json);
  ~StepSleevesLeave() override;
};

} // namespace stepsa
} // namespace models

#endif // MODELS_STEPLEAVE_H
