#include "mixing_stage.h"

namespace models {
namespace steps {

QMap<int, QString> MixingStage::mixingSpeedMapper{
  { eTravelSpeed::unknown, "" },
  { eTravelSpeed::slow, "Slow" },
  { eTravelSpeed::medium, "Medium" },
  { eTravelSpeed::fast, "Fast" }
};

MixingStage::MixingStage(QObject* parent)
  : data::Entity{ parent }
  , idx{ static_cast<data::IntDecorator*>(
      addDataItem(new data::IntDecorator{ this, "mixing_idx", "Mixing Idx" })) }
  , duration{ static_cast<data::IntDecorator*>(
      addDataItem(new data::IntDecorator{ this,
                                          "mixing_duration",
                                          "Mixing Duration",
                                          300 })) }
  , speed{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "mixing_speed",
                                                 "Mixing Speed",
                                                 medium,
                                                 mixingSpeedMapper })) }
{}

MixingStage::MixingStage(QObject* parent, const QJsonObject& json)
  : MixingStage{ parent }
{
  update(json);

  switch (static_cast<eTravelSpeed>(speed->value())) {
    case eTravelSpeed::slow:
      speed_icon = QStringLiteral(":/48px/inactive_state_48px.png");
      break;
    case eTravelSpeed::medium:
      speed_icon = QStringLiteral(":/48px/active_state_48px.png");
      break;
    case eTravelSpeed::fast:
      speed_icon = QStringLiteral(":/48px/final_state_48px.png");
      break;
    default:
      speed_icon = QStringLiteral(":/48/color/error_48px.png");
      break;
  }
}

MixingStage::~MixingStage()
{
  delete duration;
  delete speed;
}

void
MixingStage::setIdx(const int _idx)
{
  idx->setValue(_idx);
  emit idxChanged();
}

void
MixingStage::setDuration(const int _duration)
{
  duration->setValue(_duration);
  emit durationChanged();
}

void
MixingStage::setSpeed(const int _speed)
{
  speed->setValue(_speed);
  emit speedChanged();
}

} // namespace steps
} // namespace models
