#include "step_sleeves_leave.h"

namespace models {
namespace steps {

StepSleevesLeave::StepSleevesLeave(QObject* parent)
  : AbstractStep{ parent }
{
  name->setValue("Release sleeves");
  type->setValue(eStepType::leave);

  icon = QString{ ":/48px/turn_48px.png" };
}

StepSleevesLeave::StepSleevesLeave(QObject* parent, const QJsonObject& json)
  : StepSleevesLeave{ parent }
{
  update(json);
}

StepSleevesLeave::~StepSleevesLeave() {}

} // namespace steps
} // namespace models
