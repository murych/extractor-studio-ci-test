#ifndef MODELS_STEPDRY_H
#define MODELS_STEPDRY_H

#include "abstract_step.h"

namespace models {
namespace steps {

class StepDry : public AbstractStep
{
  Q_OBJECT
  Q_PROPERTY(
    int uiDuration READ getDuration WRITE setDuration NOTIFY durationChanged)
  Q_PROPERTY(
    int uiPosition READ getPosition WRITE setPosition NOTIFY positionChanged)

public:
  enum eDryPos
  {
    unknown = 0,
    above,
    outside
  };
  Q_ENUM(eDryPos)

  explicit StepDry(QObject* parent = nullptr);
  StepDry(QObject* parent, const QJsonObject& json);
  ~StepDry() override;

  data::IntDecorator* duration{ nullptr };
  data::EnumeratorDecorator* position{ nullptr };

public:
  int getDuration() const { return duration->value(); }
  int getPosition() const { return position->value(); }

public slots:
  void setDuration(const int value);
  void setPosition(const int value);

signals:
  void durationChanged();
  void positionChanged();

private:
  static QMap<int, QString> positionMapper;
};

} // namespace steps
} // namespace models

#endif // MODELS_STEPDRY_H
