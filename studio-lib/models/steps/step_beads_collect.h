#ifndef MODELS_STEPBEADSCOLLECT_H
#define MODELS_STEPBEADSCOLLECT_H

#include "abstract_step.h"

namespace models {
namespace steps {

class StepBeadsCollect : public models::steps::AbstractStep
{
  Q_OBJECT
  Q_PROPERTY(int collectCount READ getCollectCount WRITE setCollectCount NOTIFY
               collectCountChanged)
  Q_PROPERTY(int collectDuration READ getCollectDuration WRITE
               setCollectDuration NOTIFY collectDuraitonChanged)
  Q_PROPERTY(int collectSpeed READ getCollectSpeed WRITE setCollectSpeed NOTIFY
               collectSpeedChanged)

public:
  explicit StepBeadsCollect(QObject* parent = nullptr);
  StepBeadsCollect(QObject* parent, const QJsonObject& json);
  ~StepBeadsCollect() override;

  data::IntDecorator* collect_count{ nullptr };
  data::IntDecorator* collect_duration{ nullptr };
  data::EnumeratorDecorator* collect_speed{ nullptr };

public:
  int getCollectCount() const { return collect_count->value(); }
  void setCollectCount(const int count);

  int getCollectDuration() const { return collect_duration->value(); }
  void setCollectDuration(const int dur);

  int getCollectSpeed() const { return collect_speed->value(); }
  void setCollectSpeed(const int speed);

signals:
  void collectCountChanged();
  void collectDuraitonChanged();
  void collectSpeedChanged();
};

} // namespace steps
} // namespace models

#endif // MODELS_STEPBEADSCOLLECT_H
