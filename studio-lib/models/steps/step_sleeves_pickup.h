#ifndef STEPPICKUP_H
#define STEPPICKUP_H

#include "abstract_step.h"

namespace models {
namespace steps {

class StepSleevesPickup : public AbstractStep
{
  Q_OBJECT
public:
  explicit StepSleevesPickup(QObject* parent = nullptr);
  StepSleevesPickup(QObject* parent, const QJsonObject& json);
  ~StepSleevesPickup() override;
};

} // namespace steps
}

#endif // STEPPICKUP_H
