#ifndef STEPMIX_H
#define STEPMIX_H

#include "abstract_step.h"
#include "data/entity_collection.h"
#include "models/steps/mixing_stage.h"

namespace models {
namespace steps {

class StepMix : public models::steps::AbstractStep
{
  Q_OBJECT
  Q_PROPERTY(int uiPatternRepeat READ getPatternRepeat WRITE setPatternRepeat
               NOTIFY patternRepeatChanged)
  Q_PROPERTY(int uiMixAmplitude READ getMixAmplitude WRITE setMixAmplitude
               NOTIFY mixAmplitudeChanged)
  Q_PROPERTY(int uiEnterSpeed READ getEnterSpeed WRITE setEnterSpeed NOTIFY
               enterSpeedChanged)
  Q_PROPERTY(int uiEndPosition READ getStartPosition WRITE setStartPosition
               NOTIFY endPositionChanged)
public:
  enum EndPositions
  {
    unknown = 0,
  };
  Q_ENUM(EndPositions)

  explicit StepMix(QObject* parent = nullptr);
  StepMix(QObject* parent, const QJsonObject& json);
  ~StepMix() override;

  data::IntDecorator* pattern_repeat{ nullptr };
  data::IntDecorator* mix_amplitude{ nullptr };
  data::EnumeratorDecorator* enter_speed{ nullptr };
  data::EnumeratorDecorator* end_position{ nullptr };

  data::EntityCollection<MixingStage>* mix_stages{ nullptr };

public:
  int getPatternRepeat() const { return pattern_repeat->value(); }
  int getMixAmplitude() const { return mix_amplitude->value(); }
  int getEnterSpeed() const { return enter_speed->value(); }
  int getStartPosition() const { return end_position->value(); }
  QList<models::steps::MixingStage*> getMixStages() const;

public slots:
  void setPatternRepeat(const int value);
  void setMixAmplitude(const int value);
  void setEnterSpeed(const int value);
  void setStartPosition(const int value);
  void setMixStages(const QList<models::steps::MixingStage*>& stages);

signals:
  void patternRepeatChanged();
  void mixAmplitudeChanged();
  void enterSpeedChanged();
  void endPositionChanged();
  void mixStagesChanged();

private:
  static QMap<int, QString> endPositionMapper;
};

} // namespace steps
} // namespace models

#endif // STEPMIX_H
