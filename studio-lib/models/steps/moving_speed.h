#ifndef MOVING_SPEED_H
#define MOVING_SPEED_H

#include <QMap>
#include <QObject>
#include <QString>

namespace models {
namespace steps {

enum eTravelSpeed
{
  unknown = 0,
  slow,
  medium,
  fast,
  user
};

const static QMap<int, QString> travelSpeedMapper{
  { eTravelSpeed::unknown, "" },
  { eTravelSpeed::slow, QObject::tr("Slow") },
  { eTravelSpeed::medium, QObject::tr("Medium") },
  { eTravelSpeed::fast, QObject::tr("Fast") }
};

const static QMap<int, QString> speedIconMapper{
  { eTravelSpeed::unknown, "" },
  { eTravelSpeed::slow, ":/48px/inactive_state_48px.png" },
  { eTravelSpeed::medium, ":/48px/active_state_48px.png" },
  { eTravelSpeed::fast, ":/48px/final_state_48px.png" }
};

} // namespace steps
} // namespace models

#endif // MOVING_SPEED_H
