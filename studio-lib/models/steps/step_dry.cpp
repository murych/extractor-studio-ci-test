#include "step_dry.h"

namespace models {
namespace steps {

QMap<int, QString> StepDry::positionMapper{
  { StepDry::eDryPos::unknown, "" },
  { StepDry::eDryPos::above, "Above" },
  { StepDry::eDryPos::outside, "Outside" }
};

StepDry::StepDry(QObject* parent)
  : AbstractStep{ parent }
  , duration{ static_cast<data::IntDecorator*>(
      addDataItem(new data::IntDecorator{ this, "duration", "Dry Duration" })) }
  , position{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "position",
                                                 "Dry Position",
                                                 0,
                                                 positionMapper })) }
{
  name->setValue("Dry");
  type->setValue(eStepType::dry);

  icon = QString{ ":/48px/dry_48px.png" };
}

StepDry::StepDry(QObject* parent, const QJsonObject& json)
  : StepDry{ parent }
{
  update(json);
}

StepDry::~StepDry()
{
  delete duration;
  delete position;
}

void
StepDry::setDuration(const int value)
{
  duration->setValue(value);
  emit durationChanged();
}

void
StepDry::setPosition(const int value)
{
  position->setValue(value);
  emit positionChanged();
}

} // namespace steps
} // namespace models
