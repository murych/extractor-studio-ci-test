#ifndef MIXINGSTAGE_H
#define MIXINGSTAGE_H

#include "data/entity.h"
#include "data/enumerator_decorator.h"
#include "data/int_decorator.h"
#include "models/steps/moving_speed.h"
#include <QJsonObject>
#include <QMap>

namespace models {
namespace steps {

class MixingStage : public data::Entity
{
  Q_OBJECT
  Q_PROPERTY(int idx READ getIdx WRITE setIdx NOTIFY idxChanged)
  Q_PROPERTY(
    int duration READ getDuration WRITE setDuration NOTIFY durationChanged)
  Q_PROPERTY(int speed READ getSpeed WRITE setSpeed NOTIFY speedChanged)

public:
  explicit MixingStage(QObject* parent = nullptr);
  MixingStage(QObject* parent, const QJsonObject& json);
  ~MixingStage();

  data::IntDecorator* idx{ nullptr };
  //! время перемешивания в секундах
  data::IntDecorator* duration{ nullptr };
  //! скорость перемешивания
  data::EnumeratorDecorator* speed{ nullptr };

public:
  int getIdx() const { return idx->value(); }
  void setIdx(const int _idx);

  int getDuration() const { return duration->value(); }
  void setDuration(const int _duration);

  int getSpeed() const { return speed->value(); }
  void setSpeed(const int _speed);

signals:
  void idxChanged();
  void durationChanged();
  void speedChanged();

private:
  static QMap<int, QString> mixingSpeedMapper;

  //! иконка для перемешивания
  //! @todo переделать в маппер такой же как для типа скорости?
  QString speed_icon{ QStringLiteral(":/48/color/error_48px.png") };
};

} // namespace steps
} // namespace models

#endif // MIXINGSTAGE_H
