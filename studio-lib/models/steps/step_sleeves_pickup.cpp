#include "step_sleeves_pickup.h"

namespace models {
namespace steps {

StepSleevesPickup::StepSleevesPickup(QObject* parent)
  : AbstractStep{ parent }
{
  name->setValue("Pickup sleeves");
  type->setValue(eStepType::pickup);

  icon = QString{ ":/48px/arrow_up_48px.png" };
}

StepSleevesPickup::StepSleevesPickup(QObject* parent, const QJsonObject& json)
  : StepSleevesPickup{ parent }
{
  update(json);
}

StepSleevesPickup::~StepSleevesPickup() {}

} // namespace steps
} // namespace models
