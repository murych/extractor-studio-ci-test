#include "step_mix.h"

#include "logger.h"
#include "moving_speed.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(MSM, "MIX")

namespace models {
namespace steps {

QMap<int, QString> StepMix::endPositionMapper{ { StepMix::EndPositions::unknown,
                                                 "" } };

StepMix::StepMix(QObject* parent)
  : models::steps::AbstractStep{ parent }
  , pattern_repeat{ static_cast<data::IntDecorator*>(addDataItem(
      new data::IntDecorator{ this, "parttern_repeat", "Pattern Repeat" })) }
  , mix_amplitude{ static_cast<data::IntDecorator*>(addDataItem(
      new data::IntDecorator{ this, "mix_amplitude", "Mix Amplitude" })) }
  , enter_speed{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "enter_speed",
                                                 "Enter speed",
                                                 eTravelSpeed::unknown,
                                                 travelSpeedMapper })) }
  , end_position{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "end_position",
                                                 "Mix end position",
                                                 EndPositions::unknown,
                                                 endPositionMapper })) }
  , mix_stages{ static_cast<data::EntityCollection<MixingStage>*>(
      addChildCollection(
        new data::EntityCollection<MixingStage>{ this, "stages" })) }
{
  qCDebug(MSM) << "StepMix::StepMix" << parent;

  name->setValue("Mixin");
  type->setValue(eStepType::mix);

  icon = QString{ ":/48px/curly_arrow_48px.png" };
}

StepMix::StepMix(QObject* parent, const QJsonObject& json)
  : StepMix{ parent }
{
  qCDebug(MSM) << "StepMix::StepMix" << parent << json;
  update(json);
}

StepMix::~StepMix()
{
  delete pattern_repeat;
  delete mix_amplitude;
  delete enter_speed;
  delete mix_stages;
  delete end_position;
}

void
StepMix::setPatternRepeat(const int value)
{
  qCDebug(MSM) << "StepMix::setPatternRepeat" << value;
  pattern_repeat->setValue(value);
  emit patternRepeatChanged();
}

void
StepMix::setMixAmplitude(const int value)
{
  qCDebug(MSM) << "StepMix::setMixAmplitude" << value;
  if (value < 0 || value > 100)
    return;

  mix_amplitude->setValue(value);
  emit mixAmplitudeChanged();
}

void
StepMix::setEnterSpeed(const int value)
{
  qCDebug(MSM) << "StepMix::setEnterSpeed" << value;

  enter_speed->setValue(value);
  emit enterSpeedChanged();
}

void
StepMix::setStartPosition(const int value)
{
  qCDebug(MSM) << "StepMix::setEndPosition" << value;

  end_position->setValue(value);
  emit endPositionChanged();
}

QList<models::steps::MixingStage*>
StepMix::getMixStages() const
{
  return mix_stages->derivedEntities();
}

void
StepMix::setMixStages(const QList<MixingStage*>& stages)
{
  qCDebug(MSM) << "StepMix::setMixStages"
               << "setting mix stages size of" << stages.count();
  mix_stages->clear();
  for (auto& stage : stages)
    mix_stages->addEntity(stage);
}

} // namespace steps
} // namespace models
