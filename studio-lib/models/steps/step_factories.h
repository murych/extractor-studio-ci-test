#ifndef STEP_FACTORIES_H
#define STEP_FACTORIES_H

#include "step_beads_collect.h"
#include "step_beads_release.h"
#include "step_dry.h"
#include "step_heater.h"
#include "step_manual_move.h"
#include "step_mix.h"
#include "step_pause.h"
#include "step_sleep.h"
#include "step_sleeves_leave.h"
#include "step_sleeves_pickup.h"

namespace models {
namespace steps {

class AbstractStepFactory
{
public:
  AbstractStepFactory() = default;
  virtual ~AbstractStepFactory() = default;

  virtual AbstractStep* FactoryMethod(const QJsonObject&) const = 0;
};

class StepBeadsCollectFactory : public AbstractStepFactory
{
public:
  StepBeadsCollectFactory() = default;
  ~StepBeadsCollectFactory() override = default;

  AbstractStep* FactoryMethod(const QJsonObject& json) const override
  {
    return new StepBeadsCollect{ nullptr, json };
  }
};

class StepBeadsReleaseFactory : public AbstractStepFactory
{
public:
  StepBeadsReleaseFactory() = default;
  ~StepBeadsReleaseFactory() override = default;

  AbstractStep* FactoryMethod(const QJsonObject& json) const override
  {
    return new StepBeadsRelease{ nullptr, json };
  }
};

class StepDryFactory : public AbstractStepFactory
{
public:
  StepDryFactory() = default;
  ~StepDryFactory() override = default;

  AbstractStep* FactoryMethod(const QJsonObject& json) const override
  {
    return new StepDry{ nullptr, json };
  }
};

class StepHeaterFactory : public AbstractStepFactory
{
public:
  StepHeaterFactory() = default;
  ~StepHeaterFactory() override = default;

  AbstractStep* FactoryMethod(const QJsonObject& json) const override
  {
    return new StepHeater{ nullptr, json };
  }
};

class StepManualMoveFactory : public AbstractStepFactory
{
public:
  StepManualMoveFactory() = default;
  ~StepManualMoveFactory() override = default;

  AbstractStep* FactoryMethod(const QJsonObject& json) const override
  {
    return new StepManualMove{ nullptr, json };
  }
};

class StepMixFactory : public AbstractStepFactory
{
public:
  StepMixFactory() = default;
  ~StepMixFactory() override = default;

  AbstractStep* FactoryMethod(const QJsonObject& json) const override
  {
    return new StepMix{ nullptr, json };
  }
};

class StepPauseFactory : public AbstractStepFactory
{
public:
  StepPauseFactory() = default;
  ~StepPauseFactory() override = default;

  AbstractStep* FactoryMethod(const QJsonObject& json) const override
  {
    return new StepPause{ nullptr, json };
  }
};

class StepSleepFactory : public AbstractStepFactory
{
public:
  StepSleepFactory() = default;
  ~StepSleepFactory() override = default;

  AbstractStep* FactoryMethod(const QJsonObject& json) const override
  {
    return new StepSleep{ nullptr, json };
  }
};

class StepSleevesCollectFactory : public AbstractStepFactory
{
public:
  StepSleevesCollectFactory() = default;
  ~StepSleevesCollectFactory() override = default;

  AbstractStep* FactoryMethod(const QJsonObject& json) const override
  {
    return new StepSleevesPickup{ nullptr, json };
  }
};

class StepSleevesReleaseFactory : public AbstractStepFactory
{
public:
  StepSleevesReleaseFactory() = default;
  ~StepSleevesReleaseFactory() override = default;

  AbstractStep* FactoryMethod(const QJsonObject& json) const override
  {
    return new StepSleevesLeave{ nullptr, json };
  }
};

} // namespace steps
} // namespace models

#endif // STEP_FACTORIES_H
