#include "step_heater.h"

namespace models {
namespace steps {

StepHeater::StepHeater(QObject* parent)
  : AbstractStep{ parent }
  , block_until_complete{ static_cast<data::BoolDecorator*>(
      addDataItem(new data::BoolDecorator{ this,
                                           "block_until_complete",
                                           "Block Until Complete" })) }
  , target_temperature{ static_cast<data::DoubleDecorator*>(
      addDataItem(new data::DoubleDecorator{ this,
                                             "target_temperature",
                                             "Target Temperature" })) }
{
  name->setValue("Heater cotnrol");
  type->setValue(eStepType::heater);

  icon = QString{ ":/48px/heating_48px.png" };
}

StepHeater::StepHeater(QObject* parent, const QJsonObject& json)
  : StepHeater{ parent }
{
  update(json);
}

StepHeater::~StepHeater()
{
  delete block_until_complete;
  delete target_temperature;
}

void
StepHeater::setBlockExecution(const bool value)
{
  block_until_complete->setValue(value);
  emit blockExecutionChanged();
}

void
StepHeater::setTargetTemp(const double value)
{
  target_temperature->setValue(value);
  emit targetTempChanged();
}

} // namespace steps
} // namespace models
