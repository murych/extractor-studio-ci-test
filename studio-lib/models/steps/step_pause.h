#ifndef MODELS_STEPPAUSE_H
#define MODELS_STEPPAUSE_H

#include "abstract_step.h"

namespace models {
namespace steps {

class StepPause : public AbstractStep
{
  Q_OBJECT
public:
  explicit StepPause(QObject* parent = nullptr);
  StepPause(QObject* parent, const QJsonObject& json);
  ~StepPause();

private:
  data::StringDecorator* message{ nullptr };

public:
  QString getMessage() const { return message->value(); }

public slots:
  void setMessage(const QString& value);

signals:
  void messageChanged();
};

} // namespace steps
} // namespace models

#endif // MODELS_STEPPAUSE_H
