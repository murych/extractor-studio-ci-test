#include "step_pause.h"

namespace models {
namespace steps {

StepPause::StepPause(QObject* parent)
  : AbstractStep{ parent }
  , message{ static_cast<data::StringDecorator*>(
      addDataItem(new data::StringDecorator{ this, "message", "Message" })) }
{
  name->setValue("Pause");
  type->setValue(eStepType::block);

  icon = QString{ ":/48px/sleep_mode_48px.png" };
}

StepPause::StepPause(QObject* parent, const QJsonObject& json)
  : StepPause{ parent }
{
  update(json);
}

StepPause::~StepPause()
{
  delete message;
}

void
StepPause::setMessage(const QString& value)
{
  message->setValue(value);
  emit messageChanged();
}

} // namespace stepsp
} // namespace models
