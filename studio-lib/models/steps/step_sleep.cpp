#include "step_sleep.h"

#include "logger.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(MSS, "STEP SLEEP")

namespace models {
namespace steps {

StepSleep::StepSleep(QObject* parent)
  : AbstractStep{ parent }
  , sleep_duration{ static_cast<data::IntDecorator*>(addDataItem(
      new data::IntDecorator{ this, "sleep_duration", "Sleep Duration" })) }
{
  name->setValue("Sleep");
  type->setValue(eStepType::pause);

  icon = QString{ ":/48px/sleep_48px.png" };
}

StepSleep::StepSleep(QObject* parent, const QJsonObject& json)
  : StepSleep{ parent }
{
  update(json);
}

StepSleep::~StepSleep()
{
  delete sleep_duration;
}

void
StepSleep::setSleepDuration(const int value)
{
  qCDebug(MSS) << "StepSleep::setSleepDuration"
               << "old value" << sleep_duration->value() << "new value"
               << value;

  sleep_duration->setValue(value);
  emit sleepDurationChanged();
}

} // namespace steps
} // namespace models
