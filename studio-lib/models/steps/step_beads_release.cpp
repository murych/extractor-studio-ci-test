#include "step_beads_release.h"

#include "logger.h"
#include "moving_speed.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(MSBR, "BEADS RELEASE")

namespace models {
namespace steps {

StepBeadsRelease::StepBeadsRelease(QObject* parent)
  : models::steps::AbstractStep{ parent }
  , release_count{ static_cast<data::IntDecorator*>(addDataItem(
      new data::IntDecorator{ this, "release_count", "release count" })) }
  , release_duration{ static_cast<data::IntDecorator*>(addDataItem(
      new data::IntDecorator{ this, "release_duration", "release Duration" })) }
  , release_speed{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "release_speed",
                                                 "release Speed",
                                                 0,
                                                 travelSpeedMapper })) }
{
  name->setValue("Beads release");
  type->setValue(eStepType::release);

  icon = QString{ ":/48px/magnetic_release_48px.png" };
}

StepBeadsRelease::StepBeadsRelease(QObject* parent, const QJsonObject& json)
  : StepBeadsRelease{ parent }
{
  update(json);
}

StepBeadsRelease::~StepBeadsRelease()
{
  delete release_count;
  delete release_duration;
  delete release_speed;
}

void
StepBeadsRelease::setReleaseCount(const int value)
{
  qCDebug(MSBR) << "StepBeadsRelease::setReleaseCount" << value;
  release_count->setValue(value);

  emit releaseCountChanged();
}

void
StepBeadsRelease::setReleaseDuration(const int value)
{
  qCDebug(MSBR) << "StepBeadsRelease::setReleaseDuration" << value;
  release_duration->setValue(value);

  emit releaseDurationChanged();
}

void
StepBeadsRelease::setReleaseSpeed(const int value)
{
  qCDebug(MSBR) << "StepBeadsRelease::setReleaseSpeed" << value;
  release_speed->setValue(value);

  emit releaseSpeedChanged();
}

} // namespace stepsp
} // namespace models
