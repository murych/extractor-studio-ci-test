#include "step_beads_collect.h"

#include "logger.h"
#include "moving_speed.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(MSBC, "BEADS COLLECT")

namespace models {
namespace steps {

StepBeadsCollect::StepBeadsCollect(QObject* parent)
  : models::steps::AbstractStep{ parent }
  , collect_count{ static_cast<data::IntDecorator*>(addDataItem(
      new data::IntDecorator{ this, "collect_count", "Collect count" })) }
  , collect_duration{ static_cast<data::IntDecorator*>(addDataItem(
      new data::IntDecorator{ this, "collect_duration", "Collect Duration" })) }
  , collect_speed{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "collect_speed",
                                                 "Collect Speed",
                                                 0,
                                                 travelSpeedMapper })) }
{
  name->setValue("Collect Beads");
  type->setValue(eStepType::collect);

  icon = QString{ ":/48px/magnetic_collect_48px.png" };
}

StepBeadsCollect::StepBeadsCollect(QObject* parent, const QJsonObject& json)
  : StepBeadsCollect{ parent }
{
  update(json);
}

StepBeadsCollect::~StepBeadsCollect()
{
  delete collect_count;
  delete collect_duration;
  delete collect_speed;
}

void
StepBeadsCollect::setCollectCount(const int count)
{
  qCDebug(MSBC) << "StepBeadsCollect::setCollectCount" << count;
  collect_count->setValue(count);

  emit collectCountChanged();
}

void
StepBeadsCollect::setCollectDuration(const int dur)
{
  qCDebug(MSBC) << "StepBeadsCollect::setCollectCount" << dur;
  collect_duration->setValue(dur);

  emit collectDuraitonChanged();
}

void
StepBeadsCollect::setCollectSpeed(const int speed)
{
  qCDebug(MSBC) << "StepBeadsCollect::setCollectCount" << speed;
  collect_speed->setValue(speed);

  emit collectSpeedChanged();
}

} // namespace steps
} // namespace models
