#ifndef MODELS_STEPHEATER_H
#define MODELS_STEPHEATER_H

#include "abstract_step.h"
#include "data/bool_decorator.h"
#include "data/double_decorator.h"

namespace models {
namespace steps {

class StepHeater : public AbstractStep
{
  Q_OBJECT
public:
  explicit StepHeater(QObject* parent = nullptr);
  StepHeater(QObject* parent, const QJsonObject& json);
  ~StepHeater() override;

  data::BoolDecorator* block_until_complete{ nullptr };
  data::DoubleDecorator* target_temperature{ nullptr };

public:
  bool getBlockExecution() const { return block_until_complete->value(); }
  double getTargetTemp() const { return target_temperature->value(); }

public slots:
  void setBlockExecution(const bool value);
  void setTargetTemp(const double value);

signals:
  void blockExecutionChanged();
  void targetTempChanged();
};

} // namespace steps
} // namespace models

#endif // MODELS_STEPHEATER_H
