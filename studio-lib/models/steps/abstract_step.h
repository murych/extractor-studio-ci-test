#ifndef ABSTRACTSTEP_H
#define ABSTRACTSTEP_H

#include "data/entity.h"
#include "data/enumerator_decorator.h"
#include "data/int_decorator.h"
#include "data/string_decorator.h"

namespace models {
namespace steps {

class AbstractStep : public data::Entity
{
  Q_OBJECT
  Q_PROPERTY(int idx READ getStepIdx WRITE setStepIdx NOTIFY stepIdxChanged)
  Q_PROPERTY(int type READ getStepType WRITE setStepType NOTIFY stepTypeChanged)
  Q_PROPERTY(
    QString name READ getStepName WRITE setStepName NOTIFY stepNameChanged)
  Q_PROPERTY(int plate READ getRelatedPlate WRITE setRelatedPlate NOTIFY
               relatedPlateChanged)

public:
  enum eStepType
  {
    unknown = 0,
    //! зохват колпачков
    pickup = 1,
    //! сброс колпачков
    leave = 2,
    //! перемешивание
    mix = 3,
    //! сушка частиц
    dry = 4,
    //! обычная пауза
    pause = 5,
    //! ручное перемещение
    move = 6,
    //! сбор магнитных частиц
    collect = 7,
    //! сброс магнитных частиц
    release = 8,
    //! управление нагревателем
    heater = 9,
    //! блокирование выполнение, ожидание оператора
    block = 10,
    user = 11
  };
  Q_ENUM(eStepType)

  enum eTipPos
  {
    undefined = 0,
    outside,
    above,
    inside
  };
  Q_ENUM(eTipPos)

  explicit AbstractStep(QObject* parent = nullptr);
  AbstractStep(QObject* parent, const QJsonObject& json);
  virtual ~AbstractStep();

  data::IntDecorator* idx{ nullptr };
  data::StringDecorator* name{ nullptr };
  data::IntDecorator* duration{ nullptr };
  data::EnumeratorDecorator* type{ nullptr };
  data::IntDecorator* plate{ nullptr };

  data::EnumeratorDecorator* sleevesPosAtEnd{ nullptr };
  data::EnumeratorDecorator* verticalTravelSpeed{ nullptr };
  data::EnumeratorDecorator* horizontTravelSpeed{ nullptr };

public:
  int getStepIdx() const { return idx->value(); }
  void setStepIdx(const int newIdx);

  QString getStepName() const { return name->value(); }
  void setStepName(const QString& newName);

  int getRelatedPlate() const { return plate->value(); }
  void setRelatedPlate(const int newPlate);

  int getStepType() const { return type->value(); }
  void setStepType(const int newType);

  int getSleevesPosAtEnd() const { return sleevesPosAtEnd->value(); }
  void setSleevesPosAtEnd(const int newValue);

  int getVerticalSpeed() const { return verticalTravelSpeed->value(); }
  void setVerticalSpeed(const int newValue);

  int getHorizontalSpeed() const { return horizontTravelSpeed->value(); }
  void setHorizontalSpeed(const int newValue);

  QString stepIcon() const { return icon; }

  // protected:
  //  virtual void foo() = 0;

  static QMap<int, QString> stepTypeMapper;
  static QMap<int, QString> stepEndTipPosMapper;
  QString icon{ ":/48px/close_48px.png" };

signals:
  void stepIdxChanged();
  void stepNameChanged();
  void stepTypeChanged();
  void relatedPlateChanged();
  void sleevesPosAtEndChanged();
  void verticalSpeedChanged();
  void horizontalSpeedChanged();
};

} // namespace steps
} // namespace models

using StepType = models::steps::AbstractStep::eStepType;
using TipPos = models::steps::AbstractStep::eTipPos;

#endif // ABSTRACTSTEP_H
