#ifndef MODELS_STEPBEADSRELEASE_H
#define MODELS_STEPBEADSRELEASE_H

#include "abstract_step.h"

namespace models {
namespace steps {

class StepBeadsRelease : public models::steps::AbstractStep
{
  Q_OBJECT
  Q_PROPERTY(int uiReleaseCount READ getReleaseCount WRITE setReleaseCount
               NOTIFY releaseCountChanged)
  Q_PROPERTY(int uiReleaseDuration READ getReleaseDuration WRITE
               setReleaseDuration NOTIFY releaseDurationChanged)
  Q_PROPERTY(int uiReleaseSpeed READ getReleaseSpeed WRITE setReleaseSpeed
               NOTIFY releaseSpeedChanged)

public:
  explicit StepBeadsRelease(QObject* parent = nullptr);
  StepBeadsRelease(QObject* parent, const QJsonObject& json);
  ~StepBeadsRelease() override;

  data::IntDecorator* release_count{ nullptr };
  data::IntDecorator* release_duration{ nullptr };
  data::EnumeratorDecorator* release_speed{ nullptr };

public:
  int getReleaseCount() const { return release_count->value(); }
  int getReleaseDuration() const { return release_duration->value(); }
  int getReleaseSpeed() const { return release_speed->value(); }

public slots:
  void setReleaseCount(const int value);
  void setReleaseDuration(const int value);
  void setReleaseSpeed(const int value);

signals:
  void releaseCountChanged();
  void releaseDurationChanged();
  void releaseSpeedChanged();
};

} // namespace steps
} // namespace models

#endif // MODELS_STEPBEADSRELEASE_H
