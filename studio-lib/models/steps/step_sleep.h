#ifndef MODELS_STEPSLEEP_H
#define MODELS_STEPSLEEP_H

#include "abstract_step.h"

namespace models {
namespace steps {

class StepSleep : public AbstractStep
{
  Q_OBJECT
  Q_PROPERTY(int uiSleepDuration READ getSleepDuration WRITE setSleepDuration
               NOTIFY sleepDurationChanged)
public:
  explicit StepSleep(QObject* parent = nullptr);
  StepSleep(QObject* parent, const QJsonObject& json);
  ~StepSleep();

private:
  data::IntDecorator* sleep_duration{ nullptr };

public:
  int getSleepDuration() const { return sleep_duration->value(); }

public slots:
  void setSleepDuration(const int value);

signals:
  void sleepDurationChanged();
};

} // namespace steps
} // namespace models

#endif // MODELS_STEPSLEEP_H
