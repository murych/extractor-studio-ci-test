#include "step_manual_move.h"

namespace models {
namespace steps {

QMap<int, QString> StepManualMove::axisMapper{
  { StepManualMove::eAxises::unknown, "" }
};

StepManualMove::StepManualMove(QObject* parent)
  : AbstractStep{ parent }
  , target_axis{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "target_axis",
                                                 "Target Axis",
                                                 0,
                                                 axisMapper })) }
  , target_position{
    static_cast<data::DoubleDecorator*>(addDataItem(
      new data::DoubleDecorator{ this, "target_position", "Target Position" }))
  }
{
  name->setValue("Manual move");
  type->setValue(eStepType::move);

  icon = QString{ ":/48px/stepper_motor_48px.png" };
}

StepManualMove::StepManualMove(QObject* parent, const QJsonObject& json)
  : StepManualMove{ parent }
{
  update(json);
}

StepManualMove::~StepManualMove()
{
  delete target_axis;
  delete target_position;
}

} // namespace steps
} // namespace models
