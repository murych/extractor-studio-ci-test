#include "device_extractor.h"

namespace models {
namespace device {

QMap<int, QString> DeviceExtractor::extractorTypeMapper{
  { static_cast<int>(DeviceExtractor::eExtractorType::unknown), "" },
  { static_cast<int>(DeviceExtractor::eExtractorType::standard96),
    "Extractor 96" }
};

DeviceExtractor::DeviceExtractor(QObject* parent)
  : AbstractDevice{ parent }
  , model{ static_cast<data::EnumeratorDecorator*>(addDataItem(
      new data::EnumeratorDecorator(this, "model", "Device Model"))) }
  , plates{ static_cast<data::EntityCollection<Plate>*>(
      addChildCollection(new data::EntityCollection<Plate>{ this, "plates" })) }
  , motors{ static_cast<data::EntityCollection<Motor>*>(
      addChildCollection(new data::EntityCollection<Motor>{ this, "motors" })) }
{}

DeviceExtractor::DeviceExtractor(const QJsonObject& json, QObject* parent)
  : DeviceExtractor{ parent }
{
  update(json);
}

DeviceExtractor::~DeviceExtractor()
{
  delete model;
  delete plates;
  delete motors;
}

} // namespace device
} // namespace models
