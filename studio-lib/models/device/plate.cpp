#include "plate.h"

namespace models {
namespace device {

Plate::Plate(QObject* parent)
  : data::Entity{ parent, "plate" }
  , idx{ static_cast<data::IntDecorator*>(
      addDataItem(new data::IntDecorator{ this, "idx", "Plate Index" })) }
  , heater{ static_cast<data::BoolDecorator*>(
      addDataItem(new data::BoolDecorator{ this, "heater", "Has heater" })) }
{}

Plate::Plate(QObject* parent, const QJsonObject& json)
  : Plate{ parent }
{
  update(json);
}

Plate::~Plate()
{
  delete idx;
  delete heater;
}

} // namespace device
} // namespace models
