#include "abstract_device.h"

namespace models {
namespace device {

QMap<int, QString> AbstractDevice::deviceTypeMapper{
  { static_cast<int>(AbstractDevice::eDeviceType::unknown), "" },
  { static_cast<int>(AbstractDevice::eDeviceType::extractor), "Extractor" }
};

AbstractDevice::AbstractDevice(QObject* parent)
  : data::Entity{ parent }
  , title{ static_cast<data::StringDecorator*>(
      addDataItem(new data::StringDecorator{ this, "title", "Device Title" })) }
  , description{ static_cast<data::StringDecorator*>(addDataItem(
      new data::StringDecorator{ this, "description", "Device Description" })) }
  , type{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{
        this,
        "type",
        "Device Type",
        static_cast<int>(AbstractDevice::eDeviceType::unknown),
        deviceTypeMapper })) }
{}

AbstractDevice::AbstractDevice(QObject* parent, const QJsonObject& json)
  : AbstractDevice{ parent }
{
  update(json);
}

AbstractDevice::~AbstractDevice() {}

} // namespace device
} // namespace models
