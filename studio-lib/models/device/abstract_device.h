#ifndef MODELS_DEVICE_ABSTRACTDEVICE_H
#define MODELS_DEVICE_ABSTRACTDEVICE_H

#include "data/entity.h"
#include "data/enumerator_decorator.h"
#include "data/int_decorator.h"
#include "data/string_decorator.h"

namespace models {
namespace device {

class AbstractDevice : public data::Entity
{
  Q_OBJECT
public:
  enum class eDeviceType
  {
    unknown = 0,
    extractor = 1
  };
  Q_ENUM(eDeviceType)

  explicit AbstractDevice(QObject* parent = nullptr);
  AbstractDevice(QObject* parent, const QJsonObject& json);
  virtual ~AbstractDevice();

  data::StringDecorator* title{ nullptr };
  data::StringDecorator* description{ nullptr };
  data::EnumeratorDecorator* type{ nullptr };

private:
  static QMap<int, QString> deviceTypeMapper;
};

} // namespace device
} // namespace models

#endif // MODELS_DEVICE_ABSTRACTDEVICE_H
