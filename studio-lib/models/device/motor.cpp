#include "motor.h"

namespace models {
namespace device {

Motor::Motor(QObject* parent)
  : data::Entity{ parent }
  , axis{ static_cast<data::StringDecorator*>(
      addDataItem(new data::StringDecorator{ this, "axis", "Motor Axis" })) }
  , name{ static_cast<data::StringDecorator*>(
      addDataItem(new data::StringDecorator{ this, "name", "Motor Name" })) }
{}

Motor::Motor(QObject* parent, const QJsonObject& json)
  : Motor{ parent }
{
  update(json);
}

Motor::~Motor()
{
  delete axis;
  delete name;
}

} // namespace device
} // namespace models
