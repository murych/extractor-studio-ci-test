#ifndef MODELS_DEVICE_PLATE_H
#define MODELS_DEVICE_PLATE_H

#include "data/bool_decorator.h"
#include "data/entity.h"
#include "data/int_decorator.h"

namespace models {
namespace device {

class Plate : public data::Entity
{
  Q_OBJECT
public:
  explicit Plate(QObject* parent = nullptr);
  Plate(QObject* parent, const QJsonObject& json);
  ~Plate() override;

  data::IntDecorator* idx{ nullptr };
  data::BoolDecorator* heater{ nullptr };
};

} // namespace device
} // namespace models

#endif // MODELS_DEVICE_PLATE_H
