#ifndef MODELS_DEVICE_MOTOR_H
#define MODELS_DEVICE_MOTOR_H

#include "data/entity.h"
#include "data/string_decorator.h"

namespace models {
namespace device {

class Motor : public data::Entity
{
  Q_OBJECT
public:
  explicit Motor(QObject* parent = nullptr);
  Motor(QObject* parent, const QJsonObject& json);
  ~Motor() override;

  data::StringDecorator* axis{ nullptr };
  data::StringDecorator* name{ nullptr };
};

} // namespace device
} // namespace models

#endif // MODELS_DEVICE_MOTOR_H
