#ifndef MODELS_DEVICE_DEVICEEXTRACTOR_H
#define MODELS_DEVICE_DEVICEEXTRACTOR_H

#include "abstract_device.h"
#include "data/entity_collection.h"
#include "models/device/motor.h"
#include "models/device/plate.h"

namespace models {
namespace device {

class DeviceExtractor : public AbstractDevice
{
  Q_OBJECT
public:
  enum class eExtractorType
  {
    unknown = 0,
    standard96
  };
  Q_ENUM(eExtractorType)

  explicit DeviceExtractor(QObject* parent = nullptr);
  DeviceExtractor(const QJsonObject& json, QObject* parent = nullptr);
  ~DeviceExtractor() override;

  data::EnumeratorDecorator* model{ nullptr };
  data::EntityCollection<Plate>* plates{ nullptr };
  data::EntityCollection<Motor>* motors{ nullptr };

private:
  static QMap<int, QString> extractorTypeMapper;
};

} // namespace device
} // namespace models

#endif // MODELS_DEVICE_DEVICEEXTRACTOR_H
