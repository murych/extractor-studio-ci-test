#ifndef LAYOUT_H
#define LAYOUT_H

#include "data/entity.h"
#include "data/entity_collection.h"
#include "models/plates/plate.h"

namespace models {
namespace project {

class Layout : public data::Entity
{
  Q_OBJECT
public:
  explicit Layout(QObject* parent = nullptr);
  Layout(QObject* parent, const QJsonObject& json);
  ~Layout() override;

  data::EntityCollection<plates::Plate>* plates{ nullptr };

signals:
  void modified();
};

} // namespace project
} // namespace models

#endif // LAYOUT_H
