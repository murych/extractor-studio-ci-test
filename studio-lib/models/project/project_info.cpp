#include "project_info.h"

namespace models {
namespace project {

Project::Project(QObject* parent)
  : data::Entity{ parent, "project_info" }
  , spec_major_version{ static_cast<data::IntDecorator*>(
      addDataItem(new data::IntDecorator{ this,
                                          "spec_major_version",
                                          "spec major version" })) }
  , spec_minor_version{ static_cast<data::IntDecorator*>(
      addDataItem(new data::IntDecorator{ this,
                                          "spec_minor_version",
                                          "spec minor version" })) }
  , date_created{ static_cast<data::DateTimeDecorator*>(addDataItem(
      new data::DateTimeDecorator{ this, "date_created", "Date created" })) }
  , date_modified{ static_cast<data::DateTimeDecorator*>(addDataItem(
      new data::DateTimeDecorator{ this, "data_modified", "Date modified" })) }
  , title{ static_cast<data::StringDecorator*>(addDataItem(
      new data::StringDecorator{ this, "title", "Project title" })) }
  , author{ static_cast<data::StringDecorator*>(addDataItem(
      new data::StringDecorator{ this, "author", "Project author" })) }
  , description{
    static_cast<data::StringDecorator*>(addDataItem(
      new data::StringDecorator{ this, "description", "Project description" }))
  }
{}

Project::Project(QObject* parent, const QJsonObject& json)
  : Project{ parent }
{
  update(json);
}

Project::~Project()
{
  delete spec_major_version;
  delete spec_minor_version;
  delete date_created;
  delete date_modified;
  delete title;
  delete author;
  delete description;
}

} // namespace project
} // namespace models
