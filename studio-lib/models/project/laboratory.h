#ifndef LABORATORY_H
#define LABORATORY_H

#include "data/entity.h"
#include "data/string_decorator.h"

namespace models {
namespace project {

class Laboratory : public data::Entity
{
  Q_OBJECT
  Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
  Q_PROPERTY(
    QString address READ getAddress WRITE setAddress NOTIFY addressChanged)
  Q_PROPERTY(QString phone READ getPhone WRITE setPhone NOTIFY phoneChanged)
  Q_PROPERTY(QString email READ getEmail WRITE setEmail NOTIFY emailChanged)

public:
  explicit Laboratory(QObject* parent = nullptr);
  Laboratory(QObject* parent, const QJsonObject& json);
  ~Laboratory() override;

  QString getName() const { return name->value(); }
  void setName(const QString& _name) { name->setValue(_name); }

  QString getAddress() const { return address->value(); }
  void setAddress(const QString& _address) { address->setValue(_address); }

  QString getPhone() const { return phone->value(); }
  void setPhone(const QString& _phone) { phone->setValue(_phone); }

  QString getEmail() const { return email->value(); }
  void setEmail(const QString& _email) { email->setValue(_email); }

  // private:
  data::StringDecorator* name{ nullptr };
  data::StringDecorator* address{ nullptr };
  data::StringDecorator* phone{ nullptr };
  data::StringDecorator* email{ nullptr };

signals:
  void nameChanged();
  void addressChanged();
  void phoneChanged();
  void emailChanged();
};

} // namespace project
} // namespace models

#endif // LABORATORY_H
