#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "data/entity.h"
#include "data/entity_collection.h"
#include "models/steps/abstract_step.h"
#include "models/steps/step_factories.h"

namespace models {
namespace project {

class Protocol : public data::Entity
{
  Q_OBJECT
public:
  explicit Protocol(QObject* parent = nullptr);
  Protocol(QObject* parent, const QJsonObject& json);
  ~Protocol();

  data::EntityCollection<steps::AbstractStep>* steps{ nullptr };

private:
  QScopedPointer<steps::AbstractStepFactory> factory;
  void deduceType(const StepType type);

signals:
  void modified();
};

} // namespace project
} // namespace models

#endif // PROTOCOL_H
