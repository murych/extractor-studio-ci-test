#include "protocol.h"

using namespace data;

namespace models {
namespace project {

Protocol::Protocol(QObject* parent)
  : data::Entity{ parent, "protocol" }
  , steps{ static_cast<data::EntityCollection<steps::AbstractStep>*>(
      addChildCollection(
        new data::EntityCollection<steps::AbstractStep>{ this, "steps" })) }
{
  connect(
    steps, &EntityCollectionBase::collectionChanged, this, &Protocol::modified);
}

Protocol::Protocol(QObject* parent, const QJsonObject& json)
  : Protocol{ parent }
{
  if (json.contains(steps->getKey())) {
    const QJsonArray steps_array{ json.value(steps->getKey()).toArray() };

    for (int idx = 0; idx < steps_array.count(); ++idx) {
      const steps::AbstractStep aStep;
      const QJsonObject jsonStep{ steps_array.at(idx).toObject() };
      const StepType step_type{ static_cast<StepType>(
        jsonStep.value(aStep.type->key()).toInt(StepType::unknown)) };
      deduceType(step_type);
      steps->addEntity(factory->FactoryMethod(jsonStep));
    }
  }
}

Protocol::~Protocol()
{
  delete steps;
}

void
Protocol::deduceType(const StepType type)
{
  switch (type) {
    case StepType::pickup:
      factory.reset(new steps::StepSleevesCollectFactory);
      break;
    case StepType::leave:
      factory.reset(new steps::StepSleevesReleaseFactory);
      break;
    case StepType::mix:
      factory.reset(new steps::StepMixFactory);
      break;
    case StepType::dry:
      factory.reset(new steps::StepDryFactory);
      break;
    case StepType::pause:
      factory.reset(new steps::StepSleepFactory);
      break;
    case StepType::move:
      factory.reset(new steps::StepManualMoveFactory);
      break;
    case StepType::collect:
      factory.reset(new steps::StepBeadsCollectFactory);
      break;
    case StepType::release:
      factory.reset(new steps::StepBeadsReleaseFactory);
      break;
    case StepType::heater:
      factory.reset(new steps::StepHeaterFactory);
      break;
    case StepType::block:
      factory.reset(new steps::StepSleepFactory);
      break;
    default:
      break;
  }
}

} // namespace project
} // namespace models
