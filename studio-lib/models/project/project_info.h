#ifndef MODELS_PROJECT_PROJECT_H
#define MODELS_PROJECT_PROJECT_H

#include "data/datetime_decorator.h"
#include "data/entity.h"
#include "data/int_decorator.h"
#include "data/string_decorator.h"

namespace models {
namespace project {

class Project : public data::Entity
{
  Q_OBJECT
public:
  explicit Project(QObject* parent = nullptr);
  Project(QObject* parent, const QJsonObject& json);
  ~Project() override;

  data::IntDecorator* spec_major_version{ nullptr };
  data::IntDecorator* spec_minor_version{ nullptr };
  data::DateTimeDecorator* date_created{ nullptr };
  data::DateTimeDecorator* date_modified{ nullptr };
  data::StringDecorator* title{ nullptr };
  data::StringDecorator* author{ nullptr };
  data::StringDecorator* description{ nullptr };
};

} // namespace project
} // namespace models

#endif // MODELS_PROJECT_PROJECT_H
