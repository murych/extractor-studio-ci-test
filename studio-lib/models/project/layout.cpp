#include "layout.h"

using namespace data;

namespace models {
namespace project {

Layout::Layout(QObject* parent)
  : data::Entity{ parent, "layout" }
  , plates{ static_cast<EntityCollection<plates::Plate>*>(addChildCollection(
      new EntityCollection<plates::Plate>{ this, "plates" })) }
{
  connect(
    plates, &EntityCollectionBase::collectionChanged, this, &Layout::modified);
}

Layout::Layout(QObject* parent, const QJsonObject& json)
  : Layout{ parent }
{
  update(json);
}

Layout::~Layout()
{
  delete plates;
}

} // namespace project
} // namespace models
