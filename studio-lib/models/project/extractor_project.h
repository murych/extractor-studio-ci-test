#ifndef EXTRACTORPROJECT_H
#define EXTRACTORPROJECT_H

#include "data/data_decorator.h"
#include "data/datetime_decorator.h"
#include "data/entity.h"
#include "data/entity_collection.h"
#include "data/enumerator_decorator.h"
#include "data/int_decorator.h"
#include "data/string_decorator.h"
#include "models/project/laboratory.h"
#include "models/project/layout.h"
#include "models/project/project_info.h"
#include "models/project/protocol.h"

namespace models {
namespace project {

class ExtractorProject : public data::Entity
{
  Q_OBJECT
  Q_PROPERTY(bool ui_modified MEMBER is_modified NOTIFY modified)
public:
  explicit ExtractorProject(QObject* parent = nullptr);
  ExtractorProject(QObject* parent, const QJsonObject& json);
  ~ExtractorProject();

  Project* info{ nullptr };
  Laboratory* laboratory{ nullptr };
  Layout* layout{ nullptr };
  Protocol* protocol{ nullptr };

  bool is_modified{ false };

signals:
  void modified();
};

} // namespace project
}

#endif // EXTRACTORPROJECT_H
