#include "laboratory.h"

using namespace data;

namespace models {
namespace project {

Laboratory::Laboratory(QObject* parent)
  : data::Entity{ parent, "laboratory" }
  , name{ static_cast<StringDecorator*>(addDataItem(
      new StringDecorator{ this, "lab_name", "Laboratory Name", "Lab Name" })) }
  , address{ static_cast<StringDecorator*>(addDataItem(
      new StringDecorator{ this, "lab_address", "Laboratory Address", "" })) }
  , phone{ static_cast<StringDecorator*>(
      addDataItem(new StringDecorator{ this,
                                       "lab_phone",
                                       "Laboratory Phone",
                                       "88005553535" })) }
  , email{ static_cast<StringDecorator*>(
      addDataItem(new StringDecorator{ this,
                                       "lab_email",
                                       "Laboratory Email",
                                       "research@lab.ogr" })) }
{}

Laboratory::Laboratory(QObject* parent, const QJsonObject& json)
  : Laboratory{ parent }
{
  update(json);
}

Laboratory::~Laboratory()
{
  delete name;
  delete address;
  delete phone;
  delete email;
}

} // namespace project
} // namespace models
