#include "extractor_project.h"

namespace models {
namespace project {

ExtractorProject::ExtractorProject(QObject* parent)
  : data::Entity{ parent, "project" }
  , info{ static_cast<Project*>(addChild(new Project{ this }, "project_info")) }
  , laboratory{ static_cast<Laboratory*>(
      addChild(new Laboratory{ this }, "lab_info")) }
  , layout{ static_cast<Layout*>(addChild(new Layout{ this }, "layout")) }
  , protocol{ static_cast<Protocol*>(
      addChild(new Protocol{ this }, "protocol")) }
{
  connect(layout, &Layout::modified, this, [this] {
    if (!is_modified)
      is_modified = true;

    emit modified();
  });

  connect(protocol, &Protocol::modified, this, [this] {
    if (!is_modified)
      is_modified = true;

    emit modified();
  });
}

ExtractorProject::ExtractorProject(QObject* parent, const QJsonObject& json)
  : ExtractorProject{ parent }
{
  update(json);
}

ExtractorProject::~ExtractorProject()
{
  delete info;
  delete laboratory;
  delete layout;
  delete protocol;
}

} // namespace project
} // namespave models
