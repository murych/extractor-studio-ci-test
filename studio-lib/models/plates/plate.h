#ifndef PLATE_H
#define PLATE_H

#include "data/entity.h"
#include "data/entity_collection.h"
#include "data/enumerator_decorator.h"
#include "data/int_decorator.h"
#include "data/string_decorator.h"
#include "models/plates/reagent.h"
#include <QJsonObject>
#include <QMap>

namespace models {
namespace plates {

class Plate : public data::Entity
{
  Q_OBJECT
  Q_PROPERTY(data::IntDecorator* ui_plateIdx MEMBER idx NOTIFY plateIdxChanged)
  //  Q_PROPERTY(data::StringDecorator* ui_plateName MEMBER plate_name NOTIFY
  //               plateNameChanged)
  Q_PROPERTY(QString ui_plateName READ plateName WRITE setPlateName NOTIFY
               plateNameChanged)
  Q_PROPERTY(
    data::EnumeratorDecorator* ui_plateSize MEMBER size NOTIFY plateSizeChanged)
  Q_PROPERTY(
    data::EnumeratorDecorator* ui_plateType MEMBER type NOTIFY plateTypeChanged)
  Q_PROPERTY(data::StringDecorator* ui_plateBarcode MEMBER barcode NOTIFY
               plateBarcodeChanged)
  Q_PROPERTY(data::EntityCollection<Reagent>* ui_reagents MEMBER reagents NOTIFY
               reagentsChanged)
public:
  enum ePlateSize
  {
    unknown = 0,
    standard_96_plate,
    user
  };
  Q_ENUM(ePlateSize)

  enum ePlateType
  {
    //      unknown = 0,
    liquids,
    sleeves
  };
  Q_ENUM(ePlateType)

  explicit Plate(QObject* parent = nullptr);
  Plate(QObject* parent, const QJsonObject& json);
  ~Plate();

  data::StringDecorator* name{ nullptr };
  data::IntDecorator* idx{ nullptr };
  data::EnumeratorDecorator* size{ nullptr };
  data::EnumeratorDecorator* type{ nullptr };
  data::StringDecorator* barcode{ nullptr };
  data::EntityCollection<Reagent>* reagents{ nullptr };

public:
  QString plateName() const { return name->value(); }
  int plateIdx() const { return idx->value(); }
  int plateSize() const { return size->value(); }
  int plateContent() const { return type->value(); }
  QList<models::plates::Reagent*> plateReagents() const;

public slots:
  void setPlateName(const QString& newName);
  void setPlateIdx(const int newIdx);
  void setPlateSize(const int newSize);
  void setPlateContent(const int newContent);
  void setPlateReagents(const QList<models::plates::Reagent*>& newReagents);

signals:
  void reagentsChanged();
  void plateNameChanged();
  void plateIdxChanged();
  void plateSizeChanged();
  void plateTypeChanged();
  void plateBarcodeChanged();

private:
  static QMap<int, QString> plateSizeMapper;
  static QMap<int, QString> plateTypeMapper;
};

} // namespace plates
} // namespace models

using PlateSize = models::plates::Plate::ePlateSize;
using PlateType = models::plates::Plate::ePlateType;

#endif // PLATE_H
