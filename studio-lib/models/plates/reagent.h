#ifndef REAGENT_H
#define REAGENT_H

#include "data/color_decorator.h"
#include "data/entity.h"
#include "data/enumerator_decorator.h"
#include "data/int_decorator.h"
#include "data/string_decorator.h"
#include <QJsonObject>
#include <QMap>

namespace models {
namespace plates {

class Reagent : public data::Entity
{
  Q_OBJECT
  Q_PROPERTY(data::StringDecorator* ui_name MEMBER reagent_name CONSTANT)
  Q_PROPERTY(data::IntDecorator* ui_volume MEMBER reagent_volume CONSTANT)
  Q_PROPERTY(data::EnumeratorDecorator* ui_type MEMBER reagent_type CONSTANT)
  Q_PROPERTY(data::ColorDecorator* ui_color MEMBER reagent_color CONSTANT)

public:
  enum eReagentType
  {
    unknown = 0,
    reagent,
    sample,
    user
  };
  Q_ENUM(eReagentType)

  explicit Reagent(QObject* parent = nullptr);
  Reagent(QObject* parent, const QJsonObject& json);
  ~Reagent();

  data::StringDecorator* reagent_name{ nullptr };
  data::IntDecorator* reagent_volume{ nullptr };
  data::ColorDecorator* reagent_color{ nullptr };
  data::EnumeratorDecorator* reagent_type{ nullptr };

public:
  static QMap<int, QString> reagentTypeMapper;

private:
  QString m_icon{ QStringLiteral(":/48/color/error_48px.png") };

public:
  const QString icon() const { return m_icon; }
  const QString name() const { return reagent_name->value(); }
  int volume() const { return reagent_volume->value(); }
  int type() const { return reagent_type->value(); }
  const QString color() const { return reagent_color->value(); }

public slots:
  void setName(const QString& value);
  void setVolume(const int value);
  void setType(const int value);
  void setColor(const QString& value);

signals:
  void nameChanged();
  void volumeChanged();
  void typeChanged();
  void colorChanged();
};

} // namespace plates
} // namespace models

// Q_DECLARE_METATYPE(models::plates::Reagent)

#endif // REAGENT_H
