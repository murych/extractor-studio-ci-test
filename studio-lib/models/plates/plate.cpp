#include "plate.h"

#include "logger.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(MP, "PLATE")

namespace models {
namespace plates {

QMap<int, QString> Plate::plateSizeMapper{ { PlateSize::unknown, "" },
                                           { PlateSize::standard_96_plate,
                                             "Standard 96 plate" } };

QMap<int, QString> Plate::plateTypeMapper{ { PlateType::liquids, "Liquids" },
                                           { PlateType::sleeves, "Sleeves" } };

Plate::Plate(QObject* parent)
  : data::Entity{ parent, "plate" }
  , name{ static_cast<data::StringDecorator*>(addDataItem(
      new data::StringDecorator{ this, "name", "Plate Name", "New Plate" })) }
  , idx{ static_cast<data::IntDecorator*>(
      addDataItem(new data::IntDecorator{ this, "idx", "Plate Index" })) }
  , size{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "size",
                                                 "Plate size",
                                                 0,
                                                 plateSizeMapper })) }
  , type{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "type",
                                                 "Plate Type",
                                                 0,
                                                 plateTypeMapper })) }
  , reagents{ static_cast<data::EntityCollection<Reagent>*>(addChildCollection(
      new data::EntityCollection<Reagent>{ this, "reagents" })) }
{
  qRegisterMetaType<models::plates::Reagent*>("Reagent");
}

Plate::Plate(QObject* parent, const QJsonObject& json)
  : Plate{ parent }
{
  update(json);
}

Plate::~Plate()
{
  delete barcode;
  delete name;
  delete idx;
  delete size;
  delete reagents;
}

void
Plate::setPlateIdx(const int newIdx)
{
  qCDebug(MP) << "Plate::setPlateIdx"
              << "old idx" << idx->value() << "new idx" << newIdx;
  idx->setValue(newIdx);

  emit plateIdxChanged();
}

void
Plate::setPlateSize(const int newSize)
{
  qCDebug(MP) << "Plate::setPlateSize"
              << "old size" << size->value() << "new size" << newSize;
  size->setValue(newSize);

  emit plateSizeChanged();
}

void
Plate::setPlateContent(const int newContent)
{
  qCDebug(MP)
    << "Plate::setPlateContent"
    << "old content"
    << QVariant::fromValue(static_cast<ePlateType>(type->value())).toString()
    << "new content"
    << QVariant::fromValue(static_cast<ePlateType>(newContent)).toString();
  type->setValue(newContent);

  emit plateTypeChanged();
}

QList<Reagent*>
Plate::plateReagents() const
{
  return reagents->derivedEntities();
}

void
Plate::setPlateName(const QString& newName)
{
  qCDebug(MP) << "Plate::setPlateName"
              << "old plate name" << name->value() << "new plate name"
              << newName;
  name->setValue(newName);

  emit plateNameChanged();
}

void
Plate::setPlateReagents(const QList<models::plates::Reagent*>& newReagents)
{
  qCDebug(MP) << "Plate::setPlateReagents"
              << "old reagents size" << reagents->derivedEntities().count()
              << "new reagents size" << newReagents.count();
  reagents->derivedEntities().clear();
  for (auto& reagent : newReagents)
    reagents->addEntity(reagent);

  emit reagentsChanged();
}

} // namespace plates
} // namespace models
