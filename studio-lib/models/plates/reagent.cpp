#include "reagent.h"

#include "logger.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(MR, "REAGENT");

namespace models {
namespace plates {

QMap<int, QString> Reagent::reagentTypeMapper{
  { Reagent::eReagentType::unknown, "" },
  { Reagent::eReagentType::reagent, "reagent" },
  { Reagent::eReagentType::sample, "sample" }
};

Reagent::Reagent(QObject* parent)
  : data::Entity{ parent, "reagent" }
  , reagent_name{ static_cast<data::StringDecorator*>(addDataItem(
      new data::StringDecorator{ this, "reagent_name", "Reagent Name" })) }
  , reagent_volume{ static_cast<data::IntDecorator*>(addDataItem(
      new data::IntDecorator{ this, "reagent_volume", "Reagent Volume, μL" })) }
  , reagent_color{ static_cast<data::ColorDecorator*>(addDataItem(
      new data::ColorDecorator{ this, "reagent_color", "Reagent Color" })) }
  , reagent_type{ static_cast<data::EnumeratorDecorator*>(
      addDataItem(new data::EnumeratorDecorator{ this,
                                                 "reagent_type",
                                                 "Reagent Type",
                                                 0,
                                                 reagentTypeMapper })) }
{
  reagent_name->setValue("Reagent");
  reagent_volume->setValue(0);
  reagent_color->setValue("#343434");
  reagent_type->setValue(eReagentType::reagent);
}

Reagent::Reagent(QObject* parent, const QJsonObject& json)
  : Reagent{ parent }
{
  update(json);

  switch (static_cast<eReagentType>(reagent_type->value())) {
    case unknown:
    case user:
      m_icon = QStringLiteral(":/48/color/error_48px.png");
      break;
    case reagent:
      m_icon = QStringLiteral(":/48/color/test_tube_48px.png");
      break;
    case sample:
      m_icon = QStringLiteral(":/48/color/experiment_48px.png");
      break;
  }
}

Reagent::~Reagent()
{
  delete reagent_name;
  delete reagent_type;
  delete reagent_volume;
  delete reagent_color;
}

void
Reagent::setName(const QString& value)
{
  qCDebug(MR) << "Reagent::setName" << value;
  reagent_name->setValue(value);
  emit nameChanged();
}

void
Reagent::setVolume(const int value)
{
  qCDebug(MR) << "Reagent::setVolume" << value;
  reagent_volume->setValue(value);
  emit volumeChanged();
}

void
Reagent::setType(const int value)
{
  qCDebug(MR) << "Reagent::setType" << value;
  reagent_type->setValue(value);
  emit typeChanged();
}

void
Reagent::setColor(const QString& value)
{
  qCDebug(MR) << "Reagent::setColor" << value;
  reagent_color->setValue(value);
  emit colorChanged();
}

} // namespace plates
} // namespace models
